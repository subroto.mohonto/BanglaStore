
package com.arena.menu;

import java.util.ArrayList;
import java.util.List;



import com.arena.bangla.store.R;
import com.arena.bangla.store.LoginScreenActivity;
import com.arena.bangla.store.MainActivity_Static;
import com.arena.bangla.store.MenubarAdapter;
import com.arena.bangla.store.NewNumberLoginScreenActivity;
import com.arena.bangla.store.NumberSettingAcivity;
import com.arena.bangla.store.RowItem;
import com.arena.bangla.store.SettingActivity;
import com.arena.bangla.store.TemplateHomeScreenActivity;
import com.arena.bangla.store.TemplateHomeScreen_2_Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;




public class MenuDialog extends Dialog implements OnClickListener,OnItemClickListener
{ 


	Context context;
	int screenWidth, screenHeight;

	ListView list;
	Button btnList;
	RelativeLayout layoutTitle;
	private List<RowItem> rowItems;
	public String[] Titles;

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;



	/*public static final Integer[] Iamages = { 
		R.drawable.ic_my_feedback_white,// home
		R.drawable.ic_my_feedback_white,// feedback
		R.drawable.ic_my_banglalink_white

	};
	 */

	public MenuDialog(Context context) {
		super(context);
		Log.d("adnan","-----------Call dialog here11--------------");

		this.context = context;
		Log.e("adnan","------------------assadasd:------------"+context);
		//parentActivity = new ParentActivity(); 
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 

		WindowManager w = ((Activity) context).getWindowManager();
		Display d = w.getDefaultDisplay();  
		int screenWidth =d.getWidth();
		int screenHeight = d.getHeight();

		this.getWindow().setLayout(100, LayoutParams.WRAP_CONTENT);

		Window window = this.getWindow();

		WindowManager.LayoutParams wmlp = this.getWindow().getAttributes();
		wmlp.gravity = Gravity.TOP | Gravity.RIGHT; 
		//    wmlp.y = (screenHeight*16)/100;   //y position
		//wmlp.x = 100;   //x position
		wmlp.y = 0;
		window.setAttributes(wmlp);

		setContentView(R.layout.menu_dialog);

		sharedpreferences = ((Activity) context).getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

		//setLayoutParameter();

		list = (ListView) findViewById(R.id.list);
		btnList = (Button) findViewById(R.id.btn_list);

		btnList.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		layoutTitle = (RelativeLayout) findViewById(R.id.layout_title);
		layoutTitle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});


		// Defined Array values to show in ListView
		Titles = new String[] { 
				"Home Screen Template",
				"Language",
				"New Number Registration",
				"Number Setting",
				"Settings",
				"Terms & Conditions",
				"About",
				"Exit"
		};
		/*Titles = new String[] { 
				"Home Screen Template",
				"Language",
				"Help: 01814910253",
				
		};*/




		rowItems = new ArrayList<RowItem>();
		for (int i = 0; i < Titles.length; i++) 
		{
			RowItem item = new RowItem(Titles[i]);
			rowItems.add(item);
		}

		MenubarAdapter adapter = new MenubarAdapter(context,R.layout.list_item_top_menu, rowItems);
		list.setAdapter(adapter);
		list.setOnItemClickListener(this);

	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 

		Intent intent; 


		switch (position) { 
		case 0:

			String Screen = sharedpreferences.getString("HomeScreen", null);

			if(Screen.equals("MainActivity_Static"))
			{


				intent = new Intent(context, TemplateHomeScreenActivity.class);
				context.startActivity(intent);				
				((Activity) context).finish();
				dismiss(); 
			}
			else
			{
				intent = new Intent(context, MainActivity_Static.class);
				context.startActivity(intent);				
				((Activity) context).finish();
				dismiss(); 
			}


			break;
		case 1:

			String Language = sharedpreferences.getString("Language", null);
			if(Language.equals("?lang=bn"))
			{
				SharedPreferences.Editor editor2 = sharedpreferences.edit();
				//					editor2.putString("Language", "?lang=bn");
				editor2.putString("Language", "English");
				editor2.commit();

				String Screen2 = sharedpreferences.getString("HomeScreen", null);
				if(Screen2.equals("MainActivity_Static"))
				{
					intent = new Intent(context, MainActivity_Static.class);
					context.startActivity(intent);				
					((Activity) context).finish();
					dismiss(); 
				}
				else
				{
					intent = new Intent(context, TemplateHomeScreenActivity.class);
					context.startActivity(intent);				
					((Activity) context).finish();
					dismiss(); 
				}
			}
			else
			{
				SharedPreferences.Editor editor2 = sharedpreferences.edit();
				editor2.putString("Language", "?lang=bn");
				editor2.commit();
				String Screen2 = sharedpreferences.getString("HomeScreen", null);
				if(Screen2.equals("MainActivity_Static"))
				{
					intent = new Intent(context, MainActivity_Static.class);
					context.startActivity(intent);				
					((Activity) context).finish();
					dismiss(); 
				}
				else
				{
					intent = new Intent(context, TemplateHomeScreenActivity.class);
					context.startActivity(intent);				
					((Activity) context).finish();
					dismiss(); 
				}
			}

			dismiss(); 
			break;
		
		/*case 0:
			
			intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:01814910253" ));
			context.startActivity(intent);				
			
			
			
			break;*/

		/*case 0:
			intent = new Intent(context, NewNumberLoginScreenActivity.class);

			context.startActivity(intent);				
			((Activity) context).finish();
			dismiss(); 
			break;
		

		case 1:
			intent = new Intent(context, NumberSettingAcivity.class);

			context.startActivity(intent);				
			//				((Activity) context).finish();
			dismiss(); 
			break;

		case 2:
			intent = new Intent(context, SettingActivity.class);

			context.startActivity(intent);				
			((Activity) context).finish();
			dismiss(); 
			break;
			
		case 3:
			intent = new Intent(context, TemplateHomeScreen_2_Activity.class);
			
			context.startActivity(intent);				
			((Activity) context).finish();
			dismiss();
			break;
*/
		default:
			break;

		}//end switch case



	}	
	public void setFullScreen()
	{
		this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );
		// 	this.requestWindowFeature(Window.FEATURE_NO_TITLE);  
	}

	/*	@SuppressWarnings("deprecation")
	private void setScreenResolution()
	{ 
		WindowManager w = ((Activity) context).getWindowManager();
		Display d = w.getDefaultDisplay();  
		screenWidth =d.getWidth();
		screenHeight = d.getHeight();

	}
	 */








	@SuppressWarnings("deprecation")
	private void setScreenResolution()
	{  
		WindowManager w = ((Activity) context).getWindowManager();
		Display d = w.getDefaultDisplay();  
		screenWidth =d.getWidth();
		screenHeight = d.getHeight(); 

		DisplayMetrics metrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);

		switch (metrics.densityDpi) {
		case DisplayMetrics.DENSITY_HIGH: 
			screenHeight = metrics.heightPixels - getStatusBarHeight();

			break;
		case DisplayMetrics.DENSITY_MEDIUM: 
			screenHeight = metrics.heightPixels - getStatusBarHeight(); 


			break;
		case DisplayMetrics.DENSITY_LOW: 
			screenHeight = metrics.heightPixels - getStatusBarHeight();

			break;
		default: 
		} 
	}

	public int getStatusBarHeight() 
	{
		int result = 0;
		int resourceId = ((Activity) context).getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = ((Activity) context).getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub

	}






}

