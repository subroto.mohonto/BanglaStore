package com.arena.bangla.store;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForApps;
import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.AdapterForRatingReview;
import com.arena.bangla.store.R;
import com.arena.bangla.store.R.color;
import com.arena.bangla.store.AppsDetailsViewActivity.BillingAsyn;
import com.arena.bangla.store.AppsDetailsViewActivity.DownloadingCountAsyn;
import com.arena.bangla.store.AppsDetailsViewActivity.VideoScreenShow;
import com.arena.bangla.store.GameDetailsActivity.RatingAsyn;
import com.arena.image.loader.ImageLoader;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import com.devsmart.android.ui.HorizontalListView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class WallpaperDetailsActivity extends ParentActivity{
	
	String SubLink ="";
	String Language ="";
	String PinCode_ForLoign ="";
	String Login_Status = "",Login_Message="";
	String JSONResponse_Type_Login="";
	String MobileNumber ="";
	String PinCode ="";
	String Amount="";
	String Bill_Status = "",Bill_Message="",Regi_Status = "",Regi_Message="",Pin_Status = "",Pin_Message="";
	String Original_Price ="";
	String lat = "";
	String lng = "";
	String IMEI_No="";
	String JSONResponse_Type_Regi="",JSONResponse_Type_Pin="",JSONResponse_Type_Payment="";
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	
	ProgressDialog pDialog,dlDialog,pDialog12;
	String response;
	String response12;
	String _content_id12 ="";
	
	private VideoView videoView = null;
	private ProgressBar prog = null;
	private Context ctx = null;
	private MediaController mediaController = null;
//	String vidAddress = "https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";
	String _Content_Id;
	Button InstallBtn;
	
	String Content_Id, Type_Name, Content_Title, Content_Description, Preview,Banner, Total_Download, Payment_Type, Price, Company_Name, Developer_Name, Category_name,Which_Screen="";
	TextView NameTv, DownloadTv,Company_NameTv,Developer_NameTv,PriceTv, DescriptionTv;
	public ImageLoader imageLoader; 
	ImageView Banner_Image,Logo_Image;
	String _preview="", _banner="",_content_file="",_embedded_url_type, _embedded_url;
	InputStream ins;
	Bitmap bmImg;
	
/*	ArrayList<HashMap<String, String>> ContentArrayListScreenShot;
	HorizontalListView  ScreenShotProjectsList;
	AdapterForApps adapter;*/
	
	
	LinearLayout AppsLayout,VideoLayout, Mp3Layout, WallpaperLayout, GameLayout, BookLayout, LatestLayout;
	TextView appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv,LatestTv;

	TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv,LatestSubTv;

	AdapterForMainScreen adapterVideo, adapterMp3, adapterWallpaper, adapterApp,adapterGame,adapterBook	;



	ArrayList<HashMap<String, String>> ContentArrayListVideo12;
	ArrayList<HashMap<String, String>> ContentArrayListMp3;
	ArrayList<HashMap<String, String>> ContentArrayListWallpaper;
	ArrayList<HashMap<String, String>> ContentArrayListApp;
	ArrayList<HashMap<String, String>> ContentArrayListGame;
	ArrayList<HashMap<String, String>> ContentArrayListBook;
	ArrayList<HashMap<String, String>> ContentArrayListReview;

	HorizontalListView AppsProjectsList, VideoProjectsList12, Mp3ProjectsList, WallpaperProjectsList, GameProjectsList, BookProjectsList;
	Button appMoreBtn, videoMoreBtn, mp3MoreBtn, gameMoreBtn, bookMoreBtn, wallapperMoreBtn;
	RatingBar RatingBarBtn;
	String NameRating ="", CommentRating="",RatingNum="",JSONResponse_Type="";
	
	GridView  RatingReviewProjectsList;
	AdapterForRatingReview adapterRatingReview;
	LinearLayout LinearLayoutReviewShow;
	
	 ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
     ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();
     String responseSearch;
     AutoCompleteTextView SearchAutoCompleteTextView;
     long microsenconds;
     String _content_title12="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wallpaper_detais);
		
		Bundle b = getIntent().getExtras();
		_Content_Id = b.getString("Content_Id");
		Which_Screen = b.getString("which_screen");
		SubLink = b.getString("SubLink");
		Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+SubLink);
		
		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		final Context context = this;
		
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels;
		int height=dm.heightPixels/4;
		
		/*ViewInitializationAndActionofMenu(SubLink);
		ViewInitializationAndAction();*/
		ViewMenu(R.id.App_RelativeLayout,R.id.Video_RelativeLayout,R.id.MP3_RelativeLayout,R.id.Wallpaper_RelativeLayout,R.id.Game_RelativeLayout,R.id.Book_RelativeLayout,R.id.LatestNews_RelativeLayout,SubLink);
		
		imageLoader=new ImageLoader(WallpaperDetailsActivity.this);
		
		
		
//		Log.e("--------------------","------------------microsenconds  "+microsenconds);
		
		  
        SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
		SearchAutoCompleteTextView.setThreshold(1);
		
		
		NameTv = (TextView)findViewById(R.id.NameTxtV);
		DownloadTv = (TextView)findViewById(R.id.numberOfDownloadTxt);
		Company_NameTv = (TextView)findViewById(R.id.Company_NameTxt);
		Developer_NameTv = (TextView)findViewById(R.id.Developer_NameTxt);
		PriceTv = (TextView)findViewById(R.id.PriceText);
		DescriptionTv = (TextView)findViewById(R.id.DescriptionTxt);
		
		Banner_Image = (ImageView)findViewById(R.id.banner_image);
		Banner_Image.getLayoutParams().height = height;
		
		Logo_Image = (ImageView)findViewById(R.id.logo_image);
		
		Language = sharedpreferences.getString("Language", "");
		if(Language.equals("English"))
		{
			Language="";			
		}
		/* if(sharedpreferences.getString("download", null).equals("download_on"))
			{
				new BillingAsyn().execute();
			}*/
		 
		RatingReviewProjectsList  = (GridView) findViewById(R.id.gridViewReview);
		LinearLayoutReviewShow = (LinearLayout)findViewById(R.id.LinearLayoutReviewShow);
		
		InstallBtn = (Button)findViewById(R.id.InstallBtn);
		
		AppsLayout  = (LinearLayout) findViewById(R.id.LinearLayoutApp);
		VideoLayout  = (LinearLayout) findViewById(R.id.LinearLayoutVideo);
		Mp3Layout  = (LinearLayout) findViewById(R.id.LinearLayoutMp3);
		WallpaperLayout  = (LinearLayout) findViewById(R.id.LinearLayoutWallpaper);
		GameLayout  = (LinearLayout) findViewById(R.id.LinearLayoutGame);
		BookLayout  = (LinearLayout) findViewById(R.id.LinearLayoutBooks);



		AppSubTv = (TextView)findViewById(R.id.txtAppSub);
		VideoSubTv = (TextView)findViewById(R.id.txtVideoSub);
		Mp3SubTv = (TextView)findViewById(R.id.txtmp3Sub);
		GameSubTv = (TextView)findViewById(R.id.txtGameSub);
		BookSubTv = (TextView)findViewById(R.id.txtBookSub);
		WallapperSubTv = (TextView)findViewById(R.id.txtWallpaperSub);



		appTv = (TextView)findViewById(R.id.txtApp);
		videoTv = (TextView)findViewById(R.id.txtVideo);
		mp3Tv = (TextView)findViewById(R.id.txtmp3);
		gameTv = (TextView)findViewById(R.id.txtGame);
		bookTv = (TextView)findViewById(R.id.txtBook);
		wallapperTv = (TextView)findViewById(R.id.txtWallpaper);

		ContentArrayListVideo12 = new ArrayList<HashMap<String, String>>();
		ContentArrayListMp3 = new ArrayList<HashMap<String, String>>();
		ContentArrayListWallpaper = new ArrayList<HashMap<String, String>>();
		ContentArrayListApp = new ArrayList<HashMap<String, String>>();
		ContentArrayListGame = new ArrayList<HashMap<String, String>>();
		ContentArrayListBook = new ArrayList<HashMap<String, String>>();
		ContentArrayListReview = new ArrayList<HashMap<String, String>>();

		AppsProjectsList  = (HorizontalListView) findViewById(R.id.glry_Apps_HorizontalListView);
		VideoProjectsList12  = (HorizontalListView) findViewById(R.id.glry_Video_HorizontalListView);
		Mp3ProjectsList  = (HorizontalListView) findViewById(R.id.glry_mp3_HorizontalListView);
		WallpaperProjectsList  = (HorizontalListView) findViewById(R.id.glry_Wallpaper_HorizontalListView);
		GameProjectsList  = (HorizontalListView) findViewById(R.id.glry_Game_HorizontalListView);
		BookProjectsList  = (HorizontalListView) findViewById(R.id.glry_Book_HorizontalListView);

		appMoreBtn = (Button)findViewById(R.id.txtMoreApp);
		videoMoreBtn = (Button)findViewById(R.id.txtMoreVideo);
		mp3MoreBtn = (Button)findViewById(R.id.txtMoreMp3);
		gameMoreBtn = (Button)findViewById(R.id.txtMoreGame);
		bookMoreBtn = (Button)findViewById(R.id.txtMoreBook);
		wallapperMoreBtn = (Button)findViewById(R.id.txtMoreWallpaper);
		
		 Language = sharedpreferences.getString("Language", "");
	        if(Language.equals("English"))
	        {
	        	appMoreBtn.setText("More");
	        	videoMoreBtn.setText("More");
	        	mp3MoreBtn.setText("More");
	        	gameMoreBtn.setText("More");
	        	bookMoreBtn.setText("More");
	        	wallapperMoreBtn.setText("More");
	        	InstallBtn.setText("Download");
	        	
	        }
	        else
	        {
	        	appMoreBtn.setText("আরও");
	        	videoMoreBtn.setText("আরও");
	        	mp3MoreBtn.setText("আরও");
	        	gameMoreBtn.setText("আরও");
	        	bookMoreBtn.setText("আরও");
	        	wallapperMoreBtn.setText("আরও");
	        	InstallBtn.setText("ডাউনলোড");
	        }
	
	        
		RatingBarBtn = (RatingBar)findViewById(R.id.ratingBar);


//		new VideoScreenShow().execute();
		if(isInternetOn())
		{
			try {
				new VideoScreenShow().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			Toast.makeText(WallpaperDetailsActivity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
			
		}


		
		WallpaperProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub



				HashMap<String, String> data = ContentArrayListWallpaper.get(position);

				Intent idn = new Intent(WallpaperDetailsActivity.this, WallpaperDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "MainScreen");
				idn.putExtra("SubLink", "4");
				startActivity(idn);
				finish();

			}
		});
		Mp3ProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListMp3.get(position);

				Intent idn = new Intent(WallpaperDetailsActivity.this, AudioDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "MainScreen");
				idn.putExtra("SubLink", "3");
				startActivity(idn);
				finish();
			}
		});
		VideoProjectsList12.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListVideo12.get(position);

				Intent idn = new Intent(WallpaperDetailsActivity.this, VideoDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));		
				idn.putExtra("which_screen", "MainScreen");
				idn.putExtra("SubLink", "2");
				startActivity(idn);
				finish();

			}
		});

		AppsProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListApp.get(position);

				Intent idn = new Intent(WallpaperDetailsActivity.this, AppsDetailsViewActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "MainScreen");
				idn.putExtra("SubLink", "1");
				startActivity(idn);
				finish();

			}
		});

		GameProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListGame.get(position);

				Intent idn = new Intent(WallpaperDetailsActivity.this, GameDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "MainScreen");
				idn.putExtra("SubLink", "5");
				startActivity(idn);
				finish();


			}
		});

		BookProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListBook.get(position);
				Intent idn = new Intent(WallpaperDetailsActivity.this, BooksDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "MainScreen");
				idn.putExtra("SubLink", "6");
				startActivity(idn);
				finish();

			}
		});

		appMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(WallpaperDetailsActivity.this, TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "1");
				idn.putExtra("TitleName", "অ্যাপস");
				startActivity(idn);


			}
		});
		videoMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(WallpaperDetailsActivity.this, TemplateHome_InnerScreenActivity.class);
				idn.putExtra("TitleName", "ভিডিও");
				idn.putExtra("SubLink", "2");
				startActivity(idn);


			}
		});


		mp3MoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(WallpaperDetailsActivity.this, TemplateHome_InnerScreenActivity.class);
				idn.putExtra("TitleName", "গান");
				idn.putExtra("SubLink", "3");
				startActivity(idn);


			}
		});

		gameMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(WallpaperDetailsActivity.this, TemplateHome_InnerScreenActivity.class);
				idn.putExtra("TitleName", "গেমস");
				idn.putExtra("SubLink", "5");
				startActivity(idn);


			}
		});

		wallapperMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(WallpaperDetailsActivity.this, TemplateHome_InnerScreenActivity.class);
				idn.putExtra("TitleName", "ওয়ালপেপার");
				idn.putExtra("SubLink", "4");
				startActivity(idn);


			}
		});

		bookMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(WallpaperDetailsActivity.this, TemplateHome_InnerScreenActivity.class);
				idn.putExtra("TitleName", "বই");
				idn.putExtra("SubLink", "6");
				startActivity(idn);


			}
		});
		
		
		
		RatingBarBtn.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, final float rating,
				boolean fromUser) {
				/*TextView txtRatingValue = null;
				txtRatingValue.setText(String.valueOf(rating));*/
				

				
				LayoutInflater li = LayoutInflater.from(context);
				View promptsView = li.inflate(R.layout.rating_dialog, null);

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);

				// set prompts.xml to alertdialog builder
				alertDialogBuilder.setView(promptsView);

				final EditText userInputName = (EditText) promptsView
						.findViewById(R.id.editTextDialogUserInputName);
				
				final EditText userInputComment = (EditText) promptsView
						.findViewById(R.id.editTextDialogUserInputComment);

				// set dialog message
				alertDialogBuilder
					.setCancelable(false)
					.setPositiveButton("OK",
					  new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog,int id) {

					    	NameRating = userInputName.getText().toString();
					    	CommentRating = userInputComment.getText().toString();
					    	RatingNum =String.valueOf(rating);
					    	 new RatingAsyn().execute();
					    	
//					    	Toast.makeText(AppsDetailsViewActivity.this, "   "+String.valueOf(rating) +"Name :"+Name +" Comment: "+Comment, Toast.LENGTH_LONG).show();

					    }
					  })
					.setNegativeButton("Cancel",
					  new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
					    }
					  });

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});

		
		
		InstallBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Log.e("ggggggggggggggg", "gggggggggggggggggggggg"+ins);
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

//				alertDialogBuilder.setTitle("Your Title");

				alertDialogBuilder
					.setMessage("Do you want to download it by "+Original_Price+"Tk ?")
					.setCancelable(false)
					.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {				
							if(isInternetOn())
							{
								/*Log.e("HHHHHHHHHHHHHHHHHHHHH", "~~~~~~~~~~~~~~~~~~~~~~~`"+Original_Price);
								if(Original_Price.equals("0.00"))
									new Wallpaper().execute();
								else
									new BillingAsyn().execute();*/
								
								Wallpaper_DownloadingOption();
							}
							else
							{
								Toast.makeText(WallpaperDetailsActivity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
							}
							
						}
					  })
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
				
				/*Bitmap bmp = BitmapFactory.decodeFile(_content_file);
			    BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);
			    
			    WallpaperManager m=WallpaperManager.getInstance(WallpaperDetailsActivity.this);

			    try {
			        m.setBitmap(bmp);
			    } catch (IOException e) {
			        e.printStackTrace();
			    }*/
				
			
			/*	if(isInternetOn())
				{
					new Wallpaper().execute();
				}
				else
				{
					Toast.makeText(WallpaperDetailsActivity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
					
				}*/

			
			}
		});
		
	
	}

	
	 private void SearchMenuOption(String Name) {
			
			Button SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
			ImageView SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);		
			TextView NameBack = (TextView)findViewById(R.id.TitleText);	
			
			final RelativeLayout CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
			final RelativeLayout SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);
			
			NameBack.setText(Name);
			
			SearchingCategoryBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
									
					CategoryLayout.setVisibility(View.GONE);
					SearchingLayout.setVisibility(View.VISIBLE);
					new SearchAutoCompleteTextViewList().execute();
					
				}
			});
			
			SearchingBackBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
										
					CategoryLayout.setVisibility(View.VISIBLE);
					SearchingLayout.setVisibility(View.GONE);
					
					
				}
			});
			
			NameBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					finish();
				}
			});
			
		}
	 
	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "1");
				idn.putExtra("TitleName", "অ্যাপস");
				startActivity(idn);
//				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "2");
				idn.putExtra("TitleName", "ভিডিও");
				startActivity(idn);
//				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "3");
				idn.putExtra("TitleName", "গান");
				startActivity(idn);
//				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "4");
				idn.putExtra("TitleName", "ওয়ালপেপার");
				startActivity(idn);
//				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "5");
				idn.putExtra("TitleName", "গেমস");
				startActivity(idn);
//				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "6");
				idn.putExtra("TitleName", "বই");
				startActivity(idn);
//				finish();
			}
		});
		
		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
				idn.putExtra("SubLink", "1000");
				idn.putExtra("TitleName", "সর্বশেষ সংবাদ");
				startActivity(idn);
//				finish();
			}
		});


	}


	
	
	class VideoScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(WallpaperDetailsActivity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				//				String url=URLs.DOMAIN_DETAILS+"10";
				String url=URLs.DOMAIN_DETAILS+_Content_Id+Language;

				response12 = JsonParse.makeServiceCall(url);
				//				response = "{"+'"'+"Category"+'"'+":"+response12+"}";
				response = "{"+'"'+"Category"+'"'+":["+response12+"]}";
				Log.e("---------response-------------", response);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Category"+'"'+":}";
					//					Log.e("---------respon-------------", response12);

					if (!response.equals(respon)) 
						//						if (!response12.equals("")) 
					{



						JSONObject jsonObj = new JSONObject(MyResponse);

						Log.e("---------jsonObj-------------", ""+jsonObj);

						JSONArray ContentList = jsonObj.getJSONArray("Category");

						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cat = ContentList.getJSONObject(i);



							String content_details12 = cat.getString("content_details");

							String suggestion_content12 = cat.getString("suggestion_content");

							String review_list12 = cat.getString("review_list");



							Log.e("---------content_details12-------------", content_details12);
							Log.e("---------suggestion_content12-------------", suggestion_content12);
							Log.e("---------review_list12-------------", review_list12);


							//							String Content_Details_Response = "{"+'"'+"content_details"+'"'+":["+content_details12+"]}";
							String Content_Details_Response = "{"+'"'+"content_details"+'"'+":"+content_details12+"}";
							//							String Suggestion_Content_Response = "{"+'"'+"suggestion_content"+'"'+":"+suggestion_content12+"}";
							String Suggestion_Content_Response = "{"+'"'+"suggestion_content"+'"'+":"+suggestion_content12+"}";
							
							String Review_Content_Response = "{"+'"'+"review_list"+'"'+":"+review_list12+"}";


							Log.e("---------Content_Details_Response-------------", Content_Details_Response);
							Log.e("---------Suggestion_Content_Response-------------", Suggestion_Content_Response);
							Log.e("---------Review_Content_Response-------------", Review_Content_Response);


							//							    JSONObject contnt = jsonObj.getJSONObject("content_details");

							JSONObject Details_ContentJsonObj = new JSONObject(Content_Details_Response);

							JSONObject contnt = Details_ContentJsonObj.getJSONObject("content_details");

							 _content_id12 = contnt.getString("id");
							 _content_title12 = contnt.getString("content_title");
							String _content_description = contnt.getString("content_description");
							_preview = contnt.getString("preview");
							_banner = contnt.getString("banner");
							String _total_download = contnt.getString("total_download");
							String _payment_type = contnt.getString("payment_type");
							String _price = contnt.getString("price");
							Original_Price = contnt.getString("original_price");
							String _company_name = contnt.getString("company_name");
							String _developer_name = contnt.getString("developer_name");
							String _category_name = contnt.getString("category_name");
							String _rating = contnt.getString("rating");

							Log.e("AAAAAAAAAAAAAAAAa", "AAAAAAAAAAAAAAAAAAAAAA  _content_title  "+_content_title12);

							JSONObject cont = contnt.getJSONObject("content");
							_content_file = cont.getString("content_file");
							_embedded_url_type = cont.getString("embedded_url_type");
							_embedded_url = cont.getString("embedded_url");

							NameTv.setText(_content_title12);
//							DownloadTv.setText("ডাউনলোড: "+_total_download);
							Company_NameTv.setText(_company_name);
							Developer_NameTv.setText(_developer_name);
//							PriceTv.setText("মূল�?য: "+_price);
							DescriptionTv.setText(_content_description);
							
							if(Language.equals("English"))
					        {
								DownloadTv.setText("Download: "+_total_download);
					        	PriceTv.setText("Price: "+_price);
					        }
					        else
					        {
					        	DownloadTv.setText("ডাউনলোড: "+_total_download);
					        	PriceTv.setText("মূল্য: "+_price);
					        }

							SearchMenuOption(_content_title12);
							
							imageLoader.DisplayImage(_banner, Banner_Image);
							imageLoader.DisplayImage(_preview, Logo_Image);

							/*
							String Screen_shot = contnt.getString("screenshot");

							Log.e("TTTTTTTTTTTTTTTTTTTTTTT", "TTTTTTTTTTTTTTTTT"+Screen_shot);
							
							if(!Screen_shot.equals("[]"))
							{
							       JSONObject ScreenCont = contnt.getJSONObject("screenshot");
						        	Log.e("TTTTTTTTTTTTTTTTTTTTTTT", "TTTTTTTTTTTTTTTTT"+ScreenCont.length());
							
							
							
								ScreenShotProjectsList.setVisibility(View.VISIBLE);
								for (int i2 = 1; i2 <= ScreenCont.length(); i2++)
								{
									String screenshot_ = ScreenCont.getString("screenshot_"+i2);
									Log.e("AAAAAAAAAAAAAAAAa", "AAAAAAAAAAAAAAAAAAAAAA  screenshot_  "+screenshot_);
									HashMap<String, String> Info = new HashMap<String, String>();
									Info.put("My_Screenshot", screenshot_);

									ContentArrayListScreenShot.add(Info);

								}

								adapter=new AdapterForApps(VideoDetailsActivity.this, ContentArrayListScreenShot);
								ScreenShotProjectsList.setAdapter(adapter);
							}*/
							
							
							
							if(!review_list12.equals("[]"))
							{
								LinearLayoutReviewShow.setVisibility(View.VISIBLE);
								
								JSONObject Review_ContentJsonObj = new JSONObject(Review_Content_Response);
								JSONArray Review_Content_List = Review_ContentJsonObj.getJSONArray("review_list");
								
								for (int j1 = 0; j1 < Review_Content_List.length(); j1++)
								{

									JSONObject cateReview = Review_Content_List.getJSONObject(j1);

									String user_name = cateReview.getString("user_name");
									String review_text = cateReview.getString("review_text");
									String rating = cateReview.getString("rating");
									
									Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+user_name);
									Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+review_text);
									Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+rating);
									
									HashMap<String, String> InfoReview = new HashMap<String, String>();
									InfoReview.put("user_name", user_name);
									InfoReview.put("review_text", review_text);
									InfoReview.put("rating", rating);
									ContentArrayListReview.add(InfoReview);
								}
							}
							
							
							
						
						


							JSONObject Suggestion_ContentJsonObj = new JSONObject(Suggestion_Content_Response);
							JSONArray Suggestion_Content_List = Suggestion_ContentJsonObj.getJSONArray("suggestion_content");

							for (int i1 = 0; i1 < Suggestion_Content_List.length(); i1++)
							{

								JSONObject cateHome = Suggestion_Content_List.getJSONObject(i1);

								String _id = cateHome.getString("type_id");
								String _type_name = cateHome.getString("type_name");
								String _short_description = cateHome.getString("short_description");


								String ContentResponse = cateHome.getString("content");



								if(_id.equals("1"))
									AppsLayout.setVisibility(View.VISIBLE);



								if(_id.equals("2"))
									VideoLayout.setVisibility(View.VISIBLE);



								if(_id.equals("3"))
									Mp3Layout.setVisibility(View.VISIBLE);



								if(_id.equals("4"))
									WallpaperLayout.setVisibility(View.VISIBLE);



								if(_id.equals("5"))
									GameLayout.setVisibility(View.VISIBLE);



								if(_id.equals("6"))
									BookLayout.setVisibility(View.VISIBLE);



								//									TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv;

								if(_id.equals("1"))
								{
									appTv.setText(_type_name);
									AppSubTv.setText(_short_description);
								}

								if(_id.equals("2"))
								{
									videoTv.setText(_type_name);
									VideoSubTv.setText(_short_description);
								}

								if(_id.equals("3"))
								{
									mp3Tv.setText(_type_name);
									Mp3SubTv.setText(_short_description);
								}

								if(_id.equals("4"))
								{
									wallapperTv.setText(_type_name);
									WallapperSubTv.setText(_short_description);
								}

								if(_id.equals("5"))
								{
									gameTv.setText(_type_name);
									GameSubTv.setText(_short_description);
								}

								if(_id.equals("6"))
								{
									bookTv.setText(_type_name);
									BookSubTv.setText(_short_description);
								}

								if(!ContentResponse.equals("[]"))
								{

									HashMap<String, String> Info = new HashMap<String, String>();
									Info.put("id", _id);
									Info.put("type_name", _type_name);
									Info.put("short_description", _short_description);



									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_id);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_type_name);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_short_description);


									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";

									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);

									JSONArray Content_List = ContentJsonObj.getJSONArray("content");

									// content node is JSON Object

									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject c = Content_List.getJSONObject(j);

										String _content_id = c.getString("id");
										String _content_title = c.getString("content_title");
										//											String _content_description = c.getString("content_description");
										String _preview = c.getString("preview");
										//											String _banner = c.getString("banner");
										/*	String _total_download = c.getString("total_download");
											String _payment_type = c.getString("payment_type");
											String _price = c.getString("price");
											String _rating = c.getString("rating");*/
										/*	String _company_name = c.getString("company_name");
											String _developer_name = c.getString("developer_name");
											String _category_name = c.getString("category_name");*/

										HashMap<String, String> Content12 = new HashMap<String, String>();


										String type_name = Info.get("type_name");
										String type_id = Info.get("id");
										Log.e("AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC", "AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC"+type_name);

										if(type_id.equals("1"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//												Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//												Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
												Content12.put("developer_name", _developer_name);
												Content12.put("category_name", _category_name);*/



											ContentArrayListApp.add(Content12);

										}


										if(type_id.equals("2"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//												Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//												Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
												Content12.put("developer_name", _developer_name);
												Content12.put("category_name", _category_name);*/



											ContentArrayListVideo12.add(Content12);

										}

										if(type_id.equals("3"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//												Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//												Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
												Content12.put("developer_name", _developer_name);
												Content12.put("category_name", _category_name);*/



											ContentArrayListMp3.add(Content12);

										}



										if(type_id.equals("4"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//												Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//												Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
												Content12.put("developer_name", _developer_name);
												Content12.put("category_name", _category_name);*/


											ContentArrayListWallpaper.add(Content12);

										}

										if(type_id.equals("5"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//												Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//												Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
												Content12.put("developer_name", _developer_name);
												Content12.put("category_name", _category_name);*/


											ContentArrayListGame.add(Content12);
										}

										if(type_id.equals("6"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//												Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//												Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
												Content12.put("developer_name", _developer_name);
												Content12.put("category_name", _category_name);*/


											ContentArrayListBook.add(Content12);

										}


									}
									//		                      InfoList.add(Info);
								}
								else
								{
									Log.e("ServiceHandler", "Couldn't get any data from the content");

								}




							}


						}




						adapterVideo=new AdapterForMainScreen(WallpaperDetailsActivity.this, ContentArrayListVideo12);
						adapterMp3=new AdapterForMainScreen(WallpaperDetailsActivity.this, ContentArrayListMp3);
						adapterWallpaper=new AdapterForMainScreen(WallpaperDetailsActivity.this, ContentArrayListWallpaper);
						adapterApp=new AdapterForMainScreen(WallpaperDetailsActivity.this, ContentArrayListApp);
						adapterGame=new AdapterForMainScreen(WallpaperDetailsActivity.this, ContentArrayListGame);
						adapterBook=new AdapterForMainScreen(WallpaperDetailsActivity.this, ContentArrayListBook);
						adapterRatingReview=new AdapterForRatingReview(WallpaperDetailsActivity.this, ContentArrayListReview);

						WallpaperProjectsList.setAdapter(adapterWallpaper);
						VideoProjectsList12.setAdapter(adapterVideo);
						Mp3ProjectsList.setAdapter(adapterMp3);
						AppsProjectsList.setAdapter(adapterApp);
						GameProjectsList.setAdapter(adapterGame);
						BookProjectsList.setAdapter(adapterBook);
						RatingReviewProjectsList.setAdapter(adapterRatingReview);


					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(WallpaperDetailsActivity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			pDialog.dismiss();



		}

	}
	
	
	class RatingAsyn extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			
			pDialog = new ProgressDialog(WallpaperDetailsActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			String final_json = "";

			String url=URLs.AddReview;
			JSONObject jsonObject12 = new JSONObject();

			
			try {
				
				jsonObject12.accumulate("content_id", _Content_Id);
				jsonObject12.accumulate("user_name", NameRating);
				jsonObject12.accumulate("review_text", CommentRating);
				jsonObject12.accumulate("rating", RatingNum);
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String	my_final_json = jsonObject12.toString().substring(1, jsonObject12.toString().length()-1)+",";
			String my_final_json1 = my_final_json.substring(0, my_final_json.length()-1);
			final_json = "{"+my_final_json1+"}";
			Log.e("Subroto","json :"+final_json);


			JSONResponse_Type = JsonParse.makeServiceCall(url,final_json);
			Log.e("Subroto","response Tour User: "+JSONResponse_Type);

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {


						parseXmlresponseForProductCatagory(JSONResponse_Type);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String response) throws JSONException 
				{


					if (response != null) 
					{

						JSONObject jsonObj = new JSONObject(response);

						// Getting JSON Array node
						JSONArray TypArray = jsonObj.getJSONArray("result");

						// looping through All Contacts
						for (int i = 0; i < TypArray.length(); i++) {
							JSONObject c = TypArray.getJSONObject(i);

							String status = c.getString("status");
							String message = c.getString("message");


						
							Log.e("%%%%%%%%%%%%%%%%%%%%", "%%%%%%%%%%%%%%%%%%%%"+message);

						}

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

					

				}


			});

			pDialog.dismiss();
		}
	}
	
	
	
	class Wallpaper extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog12 = new ProgressDialog(WallpaperDetailsActivity.this);
			pDialog12.setMessage(" Please wait.....");
			pDialog12.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog12.setIndeterminate(false);
			pDialog12.setCancelable(false);
			pDialog12.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {

				  URL myFileUrl = null;
				    try {
				        myFileUrl = new URL(_content_file);
				    } catch (MalformedURLException e) {
				        // TODO Auto-generated catch block
				        e.printStackTrace();
				    }
				    try {
				        HttpURLConnection conn = (HttpURLConnection) myFileUrl
				                .openConnection();
				        conn.setDoInput(true);
				        conn.connect();
				        int length = conn.getContentLength();

				        InputStream is = conn.getInputStream();

				        bmImg = BitmapFactory.decodeStream(is);
				        // this.imView.setImageBitmap(bmImg);
				    } catch (IOException e) {
				        // TODO Auto-generated catch block
				        e.printStackTrace();
				    }
				

			} catch (Exception e) { }

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					 microsenconds = TimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis());
			
					
					 try {
//					        String filepath=Environment.getExternalStorageDirectory().getAbsolutePath(); 
					        String filepath = Environment.getExternalStorageDirectory()+ "/sdcard/Download/"; // Path to where you want to save the file
					        FileOutputStream fos = new FileOutputStream(filepath + "/" +_content_title12+microsenconds+".jpg"); 
					        bmImg.compress(CompressFormat.JPEG, 75, fos);
					        fos.flush();
					        fos.close();
					       
					        Context context = WallpaperDetailsActivity.this;
//					        WallpaperManager wpm = WallpaperManager.getInstance(context);
//					        wpm.setBitmap(bmImg);
					        context.setWallpaper(bmImg);
					        
					        
					        new AlertDialog.Builder(WallpaperDetailsActivity.this)
//							.setTitle("set wallpaper")
							.setMessage("Wallpaper has set on the screen")
							.setCancelable(false)
							.setPositiveButton("ok", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub


								}



							}).create().show();
					        
					    } catch (Exception e) {
					        //Log.e("MyLog", e.toString());
					       
					    }



				}

				});
			
			pDialog12.dismiss();
//			Toast.makeText(WallpaperDetailsActivity.this, Bill_Message+"\nIt is set on the screen", Toast.LENGTH_LONG).show();



		}

	}
	
	
	
	class Download extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog12 = new ProgressDialog(WallpaperDetailsActivity.this);
			pDialog12.setMessage(" Please wait.....");
			pDialog12.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog12.setIndeterminate(false);
			pDialog12.setCancelable(false);
			pDialog12.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {

				  URL myFileUrl = null;
				    try {
				        myFileUrl = new URL(_content_file);
				    } catch (MalformedURLException e) {
				        // TODO Auto-generated catch block
				        e.printStackTrace();
				    }
				    try {
				        HttpURLConnection conn = (HttpURLConnection) myFileUrl
				                .openConnection();
				        conn.setDoInput(true);
				        conn.connect();
				        int length = conn.getContentLength();

				        InputStream is = conn.getInputStream();

				        bmImg = BitmapFactory.decodeStream(is);
				        // this.imView.setImageBitmap(bmImg);
				    } catch (IOException e) {
				        // TODO Auto-generated catch block
				        e.printStackTrace();
				    }
				

			} catch (Exception e) { }

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					 microsenconds = TimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis());
					
					
					 try {
//					        final String filepath=Environment.getExternalStorageDirectory().getAbsolutePath(); 
					        String filepath = Environment.getExternalStorageDirectory()+ "/sdcard/Download/"; // Path to where you want to save the file
					        Log.e("TTTTTTTTTTTTTTTTTTtttt", "HHHHHHHHHHHHHHHHHHHHHHHH"+filepath);
					        FileOutputStream fos = new FileOutputStream(filepath + "/" +_content_title12+microsenconds+".jpg"); 
					        bmImg.compress(CompressFormat.JPEG, 75, fos);
					        fos.flush();
					        fos.close();
					       
					        new AlertDialog.Builder(WallpaperDetailsActivity.this)
							.setTitle("Download Path")
							.setMessage(filepath+"/" +_content_title12+microsenconds+".jpg")
							.setCancelable(false)
							.setPositiveButton("ok", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub


								}



							}).create().show();
					        
//					        Context context = WallpaperDetailsActivity.this;
//					        WallpaperManager wpm = WallpaperManager.getInstance(context);
//					        wpm.setBitmap(bmImg);
//					        context.setWallpaper(bmImg);
					    } catch (Exception e) {
					        //Log.e("MyLog", e.toString());
					       
					    }



				}

				});
			
			pDialog12.dismiss();
			



		}

	}
	
	
	
	class SearchAutoCompleteTextViewList extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			SearchAutoComplete_ID.clear();
			SearchAutoComplete_Name.clear();
			
			
		}

		@Override
		protected String doInBackground(String... args) {
			

			try {
				String url=URLs.Search_Keyword;
				//				String response12 = JsonParse.makeServiceCall(url);
				responseSearch = JsonParse.makeServiceCall(url);

				response = "{"+'"'+"Category"+'"'+":"+responseSearch+"}";
				Log.e("---------response-------------", responseSearch);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {


						parseXmlresponseForProductCatagory(response);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String MyResponse) throws JSONException 
				{


					if (response != null) 
					{
						
						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);

							String _id = cate.getString("id");
							String _keyword_name = cate.getString("keyword");
							
							SearchAutoComplete_ID.add(_id);
							SearchAutoComplete_Name.add(_keyword_name);
						
						}

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(WallpaperDetailsActivity.this,android.R.layout.simple_spinner_item, SearchAutoComplete_Name);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					SearchAutoCompleteTextView.setAdapter(dataAdapter);
					
				}


			});
			
			
			SearchAutoCompleteTextView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SearchAutoCompleteTextView.showDropDown();
				}
			});
			SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					
					TempData.SEARCH_ID=SearchAutoComplete_ID.get(arg2);
					Log.e("IDDDDD", String.valueOf(SearchAutoComplete_ID.get(arg2)));
				Intent in=new Intent(WallpaperDetailsActivity.this,SearchDetails.class);
				startActivity(in);
				
				
				}
			});
		}
	}

	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			
			
			if(Which_Screen.equals("MainScreen"))
			{
				/*Intent idd = new Intent(WallpaperDetailsActivity.this, MainActivity_Static.class);
				startActivity(idd);*/
				finish();
			}
			if(Which_Screen.equals("SearchDetails"))
			{
				finish();
			}
			
			if(Which_Screen.equals("sub_inner"))
			{
				/*Intent idd = new Intent(WallpaperDetailsActivity.this, CategorywiseWallpaperDetailsActivity.class);
				startActivity(idd);*/
				finish();
			}
			 if(Which_Screen.equals("InnerScreen"))
				{
					/*Intent idd = new Intent(WallpaperDetailsActivity.this, CategorywiseWallpaperDetailsActivity.class);
					startActivity(idd);*/
					finish();
				}
			
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
	
	class BillingAsyn extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(WallpaperDetailsActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			String final_json = "";

			String url=URLs.Billing;
			JSONObject jsonObject12 = new JSONObject();
			
			try {

				jsonObject12.accumulate("amount", Original_Price);
				jsonObject12.accumulate("subscriberId", sharedpreferences.getString("MobileNumberForPurchase", null));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String	my_final_json = jsonObject12.toString().substring(1, jsonObject12.toString().length()-1)+",";
			String my_final_json1 = my_final_json.substring(0, my_final_json.length()-1);
			final_json = "{"+my_final_json1+"}";
			Log.e("Subroto","json :"+final_json);


			JSONResponse_Type = JsonParse.makeServiceCall(url,final_json);

			JSONResponse_Type = "{"+'"'+"result"+'"'+":["+JSONResponse_Type+"]}";

			Log.e("Subroto","response Tour User: "+JSONResponse_Type);

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {

						parseXmlresponseForProductCatagory(JSONResponse_Type);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String response) throws JSONException 
				{

					if (response != null) 
					{
						JSONObject jsonObj = new JSONObject(response);

						// Getting JSON Array node
						JSONArray TypArray = jsonObj.getJSONArray("result");

						// looping through All Contacts
						for (int i = 0; i < TypArray.length(); i++) {
							JSONObject c = TypArray.getJSONObject(i);

							Bill_Status = c.getString("status");
							Bill_Message = c.getString("message");
						}
					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

				}

			});

			pDialog.dismiss();

			if(Bill_Status.equals("success"))
			{
				new Wallpaper().execute();
			}
			else
			{
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WallpaperDetailsActivity.this);

				//						alertDialogBuilder.setTitle("");
				alertDialogBuilder
				.setMessage(Bill_Message)
				.setCancelable(false)
				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}

		}
	}
	
	
	class DownloadBillingAsyn extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(WallpaperDetailsActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			String final_json = "";

			String url=URLs.Billing;
			JSONObject jsonObject12 = new JSONObject();
			
			try {

				jsonObject12.accumulate("amount", Original_Price);
				jsonObject12.accumulate("subscriberId", sharedpreferences.getString("MobileNumberForPurchase", null));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String	my_final_json = jsonObject12.toString().substring(1, jsonObject12.toString().length()-1)+",";
			String my_final_json1 = my_final_json.substring(0, my_final_json.length()-1);
			final_json = "{"+my_final_json1+"}";
			Log.e("Subroto","json :"+final_json);


			JSONResponse_Type = JsonParse.makeServiceCall(url,final_json);

			JSONResponse_Type = "{"+'"'+"result"+'"'+":["+JSONResponse_Type+"]}";

			Log.e("Subroto","response Tour User: "+JSONResponse_Type);

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {

						parseXmlresponseForProductCatagory(JSONResponse_Type);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String response) throws JSONException 
				{

					if (response != null) 
					{
						JSONObject jsonObj = new JSONObject(response);

						// Getting JSON Array node
						JSONArray TypArray = jsonObj.getJSONArray("result");

						// looping through All Contacts
						for (int i = 0; i < TypArray.length(); i++) {
							JSONObject c = TypArray.getJSONObject(i);

							Bill_Status = c.getString("status");
							Bill_Message = c.getString("message");
						}
					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

				}

			});

			pDialog.dismiss();

			if(Bill_Status.equals("success"))
			{
				new Download().execute();
			}
			else
			{
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WallpaperDetailsActivity.this);

				//						alertDialogBuilder.setTitle("");
				alertDialogBuilder
				.setMessage(Bill_Message)
				.setCancelable(false)
				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}

		}
	}
	
/*private void ViewInitializationAndActionofMenu(String ID) {
		
		TextView AppBtn1 = (TextView)findViewById(R.id.AppBtn1);
		TextView VideoBtn1 = (TextView)findViewById(R.id.VideoBtn1);
		TextView AudioBtn1 = (TextView)findViewById(R.id.AudioBtn2);
		TextView WallpaperBtn1 = (TextView)findViewById(R.id.WallpaperBtn2);
		TextView GameBtn1 = (TextView)findViewById(R.id.GameBtn2);
		TextView BookBtn1 = (TextView)findViewById(R.id.BookBtn2);
		TextView LatestNewsBtn1 = (TextView)findViewById(R.id.LatestNewsBtn2);
		
		RelativeLayout App_RelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout Video_RelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout MP3_RelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout Game_RelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout Wallpaper_RelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout Book_RelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNews_RelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		if(ID.equals("1"))
		{
			AppBtn1.setTextColor(color.ActionBar_Orange);
			App_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}			
		else if(ID.equals("2"))
		{
			VideoBtn1.setTextColor(color.ActionBar_Orange);
			Video_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("3"))
		{
			AudioBtn1.setTextColor(color.ActionBar_Orange);
			MP3_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("4"))
		{
			WallpaperBtn1.setTextColor(color.ActionBar_Orange);
			Wallpaper_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("5"))
		{
			GameBtn1.setTextColor(color.ActionBar_Orange);
			Game_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("6"))
		{
			BookBtn1.setTextColor(color.ActionBar_Orange);
			Book_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("1000"))
		{
			LatestNewsBtn1.setTextColor(color.ActionBar_Orange);
			LatestNews_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		
		
		
	}*/


private void Wallpaper_DownloadingOption() {
	
	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WallpaperDetailsActivity.this);

		// set title
//		alertDialogBuilder.setTitle("Your Title");

		// set dialog message
		alertDialogBuilder
			.setMessage("Do you want to download/set wallpaper?")
			.setCancelable(false)
			.setPositiveButton("Set Wallpaper",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity
				
					Log.e("HHHHHHHHHHHHHHHHHHHHH", "~~~~~~~~~~~~~~~~~~~~~~~`"+Original_Price);
					if(Original_Price.equals("0.00"))
						new Wallpaper().execute();
					else
					{
						if(sharedpreferences.getString("MobileNumberForPurchase", null).equals("no") && sharedpreferences.getString("PINCODE", null).equals("no"))
						{
							SharedPreferences.Editor editor = sharedpreferences.edit();
							editor.putString("DownloadingScreen", "WallpaperDetailsActivity");
							editor.commit(); 
							
							/*Content_Id = b.getString("Content_Id");
							Which_Screen = b.getString("which_screen");
							TitleName = b.getString("TitleName");
							SubLink = b.getString("SubLink");*/
							
							Intent idn = new Intent(WallpaperDetailsActivity.this, NewNumberLoginScreenActivity.class);
							idn.putExtra("Content_Id", _Content_Id);
							idn.putExtra("Which_Screen", Which_Screen);
							idn.putExtra("TitleName", "Wallpaper");
							idn.putExtra("SubLink", SubLink);
							startActivity(idn);
						}
						else
						new BillingAsyn().execute();
					}
						
					
					new DownloadingCountAsyn().execute();
				}
			  })
			.setNegativeButton("Download",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
//					dialog.cancel();
					
					Log.e("HHHHHHHHHHHHHHHHHHHHH", "~~~~~~~~~~~~~~~~~~~~~~~`"+Original_Price);
					if(Original_Price.equals("0.00"))
						new Download().execute();
					else
					{
						if(sharedpreferences.getString("MobileNumberForPurchase", null).equals("no") && sharedpreferences.getString("PINCODE", null).equals("no"))
						{
							SharedPreferences.Editor editor = sharedpreferences.edit();
							editor.putString("DownloadingScreen", "AppsDetailsViewActivity");
							editor.commit(); 
							
							/*Content_Id = b.getString("Content_Id");
							Which_Screen = b.getString("which_screen");
							TitleName = b.getString("TitleName");
							SubLink = b.getString("SubLink");*/
							
							Intent idn = new Intent(WallpaperDetailsActivity.this, NewNumberLoginScreenActivity.class);
							idn.putExtra("Content_Id", Content_Id);
							idn.putExtra("Which_Screen", Which_Screen);
							idn.putExtra("TitleName", "Wallpaper");
							idn.putExtra("SubLink", SubLink);
							startActivity(idn);
						}
						else
							new DownloadBillingAsyn().execute();
					}
						
					
					new DownloadingCountAsyn().execute();
				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
}

class DownloadingCountAsyn extends AsyncTask<String, String, String> {
	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		/*pDialog = new ProgressDialog(MainActivity_Static.this);
		pDialog.setMessage("Please wait...");
		pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();*/
	}

	@Override
	protected String doInBackground(String... args) {
		String final_json = "";

		String url=URLs.DownloadingCount;
		JSONObject jsonObject12 = new JSONObject();

		String lat_long = lat+","+lng;

		try {

			jsonObject12.accumulate("mobile_no", sharedpreferences.getString("MobileNumberForPurchase", null));
			jsonObject12.accumulate("content_id", _content_id12);



		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String	my_final_json = jsonObject12.toString().substring(1, jsonObject12.toString().length()-1)+",";
		String my_final_json1 = my_final_json.substring(0, my_final_json.length()-1);
		final_json = "{"+my_final_json1+"}";
		Log.e("Subroto","json :"+final_json);


		JSONResponse_Type = JsonParse.makeServiceCall(url,final_json);
		Log.e("Subroto","response Tour User: "+JSONResponse_Type);

		return null;
	}

	@Override
	protected void onPostExecute(String file_url) {
		runOnUiThread(new Runnable() {
			@SuppressLint("SetJavaScriptEnabled")
			@Override
			public void run() {

				try {


					parseXmlresponseForProductCatagory(JSONResponse_Type);

				} catch (Exception e) { }

			}

			private void parseXmlresponseForProductCatagory(String response) throws JSONException 
			{


				if (response != null) 
				{

					JSONObject jsonObj = new JSONObject(response);

					// Getting JSON Array node
					JSONArray TypArray = jsonObj.getJSONArray("result");

					// looping through All Contacts
					for (int i = 0; i < TypArray.length(); i++) {
						JSONObject c = TypArray.getJSONObject(i);

						String status = c.getString("status");
//						String message = c.getString("message");



//						Log.e("%%%%%%%%%%%%%%%%%%%%", "%%%%%%%%%%%%%%%%%%%%"+status);

					}


				}
				else 
				{
					Log.e("ServiceHandler", "Couldn't get any data from the url");
				}



			}


		});

		//			pDialog.dismiss();
	}
}
}
