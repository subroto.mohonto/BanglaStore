package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.AdapterForMainScreenLatest;
import com.arena.adapter.AdapterLatestForMainScreen;
import com.arena.bangla.store.MainActivity.MainScreenShow;
import com.arena.image.loader.ImageLoader;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import com.devsmart.android.ui.HorizontalListView;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Layout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class CategorywiseGamesDetailsActivity extends Activity{
	
	String lat = "";
	String lng = "";
	

	ProgressDialog pDialog;
	String response;
	String response12;
	AdapterForMainScreen adapter, adapter2, adapter3, adapter4,adapter5,adapter6;
	AdapterLatestForMainScreen adapterLatest;

	ArrayList<HashMap<String, String>> InfoList;
	ArrayList<HashMap<String, String>> ContentArrayListVideos;
	ArrayList<HashMap<String, String>> ContentArrayListMp3;
	ArrayList<HashMap<String, String>> ContentArrayListWallpaper;
	ArrayList<HashMap<String, String>> ContentArrayListApp;
	ArrayList<HashMap<String, String>> ContentArrayListGame;
	ArrayList<HashMap<String, String>> ContentArrayListBook;
	ArrayList<HashMap<String, String>> ContentArrayListLatest;
	
	ListView mainsListview;

//	Button SearchingCategoryBtn, SearchingSearchingBtn, CategoryBtn;
//	Button AppBtn, SearchingCategoryBtn, SearchingSearchingBtn, CategoryBtn,VideoBtn, AudioBtn,WallpaperBtn, GameBtn, BooksBtn;
	RelativeLayout App_RelativeLayout,Video_RelativeLayout,Audio_RelativeLayout, Wallpaper_RelativeLayout,Game_RelativeLayout,Books_RelativeLayout,LatestNews_RelativeLayout;
//	ImageView SearchingBackBtn;
	
	
	
	HorizontalListView AppsProjectsList, VideoProjectsList, Mp3ProjectsList, WallpaperProjectsList, GameProjectsList, BookProjectsList;

	LinearLayout AppsLayout,VideoLayout, Mp3Layout, WallpaperLayout, GameLayout, BookLayout;
	TextView appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv,LatestTv;
	
	TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv;
	
	Button appMoreBtn, videoMoreBtn, mp3MoreBtn, gameMoreBtn, bookMoreBtn, wallapperMoreBtn;
//	AutoCompleteTextView SearchAutoCompleteTextView;
	
//	ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
//	ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();
	

	public ImageLoader imageLoader; 
	
//	ImageView BannerImage;
//	String Top_banner_image,Top_banner_content_id="",Top_banner_content_type_id="";
	ScrollView bgSc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.categorywise_video_detais);

	

	
		imageLoader=new ImageLoader(CategorywiseGamesDetailsActivity.this);
		
//		BannerImage = (ImageView)findViewById(R.id.glry_Latest_Image);
		
		AppsProjectsList  = (HorizontalListView) findViewById(R.id.glry_Apps_HorizontalListView);
		VideoProjectsList  = (HorizontalListView) findViewById(R.id.glry_Video_HorizontalListView);
		Mp3ProjectsList  = (HorizontalListView) findViewById(R.id.glry_mp3_HorizontalListView);
		WallpaperProjectsList  = (HorizontalListView) findViewById(R.id.glry_Wallpaper_HorizontalListView);
		GameProjectsList  = (HorizontalListView) findViewById(R.id.glry_Game_HorizontalListView);
		BookProjectsList  = (HorizontalListView) findViewById(R.id.glry_Book_HorizontalListView);
		
		Scrolling();

		AppsLayout  = (LinearLayout) findViewById(R.id.LinearLayoutApp);
		VideoLayout  = (LinearLayout) findViewById(R.id.LinearLayoutVideo);
		Mp3Layout  = (LinearLayout) findViewById(R.id.LinearLayoutMp3);
		WallpaperLayout  = (LinearLayout) findViewById(R.id.LinearLayoutWallpaper);
		GameLayout  = (LinearLayout) findViewById(R.id.LinearLayoutGame);
		BookLayout  = (LinearLayout) findViewById(R.id.LinearLayoutBooks);
//		LatestLayout  = (LinearLayout) findViewById(R.id.LinearLayoutLatest);
		
//		TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv;
		AppSubTv = (TextView)findViewById(R.id.txtAppSub);
		VideoSubTv = (TextView)findViewById(R.id.txtVideoSub);
		Mp3SubTv = (TextView)findViewById(R.id.txtmp3Sub);
		GameSubTv = (TextView)findViewById(R.id.txtGameSub);
		BookSubTv = (TextView)findViewById(R.id.txtBookSub);
		WallapperSubTv = (TextView)findViewById(R.id.txtWallpaperSub);
//		LatestSubTv = (TextView)findViewById(R.id.txtLatestSub);
//		LatestSubTv.setVisibility(View.GONE);
		
		appTv = (TextView)findViewById(R.id.txtApp);
		videoTv = (TextView)findViewById(R.id.txtVideo);
		mp3Tv = (TextView)findViewById(R.id.txtmp3);
		gameTv = (TextView)findViewById(R.id.txtGame);
		bookTv = (TextView)findViewById(R.id.txtBook);
		wallapperTv = (TextView)findViewById(R.id.txtWallpaper);
		LatestTv = (TextView)findViewById(R.id.txtLatest);
		
		
		appMoreBtn = (Button)findViewById(R.id.txtMoreApp);
		videoMoreBtn = (Button)findViewById(R.id.txtMoreVideo);
		mp3MoreBtn = (Button)findViewById(R.id.txtMoreMp3);
		gameMoreBtn = (Button)findViewById(R.id.txtMoreGame);
		bookMoreBtn = (Button)findViewById(R.id.txtMoreBook);
		wallapperMoreBtn = (Button)findViewById(R.id.txtMoreWallpaper);
		
//		SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
//		SearchAutoCompleteTextView.setThreshold(1);

		ContentArrayListVideos = new ArrayList<HashMap<String, String>>();
		ContentArrayListMp3 = new ArrayList<HashMap<String, String>>();
		ContentArrayListWallpaper = new ArrayList<HashMap<String, String>>();
		ContentArrayListApp = new ArrayList<HashMap<String, String>>();
		ContentArrayListGame = new ArrayList<HashMap<String, String>>();
		ContentArrayListBook = new ArrayList<HashMap<String, String>>();
		ContentArrayListLatest = new ArrayList<HashMap<String, String>>();
		
		
		InfoList = new ArrayList<HashMap<String, String>>();

		mainsListview = (ListView)findViewById(R.id.mainsListview);
		
//		SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
//		SearchingSearchingBtn = (Button)findViewById(R.id.SearchingSearchingBtn);
//		CategoryBtn = (Button)findViewById(R.id.category);
		
//		SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);


//		CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
//		SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);


		ContentArrayListVideos = new ArrayList<HashMap<String, String>>();
		InfoList = new ArrayList<HashMap<String, String>>();

		mainsListview = (ListView)findViewById(R.id.mainsListview);
		
		App_RelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		Video_RelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		Audio_RelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		Wallpaper_RelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		Game_RelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		Books_RelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		LatestNews_RelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);

		bgSc = (ScrollView)findViewById(R.id.Scroll1);
		
		if(isInternetOn())
		{
			try {
				new MainScreenShow().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			Toast.makeText(CategorywiseGamesDetailsActivity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
			
		}


		
		
		


	

		WallpaperProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				
						
				
				HashMap<String, String> data = ContentArrayListWallpaper.get(position);
	
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "sub_inner");
				startActivity(idn);
				finish();
				
			}
		});
		Mp3ProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListMp3.get(position);
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "sub_inner");
				startActivity(idn);
				finish();
			}
		});
		VideoProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListVideos.get(position);
		
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));		
				idn.putExtra("which_screen", "sub_inner");
				startActivity(idn);
				finish();

			}
		});
		
		AppsProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListApp.get(position);
						
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "sub_inner");
				startActivity(idn);
				finish();

			}
		});
		
		GameProjectsList.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListGame.get(position);
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "sub_inner");
				startActivity(idn);
				finish();
				
				
			}
		});

	/*	BookProjectsList.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListBook.get(position);
				Intent idn = new Intent(CategorywiseVideoDetailsActivity.this, BooksDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "MainScreen");
				startActivity(idn);
				finish();
				
			}
		});*/
		
		
	

		

		LatestNews_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, LatestNewsActivity.class);
				startActivity(idn);
				finish();


			}
		});
		
		
		App_RelativeLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, CategorywiseAppsDetailsActivity.class);
				startActivity(idn);
				finish();
				
				
			}
		});
		
		appMoreBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameActivity.class);
				idn.putExtra("Content_Id", "10");
				startActivity(idn);
				finish();
				
				
			}
		});
		
		
		Video_RelativeLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, CategoryWise_VideoActivity.class);
				startActivity(idn);
				finish();
				
				
			}
		});
		
		videoMoreBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameActivity.class);
				idn.putExtra("Content_Id", "16");
				startActivity(idn);
				finish();
				
				
			}
		});
		
		Audio_RelativeLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, CategorywiseAudioDetailsActivity.class);
				startActivity(idn);
				finish();
				
				
			}
		});
		
		mp3MoreBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameActivity.class);
				idn.putExtra("Content_Id", "17");
				startActivity(idn);
				finish();
				
				
			}
		});
		Game_RelativeLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, CategorywiseGamesDetailsActivity.class);
				startActivity(idn);
				finish();
				
				
			}
		});
		
		gameMoreBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameActivity.class);
				idn.putExtra("Content_Id", "8");
				startActivity(idn);
				finish();
				
				
			}
		});
		Wallpaper_RelativeLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, CategorywiseWallpaperDetailsActivity.class);
				startActivity(idn);
				finish();
				
				
			}
		});
		
		wallapperMoreBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, GameActivity.class);
				idn.putExtra("Content_Id", "7");
				startActivity(idn);
				finish();
				
				
			}
		});
		Books_RelativeLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, CategorywiseBooksDetailsActivity.class);
				startActivity(idn);
				finish();
				
				
			}
		});
		
		bookMoreBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(CategorywiseGamesDetailsActivity.this, BooksActivity.class);
				startActivity(idn);
				finish();
				
				
			}
		});
		
	}

	private void Scrolling() {
		// TODO Auto-generated method stub
	  
		AppsProjectsList.setOnTouchListener(new ListView.OnTouchListener() {
	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	            	
	            	
	                int action = event.getAction();
	                switch (action) {
	                case MotionEvent.ACTION_DOWN:
	                    // Disallow ListView to intercept touch events.
	                    v.getParent().requestDisallowInterceptTouchEvent(true);
	                    break;
	                case MotionEvent.ACTION_UP:
	                    // Allow ListView to intercept touch events.
	                    v.getParent().requestDisallowInterceptTouchEvent(false);
	                    break;
	                    
	                    
	                
	                }
	                v.onTouchEvent(event);
	                
	                
	                return true;
	            }
	        });
		

	
		VideoProjectsList.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                case MotionEvent.ACTION_UP:
                    // Allow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                }
                v.onTouchEvent(event);
                return true;
            }
        });
		Mp3ProjectsList.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                case MotionEvent.ACTION_UP:
                    // Allow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                }
                v.onTouchEvent(event);
                return true;
            }
        });
		WallpaperProjectsList.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                case MotionEvent.ACTION_UP:
                    // Allow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                }
                v.onTouchEvent(event);
                return true;
            }
        });
		GameProjectsList.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                case MotionEvent.ACTION_UP:
                    // Allow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                }
                v.onTouchEvent(event);
                return true;
            }
        });
		BookProjectsList.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                case MotionEvent.ACTION_UP:
                    // Allow ListView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                }
                v.onTouchEvent(event);
                return true;
            }
        });
	

	
	
	}


	class MainScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			
				
			pDialog = new ProgressDialog(CategorywiseGamesDetailsActivity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.Games;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
//				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Category"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response12);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Category"+'"'+":}";
					
					Log.e("---------respon-------------", respon);
					
					if (!response.equals(respon)) 
					{
						
						bgSc.setVisibility(View.VISIBLE);
						
						
						Log.e("---------response-------------", response);
						
						
						JSONObject jsonObj = new JSONObject(MyResponse);

						
							JSONArray Home_Content_List = jsonObj.getJSONArray("Category");
							
							for (int i1 = 0; i1 < Home_Content_List.length(); i1++)
							{

								JSONObject cateHome = Home_Content_List.getJSONObject(i1);

								String _id = cateHome.getString("id");
								String _type_name = cateHome.getString("category_title");
								String _short_description = cateHome.getString("short_description");


//								appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv;
								
//								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_id);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_type_name);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_short_description);
								

								String ContentResponse = cateHome.getString("content");

								if(_id.equals("10")&&!ContentResponse.equals("[]"))
									AppsLayout.setVisibility(View.VISIBLE);
								

								if(_id.equals("16")&&!ContentResponse.equals("[]"))
									
									VideoLayout.setVisibility(View.VISIBLE);
																

								if(_id.equals("17")&&!ContentResponse.equals("[]"))
									Mp3Layout.setVisibility(View.VISIBLE);
								
									

								if(_id.equals("7"))
									WallpaperLayout.setVisibility(View.VISIBLE);
						

								if(_id.equals("8"))
									GameLayout.setVisibility(View.VISIBLE);
							
									

								/*if(_type_name.equals("6")&&ContentResponse.equals("[]"))
									BookLayout.setVisibility(View.GONE);*/
								
									
//								TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv;
								
								if(_id.equals("10"))
								{
									appTv.setText(_type_name);
								 	AppSubTv.setText(_short_description);
								}
												
								if(_id.equals("16"))
								{
									videoTv.setText(_type_name);
									VideoSubTv.setText(_short_description);
								}
								
								if(_id.equals("17"))
								{
									mp3Tv.setText(_type_name);
									Mp3SubTv.setText(_short_description);
								}
									
								if(_id.equals("7"))
								{
									wallapperTv.setText(_type_name);
									WallapperSubTv.setText(_short_description);
								}
									
								if(_id.equals("8"))
								{
									gameTv.setText(_type_name);
									GameSubTv.setText(_short_description);
								}
																
								/*if(_type_name.equals("6"))
								{
									bookTv.setText(_type_name);
									BookSubTv.setText(_short_description);
								}*/
									



								if(!ContentResponse.equals("[]"))
								{

									HashMap<String, String> Info = new HashMap<String, String>();
//									Info.put("id", _id);
									Info.put("type_name", _type_name);
									Info.put("short_description", _short_description);



//									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_id);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_type_name);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_short_description);


									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";

									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);

									JSONArray Content_List = ContentJsonObj.getJSONArray("content");

									// content node is JSON Object

									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject c = Content_List.getJSONObject(j);

										String _content_id = c.getString("id");
										String _content_title = c.getString("content_title");
//										String _content_description = c.getString("content_description");
										String _preview = c.getString("preview");
//										String _banner = c.getString("banner");
										String _total_download = c.getString("total_download");
										String _payment_type = c.getString("payment_type");
										String _price = c.getString("price");
										String _rating = c.getString("rating");
									/*	String _company_name = c.getString("company_name");
										String _developer_name = c.getString("developer_name");
										String _category_name = c.getString("category_name");*/
										
										Log.e("AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC", "AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC"+_content_id);
										Log.e("AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC", "AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC"+_content_title);

										HashMap<String, String> Content12 = new HashMap<String, String>();


										String type_name = Info.get("type_name");
										String type_id = Info.get("id");
										Log.e("AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC", "AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC"+type_name);

										if(_id.equals("10"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/

											

											ContentArrayListApp.add(Content12);

										}
										
										
										if(_id.equals("16"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/

											

											ContentArrayListVideos.add(Content12);

										}

										if(_id.equals("17"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/

											

											ContentArrayListMp3.add(Content12);

										}



										if(_id.equals("7"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/

											
											ContentArrayListWallpaper.add(Content12);

										}
										
										if(_id.equals("8"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/
											
											
											ContentArrayListGame.add(Content12);
										}
											
											/*if(type_id.equals("6"))
											{
												Content12.put("content_id", _content_id);
												Content12.put("type_name", type_name);
												Content12.put("content_title", _content_title);
//											Content12.put("content_description", _content_description);
												Content12.put("preview", _preview);
//											Content12.put("banner", _banner);
												Content12.put("total_download", _total_download);
												Content12.put("payment_type", _payment_type);
												Content12.put("price", _price);
												Content12.put("rating", _rating);
												Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);
												
												
												ContentArrayListBook.add(Content12);
												
										}*/


									}
									//		                      InfoList.add(Info);
								}
								else
								{
									Log.e("ServiceHandler", "Couldn't get any data from the content");



								}



							
							
							
						}

						

						
						adapter=new AdapterForMainScreen(CategorywiseGamesDetailsActivity.this, ContentArrayListVideos);
						adapter2=new AdapterForMainScreen(CategorywiseGamesDetailsActivity.this, ContentArrayListMp3);
						adapter3=new AdapterForMainScreen(CategorywiseGamesDetailsActivity.this, ContentArrayListWallpaper);
						adapter4=new AdapterForMainScreen(CategorywiseGamesDetailsActivity.this, ContentArrayListApp);
						adapter5=new AdapterForMainScreen(CategorywiseGamesDetailsActivity.this, ContentArrayListGame);
//						adapter6=new AdapterForMainScreen(CategorywiseVideoDetailsActivity.this, ContentArrayListBook);

						
						WallpaperProjectsList.setAdapter(adapter3);
						VideoProjectsList.setAdapter(adapter);
						Mp3ProjectsList.setAdapter(adapter2);
						AppsProjectsList.setAdapter(adapter4);
						GameProjectsList.setAdapter(adapter5);
//						BookProjectsList.setAdapter(adapter6);
						

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(CategorywiseGamesDetailsActivity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			pDialog.dismiss();



		}

	}
	
	
	
	
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent idd = new Intent(CategorywiseGamesDetailsActivity.this, MainActivity_Static.class);
			startActivity(idd);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}

	


}
