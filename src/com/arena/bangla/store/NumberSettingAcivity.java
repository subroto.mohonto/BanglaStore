package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class NumberSettingAcivity extends Activity{
	
	public static SQLiteDatabase myDB;
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	ProgressDialog pDialog;
	String selectedMobileNumber = "";
	
	Spinner SpMobile;
	ArrayList<String> MobileID=new ArrayList<String>();
	ArrayList<String> MobileNumber=new ArrayList<String>();
	ArrayList<String> MobliePin=new ArrayList<String>();
	ArrayAdapter<String> dataAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mobile_number_setting_screen);
		
		
		  // Creating Database
        myDB =  this.openOrCreateDatabase("apponit_db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        myDB.setVersion(1);
        myDB.setLocale(Locale.getDefault());
        
        
        myDB.execSQL("CREATE TABLE IF NOT EXISTS " +
        		"apponit_table" +
        		" (_id INTEGER PRIMARY KEY, mobile_number TEXT, pin TEXT, hash_tag TEXT);");
        
        
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        
        TextView Text = (TextView)findViewById(R.id.Text);
//        Text.setText(sharedpreferences.getString("MobileNumberForPurchase", null));
        
        SpMobile = (Spinner)findViewById(R.id.spMobile);
        
        new MobileList().execute();
        
    	SpMobile.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

//				new MobileList().execute();
				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+MobileNumber.get(arg2));
				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+MobliePin.get(arg2));
				
				SharedPreferences.Editor editor = sharedpreferences.edit();
				editor.putString("MobileNumberForPurchase", MobileNumber.get(arg2));
				editor.putString("PINCODE", MobliePin.get(arg2));
				editor.commit(); 
				
				
				/*selectedMobileNumber= MobileID.get(arg2);
//				designation_idDcm = AsmDesg.get(arg2);
				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+SpBrandID);
				if(!SpMobile.getSelectedItem().toString().equalsIgnoreCase("Select Asm"))
				{
					new MobileList().execute();
				}
*/
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
    	
    	
    	
		
			}

	
	
	class MobileList extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			MobileID.clear();
			MobileNumber.clear();
			MobliePin.clear();
			MobileID.add("00");
			MobileNumber.add(sharedpreferences.getString("MobileNumberForPurchase", null));
			MobliePin.add(sharedpreferences.getString("PINCODE", null));
			pDialog = new ProgressDialog(NumberSettingAcivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			

			
			Cursor c = myDB.rawQuery("SELECT * FROM apponit_table", null);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						
						HashMap<String, String> map = new HashMap<String, String>();
						
						
						map.put("mobile_number", c.getString(c.getColumnIndex("mobile_number")));
						map.put("pin", c.getString(c.getColumnIndex("pin")));
						
						String Mid = c.getString(c.getColumnIndex("_id"));
						String Mnumber = c.getString(c.getColumnIndex("mobile_number"));
						String Mpin = c.getString(c.getColumnIndex("pin"));
						Log.e("---------------------------", "---------------------------"+c.getString(c.getColumnIndex("mobile_number")));
						Log.e("---------------------------", "---------------------------"+c.getString(c.getColumnIndex("pin")));
						

						MobileID.add(Mid);
						MobileNumber.add(Mnumber);
						MobliePin.add(Mpin);
						
//						listviewitems.add(c.getString(c.getColumnIndex("product_family_name")));
						
					} while (c.moveToNext());


				}
				
				dataAdapter = new ArrayAdapter<String>(NumberSettingAcivity.this,android.R.layout.simple_spinner_item, MobileNumber);
				dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


			}

		
			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {


						SpMobile.setAdapter(dataAdapter);

					} catch (Exception e) { }

				}

				
			});

			pDialog.dismiss();
		}
	}
}
