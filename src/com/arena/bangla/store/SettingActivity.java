package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.CustomAdapter;
import com.arena.adapter.CustomAdapterTemplateHomeScreen;
import com.arena.adapter.CustomAdapterTemplateHomeScreen_WithInnerScreen;
import com.arena.bangla.store.R.color;
import com.arena.bangla.store.MainActivity_Static.SearchAutoCompleteTextViewList;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import com.devsmart.android.ui.HorizontalListView;



import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SettingActivity extends Activity {

	String Language ="",Language_For_Set = "";
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	String TempScreen="", Temp_Format12="";

	Context context;

	ProgressDialog pDialog;
	String ParentType="abc";
	String response;
	ArrayList<ArrayList< HashMap<String, String>>> collection=new ArrayList<ArrayList< HashMap<String, String>>>();
	String SubLink_, TitleName;
	TextView AppBtn1, VideoBtn1, AudioBtn1, GameBtn1, WallpaperBtn1,BookBtn1,LatestNewsBtn1;

	ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
	ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();
	String response12;
	AutoCompleteTextView SearchAutoCompleteTextView;

	private RadioGroup radioLanguageGroup;
	private RadioButton radioLanguageButton;  
	private Button btnDisplay;
	RadioButton Temp1,Temp2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings);
		
		LinearLayout Linear = (LinearLayout)findViewById(R.id.menuHorizontal);
		Linear.setVisibility(View.GONE);

		Temp1 = (RadioButton) findViewById(R.id.radio1);
		Temp2 = (RadioButton) findViewById(R.id.radio2);

		RadioButton Language1 = (RadioButton) findViewById(R.id.BanglaLangu);
		RadioButton Language2 = (RadioButton) findViewById(R.id.EnglishLangu);

		Temp1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//					Toast.makeText(SettingActivity.this, "Temp1", Toast.LENGTH_LONG).show();
				Temp_Format12 = "TemplateHomeScreenActivity";
			}
		});
		Temp2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//	        		Toast.makeText(SettingActivity.this, "Temp2", Toast.LENGTH_LONG).show();
				Temp_Format12 = "MainActivity_Static";
			}
		});

		Language1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//					Toast.makeText(SettingActivity.this, "Temp1", Toast.LENGTH_LONG).show();
				//	        		Temp_Format12 = "TemplateHomeScreenActivity";
				Language_For_Set = "?lang=bn";
			}
		});
		Language2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//	        		Toast.makeText(SettingActivity.this, "Temp2", Toast.LENGTH_LONG).show();
				//	        		Temp_Format12 = "MainActivity_Static";
				Language_For_Set = "English";
			}
		});

		addListenerOnButton();

		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

		Language = sharedpreferences.getString("Language", "");
		TempScreen = sharedpreferences.getString("HomeScreen", null);

		if(Language.equals("English"))
			Language2.setChecked(true);		
		else
			Language1.setChecked(true);

		if(TempScreen.equals("MainActivity_Static"))
			Temp2.setChecked(true);		
		else
			Temp1.setChecked(true);


		if(Temp1.isChecked())
			Temp_Format12 = "TemplateHomeScreenActivity";
		if(Temp2.isChecked())
			Temp_Format12 = "MainActivity_Static";

		if(Language1.isChecked())
			Language_For_Set = "?lang=bn";
		if(Language2.isChecked())
			Language_For_Set = "English";
			
			
		



		/*  SearchMenuOption(TitleName);
	        ViewInitializationAndActionofMenu(SubLink_);*/
		ViewInitializationAndAction();

		context=this;


		SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
		SearchAutoCompleteTextView.setThreshold(1);




	}

	public void addListenerOnButton() {

		radioLanguageGroup = (RadioGroup) findViewById(R.id.radioGender);
		btnDisplay = (Button) findViewById(R.id.btnDisplay);

		btnDisplay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// get selected radio button from radioGroup
				/*  int selectedId = radioLanguageGroup.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    radioLanguageButton = (RadioButton) findViewById(selectedId);*/



				//                    Toast.makeText(SettingActivity.this,Temp_Format, Toast.LENGTH_SHORT).show();

				/*   if(radioLanguageButton.getText().equals("Bangla"));
                    	Language_For_Set = "?lang=bn";

                    if(radioLanguageButton.getText().equals("English"));
                    	Language_For_Set = "English";*/




//				Toast.makeText(SettingActivity.this,Language_For_Set+"\n"+Temp_Format12, Toast.LENGTH_SHORT).show();


				SharedPreferences.Editor editor = sharedpreferences.edit();
				editor.putString("Language", Language_For_Set);
				editor.putString("HomeScreen", Temp_Format12);
				editor.commit();
				
				
				String Screen2 = sharedpreferences.getString("HomeScreen", null);

				Log.e("TTTTTTTTTTTTTTTTttt", "HHHHHHHHHHHHHHHHHHHH"+Screen2);
				if(Screen2.equals("MainActivity_Static"))
				{
					Intent idd = new Intent(SettingActivity.this, MainActivity_Static.class);
					startActivity(idd);
					finish();
				}
				else
				{
					Intent idd = new Intent(SettingActivity.this, TemplateHomeScreenActivity.class);
					startActivity(idd);
					finish(); 
				}


			}

		});
	}


	private void SearchMenuOption(String Name) {

		Button SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
		ImageView SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);		
		TextView NameBack = (TextView)findViewById(R.id.TitleText);	

		final RelativeLayout CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
		final RelativeLayout SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);

		NameBack.setText(Name);

		SearchingCategoryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				CategoryLayout.setVisibility(View.GONE);
				SearchingLayout.setVisibility(View.VISIBLE);
				new SearchAutoCompleteTextViewList().execute();

			}
		});

		SearchingBackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				CategoryLayout.setVisibility(View.VISIBLE);
				SearchingLayout.setVisibility(View.GONE);


			}
		});

		NameBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				finish();
			}
		});

	}
	private void ViewInitializationAndActionofMenu(String ID) {

		TextView AppBtn1 = (TextView)findViewById(R.id.AppBtn1);
		TextView VideoBtn1 = (TextView)findViewById(R.id.VideoBtn1);
		TextView AudioBtn1 = (TextView)findViewById(R.id.AudioBtn2);
		TextView WallpaperBtn1 = (TextView)findViewById(R.id.WallpaperBtn2);
		TextView GameBtn1 = (TextView)findViewById(R.id.GameBtn2);
		TextView BookBtn1 = (TextView)findViewById(R.id.BookBtn2);
		TextView LatestNewsBtn1 = (TextView)findViewById(R.id.LatestNewsBtn2);

		if(ID.equals("1"))
			AppBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("2"))
			VideoBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("3"))
			AudioBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("4"))
			WallpaperBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("5"))
			GameBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("6"))
			BookBtn1.setTextColor(color.ActionBar_Orange);
		else if(ID.equals("1000"))
			LatestNewsBtn1.setTextColor(color.ActionBar_Orange);



	}

	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);


		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), SettingActivity.class);
				idn.putExtra("SubLink", "1");
				idn.putExtra("TitleName", "অ�?যাপস");
				startActivity(idn);
				//					finish();
			}
		});


		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), SettingActivity.class);
				idn.putExtra("SubLink", "2");
				idn.putExtra("TitleName", "ভিডিও");
				startActivity(idn);
				//					finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), SettingActivity.class);
				idn.putExtra("SubLink", "3");
				idn.putExtra("TitleName", "�?মপিথ�?রি");
				startActivity(idn);
				//					finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), SettingActivity.class);
				idn.putExtra("SubLink", "4");
				idn.putExtra("TitleName", "ওয়ালপেপার");
				startActivity(idn);
				//					finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), SettingActivity.class);
				idn.putExtra("SubLink", "5");
				idn.putExtra("TitleName", "গেইম");
				startActivity(idn);
				//					finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), SettingActivity.class);
				idn.putExtra("SubLink", "6");
				idn.putExtra("TitleName", "বই");
				startActivity(idn);
				//					finish();
			}
		});

		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
				idn.putExtra("SubLink", "1000");
				idn.putExtra("TitleName", "সর�?বশেষ সংবাদ");
				startActivity(idn);
				//					finish();
			}
		});



	}



	class SearchAutoCompleteTextViewList extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			SearchAutoComplete_ID.clear();
			SearchAutoComplete_Name.clear();


		}

		@Override
		protected String doInBackground(String... args) {


			try {
				String url=URLs.Search_Keyword;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);

				response = "{"+'"'+"Category"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response12);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {


						parseXmlresponseForProductCatagory(response);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String MyResponse) throws JSONException 
				{


					if (response != null) 
					{

						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);

							String _id = cate.getString("id");
							String _keyword_name = cate.getString("keyword");

							SearchAutoComplete_ID.add(_id);
							SearchAutoComplete_Name.add(_keyword_name);

						}

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SettingActivity.this,android.R.layout.simple_spinner_item, SearchAutoComplete_Name);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					SearchAutoCompleteTextView.setAdapter(dataAdapter);

				}


			});


			SearchAutoCompleteTextView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SearchAutoCompleteTextView.showDropDown();
				}
			});
			SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {

					TempData.SEARCH_ID=SearchAutoComplete_ID.get(arg2);
					Log.e("IDDDDD", String.valueOf(SearchAutoComplete_ID.get(arg2)));
					Intent in=new Intent(SettingActivity.this,SearchDetails.class);
					startActivity(in);


				}
			});
		}
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			/*Intent idd = new Intent(SettingActivity.this, MainActivity_Static.class);
				startActivity(idd);
				finish();*/
			String Screen2 = sharedpreferences.getString("HomeScreen", null);

			Log.e("TTTTTTTTTTTTTTTTttt", "HHHHHHHHHHHHHHHHHHHH"+Screen2);
			if(Screen2.equals("MainActivity_Static"))
			{
				Intent idd = new Intent(SettingActivity.this, MainActivity_Static.class);
				startActivity(idd);
				finish();
			}
			else
			{
				Intent idd = new Intent(SettingActivity.this, TemplateHomeScreenActivity.class);
				startActivity(idd);
				finish(); 
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
}
