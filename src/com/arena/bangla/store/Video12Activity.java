package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.LazyAdapter;
import com.arena.bangla.store.R;
import com.arena.bangla.store.AppsActivity.VideoScreenShow;
import com.arena.json.JsonParse;
import com.arena.json.URLs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class Video12Activity extends Activity{
	
	ProgressDialog pDialog;
	String response;
	String response12;
//	LazyAdapter adapter;
	LazyAdapter adapter;
	
	ArrayList<HashMap<String, String>> ContentArrayList;
	GridView gv;
	
	String Content_Id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.apps_screen);
		
		Bundle b = getIntent().getExtras();
		Content_Id = b.getString("Content_Id");
		
		
		ViewInitializationAndAction();
		gv = (GridView)findViewById(R.id.gridView14);
		ContentArrayList = new ArrayList<HashMap<String, String>>();
		
//		new VideoScreenShow().execute();
		if(isInternetOn())
		{
			try {
				new VideoScreenShow().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			Toast.makeText(Video12Activity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
			
		}

		
		
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				
				HashMap<String, String> data = ContentArrayList.get(position);
			
				Intent idn = new Intent(Video12Activity.this, VideoDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("which_screen", "InnerScreen");
				startActivity(idn);
				finish();
			}
		});
		
	}
	
	
	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "1");
				startActivity(idn);
				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "2");
				startActivity(idn);
				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "3");
				startActivity(idn);
				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "4");
				startActivity(idn);
				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "5");
				startActivity(idn);
				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "6");
				startActivity(idn);
				finish();
			}
		});
		
		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
				idn.putExtra("SubLink", "1000");
				startActivity(idn);
				finish();
			}
		});



	}

	
	class VideoScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(Video12Activity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.CategoryWise+"2/"+Content_Id+"?lang=bn";
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Contents"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Contents"+'"'+":}";
					Log.e("---------respon-------------", respon);
					
					if (!response.equals(respon)) 
					{
						
						
						
						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Contents");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject con = ContentList.getJSONObject(i);

							
							/*id": "10",
					        "content_title": "Depika's New Video",
					        "preview": "http:\/\/192.168.1.20\/apponit\/uploaded_content\/videos\/preview\/20160525144140.png",
					        "total_download": "0",
					        "payment_type": "Paid",
					        "price": "10.00",
					        "rating": 4*/
					        
							String _content_id = con.getString("id");
							String _content_title = con.getString("content_title");
							String _preview = con.getString("preview");
							String _total_download = con.getString("total_download");
							String _payment_type = con.getString("payment_type");
							String _price = con.getString("price");
							String _rating = con.getString("rating");
							

							HashMap<String, String> Content12 = new HashMap<String, String>();
							
							
							Content12.put("content_id", _content_id);
							Content12.put("content_title", _content_title);
							Content12.put("preview", _preview);
						    Content12.put("total_download", _total_download);
							Content12.put("payment_type", _payment_type);
							Content12.put("price", _price);
							Content12.put("rating", _rating);
							
							ContentArrayList.add(Content12);
							
						}
						adapter = new LazyAdapter(Video12Activity.this, ContentArrayList);	
						gv.setAdapter(adapter);
							

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(Video12Activity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			pDialog.dismiss();



		}

	}
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent idd = new Intent(Video12Activity.this, CategoryWise_VideoActivity.class);
			startActivity(idd);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
	
}
