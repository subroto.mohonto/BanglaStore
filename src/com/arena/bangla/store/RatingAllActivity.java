package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForApps;
import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.AdapterForRatingReview;
import com.arena.adapter.LazyAdapter;
import com.arena.bangla.store.MainActivity_Static.MainScreenShow;
import com.arena.json.JsonParse;
import com.arena.json.URLs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class RatingAllActivity extends Activity{
	
	ProgressDialog pDialog;
	String response;
	String response12;
//	LazyAdapter adapter;
	LazyAdapter adapter;
	
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	String Language ="";
	
//	ArrayList<HashMap<String, String>> ContentArrayList;
	GridView RatingReviewProjectsList;
	
	AdapterForRatingReview adapterRatingReview;
	ArrayList<HashMap<String, String>> ContentArrayListReview;
	
	String Content_Id,Which_Screen;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.apps_screen);
		ViewInitializationAndAction();
		
		Bundle b = getIntent().getExtras();
		Content_Id = b.getString("Content_Id");
		Which_Screen = b.getString("which_screen");
		
		RatingReviewProjectsList = (GridView)findViewById(R.id.gridView14);
		ContentArrayListReview = new ArrayList<HashMap<String, String>>();
		
		Language = sharedpreferences.getString("Language", "");
		if(Language.equals("English"))
		{
			Language="";		
		}
		
		if(isInternetOn())
		{
			new VideoScreenShow().execute();
		}
		else
		{
			Toast.makeText(RatingAllActivity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
			
		}
	}
	
	
	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(RatingAllActivity.this, RatingAllActivity.class);
				startActivity(idn);
				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(RatingAllActivity.this, Video12Activity.class);
				startActivity(idn);
				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(RatingAllActivity.this, AudioActivity.class);
				startActivity(idn);
				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(RatingAllActivity.this, WallpaperActivity.class);
				startActivity(idn);
				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(RatingAllActivity.this, GameActivity.class);
				startActivity(idn);
				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(RatingAllActivity.this, BooksActivity.class);
				startActivity(idn);
				finish();
			}
		});


	}


	class VideoScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(RatingAllActivity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				//				String url=URLs.DOMAIN_DETAILS+"10";
				String url=URLs.DOMAIN_DETAILS+Content_Id+Language;

				response12 = JsonParse.makeServiceCall(url);
				//				response = "{"+'"'+"Category"+'"'+":"+response12+"}";
				response = "{"+'"'+"Category"+'"'+":["+response12+"]}";
				Log.e("---------response-------------", response);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {

						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Category"+'"'+":}";
					//					Log.e("---------respon-------------", response12);

					if (!response.equals(respon)) 
						//						if (!response12.equals("")) 
					{



						JSONObject jsonObj = new JSONObject(MyResponse);

						Log.e("---------jsonObj-------------", ""+jsonObj);

						JSONArray ContentList = jsonObj.getJSONArray("Category");

						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cat = ContentList.getJSONObject(i);



							
							String review_list12 = cat.getString("review_list");



							Log.e("---------review_list12-------------", review_list12);


							
							
							String Review_Content_Response = "{"+'"'+"review_list"+'"'+":"+review_list12+"}";


							
							Log.e("---------Review_Content_Response-------------", Review_Content_Response);


							//							    JSONObject contnt = jsonObj.getJSONObject("content_details");

							
							
							
							
							if(!review_list12.equals("[]"))
							{
								
								JSONObject Review_ContentJsonObj = new JSONObject(Review_Content_Response);
								JSONArray Review_Content_List = Review_ContentJsonObj.getJSONArray("review_list");
								
								if(Review_Content_List.length()!=0)
								{
									for (int j1 = 0; j1 < Review_Content_List.length(); j1++)
									{

										JSONObject cateReview = Review_Content_List.getJSONObject(j1);

										String user_name = cateReview.getString("user_name");
										String review_text = cateReview.getString("review_text");
										String rating = cateReview.getString("rating");
										
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+user_name);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+review_text);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+rating);
										
										HashMap<String, String> InfoReview = new HashMap<String, String>();
										InfoReview.put("user_name", user_name);
										InfoReview.put("review_text", review_text);
										InfoReview.put("rating", rating);
										ContentArrayListReview.add(InfoReview);
									}
								}
							
							}		
						}
						adapterRatingReview=new AdapterForRatingReview(RatingAllActivity.this, ContentArrayListReview);
					
						RatingReviewProjectsList.setAdapter(adapterRatingReview);

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(RatingAllActivity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			pDialog.dismiss();

			

			
		}

	}
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent idd = new Intent(RatingAllActivity.this, MainActivity_Static.class);
			startActivity(idd);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}

}
