package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


import com.arena.adapter.AdapterForLatestNews;
import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.LazyAdapter;
import com.arena.bangla.store.R.color;
import com.arena.bangla.store.MainActivity_Static.MainScreenShow;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import com.arena.json.XMLfunctions;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class LatestNewsActivity_working extends Activity{
	
	ProgressDialog pDialog;
	String response;
	String response12;
//	LazyAdapter adapter;
//	AdapterForLatestNews adapter;
	
	SimpleAdapter adapter;
	
	ArrayList<HashMap<String, String>> Bangladesh=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> world=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> business=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> politics=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> economy=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> sport=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> cricket=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> tech=new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> lifestyle=new ArrayList<HashMap<String, String>>();
	
	ArrayList<HashMap<String, String>> TotalData=new ArrayList<HashMap<String, String>>();
	 ListView ll;
	 ArrayList<HashMap<String, String>> list=new ArrayList<HashMap<String, String>>();
	 
	 
	 
	// All static variables
		static final String URL = "http://bangla.bdnews24.com/?widgetName=rssfeed&widgetId=1151&getXmlFeed=true";
		// XML node keys
		static final String KEY_ITEM = "item"; // parent node
//		static final String KEY_ID = "id";
		static final String KEY_NAME = "title";
		static final String KEY_Link = "link";
		static final String KEY_DESC = "description";
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.latestnews);
		
		Bundle b= getIntent().getExtras();
		String SubLink = b.getString("SubLink");
		
//		ViewInitializationAndActionofMenu(SubLink);
//		ViewInitializationAndAction();
		
		 ll =(ListView)findViewById(R.id.listView1);
		
		
		
		if(isInternetOn())
		{
			try {
				new BusinessList().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}

//			new VideoScreenShow().execute();
		}
		else
		{
			Toast.makeText(LatestNewsActivity_working.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
			
		}

		
	
}
	
	
	private void ViewInitializationAndActionofMenu(String ID) {
		
		TextView AppBtn1 = (TextView)findViewById(R.id.AppBtn1);
		TextView VideoBtn1 = (TextView)findViewById(R.id.VideoBtn1);
		TextView AudioBtn1 = (TextView)findViewById(R.id.AudioBtn2);
		TextView WallpaperBtn1 = (TextView)findViewById(R.id.WallpaperBtn2);
		TextView GameBtn1 = (TextView)findViewById(R.id.GameBtn2);
		TextView BookBtn1 = (TextView)findViewById(R.id.BookBtn2);
		TextView LatestNewsBtn1 = (TextView)findViewById(R.id.LatestNewsBtn2);
		
		RelativeLayout App_RelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout Video_RelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout MP3_RelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout Game_RelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout Wallpaper_RelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout Book_RelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNews_RelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		if(ID.equals("1"))
		{
			AppBtn1.setTextColor(color.ActionBar_Orange);
			App_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}			
		else if(ID.equals("2"))
		{
			VideoBtn1.setTextColor(color.ActionBar_Orange);
			Video_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("3"))
		{
			AudioBtn1.setTextColor(color.ActionBar_Orange);
			MP3_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("4"))
		{
			WallpaperBtn1.setTextColor(color.ActionBar_Orange);
			Game_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("5"))
		{
			GameBtn1.setTextColor(color.ActionBar_Orange);
			Wallpaper_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("6"))
		{
			BookBtn1.setTextColor(color.ActionBar_Orange);
			Book_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("1000"))
		{
			LatestNewsBtn1.setTextColor(color.ActionBar_Orange);
			LatestNews_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		
		
	}
	
	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "1");
				startActivity(idn);
//				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "2");
				startActivity(idn);
//				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "3");
				startActivity(idn);
//				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "4");
				startActivity(idn);
//				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "5");
				startActivity(idn);
//				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "6");
				startActivity(idn);
//				finish();
			}
		});
		
		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity_working.class);
				idn.putExtra("SubLink", "1000");
				startActivity(idn);
//				finish();
			}
		});



	}


	class VideoScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(LatestNewsActivity_working.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.LatestNess;
				
				response12 = JsonParse.makeServiceCall(url);
				Log.e("response12", response12);
						
			

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						JSONObject jsonObj = new JSONObject(response12);
						
						for (int i = 0; i < jsonObj.getJSONArray("bangladesh").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("bangladesh").getJSONObject(i);
							map.put("Head","Bangladesh");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							Bangladesh.add(map);
						}
						
						
						for (int i = 0; i < jsonObj.getJSONArray("world").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("world").getJSONObject(i);
							map.put("Head","World");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							world.add(map);
						}
						
						for (int i = 0; i < jsonObj.getJSONArray("business").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("business").getJSONObject(i);
							map.put("Head","Bussines");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							business.add(map);
						}
						
						
						for (int i = 0; i < jsonObj.getJSONArray("politics").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("politics").getJSONObject(i);
							map.put("Head","Politics");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							politics.add(map);
						}
						
						
						for (int i = 0; i < jsonObj.getJSONArray("economy").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("economy").getJSONObject(i);
							map.put("Head","Economy");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							economy.add(map);
						}
						
						
						for (int i = 0; i < jsonObj.getJSONArray("sport").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("sport").getJSONObject(i);
							map.put("Head","Sprots");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							sport.add(map);
						}
						
						for (int i = 0; i < jsonObj.getJSONArray("cricket").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("cricket").getJSONObject(i);
							map.put("Head","Cricket");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							cricket.add(map);
						}
						
						
						for (int i = 0; i < jsonObj.getJSONArray("tech").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("tech").getJSONObject(i);
							map.put("Head","Technology");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							tech.add(map);
						}
						
						for (int i = 0; i < jsonObj.getJSONArray("lifestyle").length(); i++)
						{
							HashMap<String, String> map=new HashMap<String, String>();

							JSONObject con = jsonObj.getJSONArray("lifestyle").getJSONObject(i);
							map.put("Head","Life Style");
							map.put("title", con.getString("title"));
							map.put("authorname", con.getString("authorname"));
							map.put("description", con.getString("description"));
							map.put("details", con.getString("details"));
							map.put("link", con.getString("link"));
							lifestyle.add(map);
						}

					} catch (Exception e) { }
					
					
					Log.e("Bangladesh", String.valueOf(Bangladesh.size()));
					Log.e("world", String.valueOf(world.size()));
					Log.e("business", String.valueOf(business.size()));
					Log.e("politics", String.valueOf(politics.size()));
					Log.e("economy", String.valueOf(economy.size()));
					Log.e("sport", String.valueOf(sport.size()));
					Log.e("tech", String.valueOf(tech.size()));
					Log.e("cricket", String.valueOf(cricket.size()));
					Log.e("lifestyle", String.valueOf(lifestyle.size()));
				}
			});
			pDialog.dismiss();


			ListData();

		}

		private void ListData() {
			// TODO Auto-generated method stub
			TotalData.addAll(Bangladesh);
			TotalData.addAll(world);
			TotalData.addAll(business);
			TotalData.addAll(politics);
			TotalData.addAll(economy);
			TotalData.addAll(sport);
			TotalData.addAll(tech);
			TotalData.addAll(cricket);
			TotalData.addAll(lifestyle);
			TempData.TempTotalData=TotalData;
			   
			 /*  adapter=new AdapterForLatestNews(LatestNewsActivity.this,TotalData);
			   ListView ll=(ListView)findViewById(R.id.listView1);
			   ll.setAdapter(adapter);*/

			   
			
		}

		

	}
	
	
	 class BusinessList extends AsyncTask<String, String, String> {

			/**
			 * Before starting background thread Show Progress Dialog
			 * */
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				pDialog = new ProgressDialog(LatestNewsActivity_working.this);
			
			    pDialog.setMessage("Loading Please wait.");
			    pDialog .requestWindowFeature (Window.FEATURE_NO_TITLE);
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
			}

			/**
			 * getting All products from url
			 * */
			@Override
			protected String doInBackground(String... args) {
				// Building Parameters

				
				XMLParser parser = new XMLParser();
				String xml = parser.getXmlFromUrl(URL).replaceAll("<p><p><br>", ""); // getting XML
				
				Log.e("GGGGGGGGGGGGGG", "----------------"+xml);
				Document doc = parser.getDomElement(xml); // getting DOM element

				NodeList nl = doc.getElementsByTagName(KEY_ITEM);
				// looping through all item nodes <item>
				for (int i = 0; i < nl.getLength(); i++) {
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					Element e = (Element) nl.item(i);
					// adding each child node to HashMap key => value
//					map.put(KEY_ID, parser.getValue(e, KEY_ID));
					map.put(KEY_NAME, parser.getValue(e, KEY_NAME));
					map.put(KEY_Link, parser.getValue(e, KEY_Link));
					map.put(KEY_DESC, parser.getValue(e, KEY_DESC));

					// adding HashList to ArrayList
					list.add(map);
				}
				
				
				String [] from = {KEY_NAME};
		        int [] to = { R.id.txtTitle}; 
//		        adapter = new SimpleAdapter(this, list, R.layout.row_latest_news,from,to);
		        adapter = new SimpleAdapter(getApplicationContext(), list, R.layout.row_latest_news, from, to);
				return null;
			}

			/**
			 * After completing background task Dismiss the progress dialog
			 * **/
			@Override
			protected void onPostExecute(String file_url) {
				// dismiss the dialog after getting all products
				pDialog.dismiss();
				// updating UI from Background Thread
				runOnUiThread(new Runnable() {
					public void run() {
						/**
						 * Updating parsed JSON data into ListView
						 * */

						ll.setAdapter(adapter);
					}
				});

			}

		}
	    
	    
	   
	    
	    public void Listv() 
	    {
	    	
	    	XMLParser parser = new XMLParser();
	    	
	    	String xml = XMLfunctions.getXMLNews().replaceAll("<p><p><br>", ""); // getting XML
	    	Log.e("GGGGGGGGGG", "GGGGGGGGGGGGG"+xml);
			Document doc = XMLfunctions.XMLfromString(xml);
			
			NodeList nodes = doc.getElementsByTagName("item");
			
			
			for(int j =0; j<nodes.getLength();j++)
			{
				Element y = (Element)nodes.item(j);
				HashMap<String, String> rmap = new HashMap<String, String>();
				
//				rmap.put("title", ""+Html.fromHtml(XMLfunctions.getCharacterDataFromElement(y, "title")));
				rmap.put("title", XMLfunctions.getValue(y, "title"));
				rmap.put("pubDate", XMLfunctions.getValue(y, "pubDate"));
				Log.e("HHHHHHHHH", "HHHHHHHHHHHHH"+XMLfunctions.getValue(y, "title"));
				/*rmap.put("description", XMLfunctions.getValue(y, "description"));
				rmap.put("link", XMLfunctions.getValue(y, "link"));*/
				/*link = XMLfunctions.getValue(y, "link");
				rmap.put("pubDate", XMLfunctions.getValue(y, "pubDate"));;*/
				list.add(rmap);
			
			}
			
			String [] from = {"title","pubDate"};
	        int [] to = { R.id.txtTitle, R.id.txtAuthorName}; 
	        adapter = new SimpleAdapter(this, list, R.layout.row_latest_news,from,to);
	    	
	       
	    }
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			/*Intent idd = new Intent(LatestNewsActivity.this, MainActivity_Static.class);
			startActivity(idd);*/
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}

}
