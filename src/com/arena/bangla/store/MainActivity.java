package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends Activity {
	ProgressDialog pDialog;
	String response;
	String response12;
	AdapterForMainScreen adapter;

	ArrayList<HashMap<String, String>> InfoList;
	ArrayList<HashMap<String, String>> ContentArrayList;
	ListView mainsListview;

	Button AppBtn, SearchingCategoryBtn, SearchingSearchingBtn;
	RelativeLayout CategoryLayout, SearchingLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main_past);

		AppBtn = (Button)findViewById(R.id.hm_btn_Apps);
		SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
//		SearchingSearchingBtn = (Button)findViewById(R.id.SearchingSearchingBtn);


		CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
		SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);


		ContentArrayList = new ArrayList<HashMap<String, String>>();
		InfoList = new ArrayList<HashMap<String, String>>();

		mainsListview = (ListView)findViewById(R.id.mainsListview);

		try {
			new MainScreenShow().execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
		


		SearchingCategoryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~`");

				CategoryLayout.setVisibility(View.GONE);
//				SearchingLayout.setVisibility(View.VISIBLE);


			}
		});


		AppBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity.this, AppsActivity.class);
				startActivity(idn);


			}
		});
	}


	class MainScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.MainScreen;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Category"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					if (response12 != null) 
					{
						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);

							String _id = cate.getString("id");
							String _type_name = cate.getString("type_name");
							String _short_description = cate.getString("short_description");




							String ContentResponse = cate.getString("content");

							if(!ContentResponse.equals("[]"))
							{
								HashMap<String, String> Info = new HashMap<String, String>();
								Info.put("id", _id);
								Info.put("type_name", _type_name);
								Info.put("short_description", _short_description);



								Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_id);
								Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_type_name);
								Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_short_description);


								String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";

								JSONObject ContentJsonObj = new JSONObject(_ContentResponse);

								JSONArray Content_List = ContentJsonObj.getJSONArray("content");

								// content node is JSON Object

								for (int j = 0; j < Content_List.length(); j++)
								{
									JSONObject c = Content_List.getJSONObject(j);

									String _content_id = c.getString("id");
									String _content_title = c.getString("content_title");
									String _content_description = c.getString("content_description");
									String _preview = c.getString("preview");
									String _banner = c.getString("banner");
									String _total_download = c.getString("total_download");
									String _payment_type = c.getString("payment_type");
									String _price = c.getString("price");
									String _company_name = c.getString("company_name");
									String _developer_name = c.getString("developer_name");
									String _category_name = c.getString("category_name");

									HashMap<String, String> Content12 = new HashMap<String, String>();

									Content12.put("content_id", _content_id);
									Content12.put("content_title", _content_title);
									Content12.put("content_description", _content_description);
									Content12.put("preview", _preview);
									Content12.put("banner", _banner);
									Content12.put("total_download", _total_download);
									Content12.put("payment_type", _payment_type);
									Content12.put("price", _price);
									Content12.put("company_name", _company_name);
									Content12.put("developer_name", _developer_name);
									Content12.put("category_name", _category_name);

									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_content_title);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_content_description);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_preview);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_banner);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_total_download);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_payment_type);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_price);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_company_name);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_developer_name);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_category_name);

									ContentArrayList.add(Content12);


								}
								InfoList.add(Info);
							}
							else
							{
								Log.e("ServiceHandler", "Couldn't get any data from the content");
							}



						}

						/*adapter=new AdapterForMainScreen(MainActivity.this, ContentArrayList, InfoList);
						mainsListview.setAdapter(adapter);*/

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

				}

			});
			pDialog.dismiss();



		}

	}

}
