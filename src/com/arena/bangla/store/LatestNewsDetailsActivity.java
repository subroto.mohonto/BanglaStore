package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForLatestNews;
import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.LazyAdapter;
import com.arena.bangla.store.MainActivity_Static.MainScreenShow;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class LatestNewsDetailsActivity extends Activity{
	
	HashMap<String, String> resultMap = new HashMap<String, String>();
	TextView txtTitle,txtAuthorName,txtDescripiton;
	private WebView wv1;
	ProgressDialog pDialog;
	String response;
	String response12;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.latestnews_details);
		ViewInitializationAndAction();
		SetDescription();

}
					/*	map.put("Head","Bangladesh");
						map.put("title", con.getString("title"));
						map.put("authorname", con.getString("authorname"));
						map.put("description", con.getString("description"));
						map.put("details", con.getString("details"));
						map.put("link", con.getString("link"));
						*/
	private void SetDescription() {
		// TODO Auto-generated method stub
		resultMap=TempData.TempTotalData.get(TempData.DescriptionID);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtAuthorName=(TextView)findViewById(R.id.txtAuthorName);
		txtDescripiton=(TextView)findViewById(R.id.txtDescripiton);
		
		
		
		Log.e("Description",resultMap.get("details"));
		txtTitle.setText(resultMap.get("title"));
		txtAuthorName.setText(resultMap.get("authorname"));
		String detail_txt = resultMap.get("details").replaceAll("&lt;p&gt;", "").replaceAll("&lt;/p&gt;", "").replaceAll("&lt;p align=&#034;justify&#034;&gt;", "").replaceAll("&lt;/p&gt;&lt;p align=&#034;justify&#034;&gt;", "");
		Log.e("RRRRRRRRRRRRRR", "RRRRRRRRRRRRRRRr"+detail_txt);
		txtDescripiton.setText(Html.fromHtml(detail_txt));
		
		
		TextView link =(TextView)findViewById(R.id.txtChangeLinks);
		link.setClickable(true);
		link.setMovementMethod(LinkMovementMethod.getInstance());
		link.setText(Html.fromHtml(resultMap.get("link")));
		link.setPaintFlags(link.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
		
		
		final LinearLayout newsDetails=(LinearLayout)findViewById(R.id.newsDetails);
		final LinearLayout newsLink=(LinearLayout)findViewById(R.id.newsLink);
		link.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newsDetails.setVisibility(View.GONE);
				newsLink.setVisibility(View.VISIBLE);  
				wv1=(WebView)findViewById(R.id.webView);
			    wv1.setWebViewClient(new MyBrowser());
			    new LoadWebViw().execute();
				
			}
		});
		
		TextView txtChangeDetails=(TextView)findViewById(R.id.txtChangeDetails);
		
		txtChangeDetails.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				newsDetails.setVisibility(View.VISIBLE);
				newsLink.setVisibility(View.GONE);
				resultMap=TempData.TempTotalData.get(TempData.DescriptionID);
				txtTitle=(TextView)findViewById(R.id.txtTitle);
				txtAuthorName=(TextView)findViewById(R.id.txtAuthorName);
				txtDescripiton=(TextView)findViewById(R.id.txtDescripiton);
				
				txtTitle.setText(resultMap.get("title"));
				txtAuthorName.setText(resultMap.get("authorname"));
				txtDescripiton.setText(Html.fromHtml(resultMap.get("details")));
			}
		});
		
	}

	
	
	private class MyBrowser extends WebViewClient {
	      @Override
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	         view.loadUrl(url);
	         return true;
	      }
	   }
	   

	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "1");
				startActivity(idn);
				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "2");
				startActivity(idn);
				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "3");
				startActivity(idn);
				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "4");
				startActivity(idn);
				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "5");
				startActivity(idn);
				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "6");
				startActivity(idn);
				finish();
			}
		});
		
		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
				idn.putExtra("SubLink", "1000");
				startActivity(idn);
				finish();
			}
		});



	}

	
	class LoadWebViw extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(LatestNewsDetailsActivity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {


			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {
					    wv1.getSettings().setLoadsImagesAutomatically(true);
			            wv1.getSettings().setJavaScriptEnabled(true);
			            wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
			            wv1.loadUrl(resultMap.get("link"));

					} catch (Exception e) {
					}
				

				}

				

			});
			pDialog.dismiss();



		}

	}
	

	
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}

}
