package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.arena.adapter.LazyAdapter;
import com.arena.adapter.LazyAdapter_Searching;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;

import com.arena.json.URLs;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SearchDetails extends Activity{
	ProgressDialog pDialog;
	String StringResponse;
	
	ArrayList<HashMap<String, String>> ContentArrayList;
	GridView gv;
	LazyAdapter_Searching adapter;
	
	ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
    ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();
    String responseSearch;
//    String responseSearch;
    AutoCompleteTextView SearchAutoCompleteTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.apps_screen);
		
		SearchMenuOption("Searching Data");
		ViewInitializationAndAction();
		
		gv = (GridView)findViewById(R.id.gridView14);
		ContentArrayList = new ArrayList<HashMap<String, String>>();
		
		  SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
			SearchAutoCompleteTextView.setThreshold(1);
	
			try {
				new SearchDetailsList().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
		
	}
	
	 
	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "1");
				startActivity(idn);
				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "2");
				startActivity(idn);
				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "3");
				startActivity(idn);
				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "4");
				startActivity(idn);
				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "5");
				startActivity(idn);
				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
				idn.putExtra("SubLink", "6");
				startActivity(idn);
				finish();
			}
		});
		
		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
				startActivity(idn);
				finish();
			}
		});



	}


		
	class SearchDetailsList extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SearchDetails.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			 
			
			
			StringResponse= JsonParse.fetchJSON(URLs.SearchResult+TempData.SEARCH_ID+"?lang=bn");
		    
		  Log.e("Response", StringResponse);
		  
			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {
					
					try
					{
						
						String respon = "{"+'"'+"Contents"+'"'+":}";
						if (!StringResponse.equals(respon)) 
						{
							
							
							
							JSONObject jsonObj = new JSONObject(StringResponse);

							// Getting JSON Array node
							JSONArray ContentList = jsonObj.getJSONArray("content");


							// looping through All Contacts
							for (int i = 0; i < ContentList.length(); i++)
							{

								JSONObject con = ContentList.getJSONObject(i);

								
						        
								String _content_id = con.getString("id");
								String _content_title = con.getString("content_title");
								String type_id = con.getString("type_id");
								String _preview = con.getString("preview");
								String _total_download = con.getString("total_download");
								String _payment_type = con.getString("payment_type");
								String _price = con.getString("price");
								String _rating = con.getString("rating");
								

								HashMap<String, String> Content12 = new HashMap<String, String>();
								
								
								Content12.put("content_id", _content_id);
								Content12.put("content_title", _content_title);
								Content12.put("type_id", type_id);
								Content12.put("preview", _preview);
							    Content12.put("total_download", _total_download);
								Content12.put("payment_type", _payment_type);
								Content12.put("price", _price);
								Content12.put("rating", _rating);
								
								ContentArrayList.add(Content12);
								
							}
							adapter = new LazyAdapter_Searching(SearchDetails.this, ContentArrayList);	
							gv.setAdapter(adapter);
								

						}
						else 
						{
							Log.e("ServiceHandler", "Couldn't get any data from the url");
							Toast.makeText(SearchDetails.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
						}
					}
					catch(Exception e){}
					

					
				}
			});
			
		
			pDialog.dismiss();
		
		}
	}

	
	 private void SearchMenuOption(String Name) {
			
			Button SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
			ImageView SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);		
			TextView NameBack = (TextView)findViewById(R.id.TitleText);	
			
			final RelativeLayout CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
			final RelativeLayout SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);
			
			NameBack.setText(Name);
			
			SearchingCategoryBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
									
					CategoryLayout.setVisibility(View.GONE);
					SearchingLayout.setVisibility(View.VISIBLE);
					new SearchAutoCompleteTextViewList().execute();
					
				}
			});
			
			SearchingBackBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
										
					CategoryLayout.setVisibility(View.VISIBLE);
					SearchingLayout.setVisibility(View.GONE);
					
					
				}
			});
			
			NameBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					finish();
				}
			});
			
		}
	 
	 
	 class SearchAutoCompleteTextViewList extends AsyncTask<String, String, String> {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				SearchAutoComplete_ID.clear();
				SearchAutoComplete_Name.clear();
				
				
			}

			@Override
			protected String doInBackground(String... args) {
				

				try {
					String url=URLs.Search_Keyword;
					//				String response12 = JsonParse.makeServiceCall(url);
					String response = JsonParse.makeServiceCall(url);

					responseSearch = "{"+'"'+"Category"+'"'+":"+response+"}";
					Log.e("---------responseSearch-------------", responseSearch);

				} catch (Exception e) {
				}

				return null;
			}

			@Override
			protected void onPostExecute(String file_url) {
				runOnUiThread(new Runnable() {
					@SuppressLint("SetJavaScriptEnabled")
					@Override
					public void run() {

						try {


							parseXmlresponseForProductCatagory(responseSearch);

						} catch (Exception e) { }

					}

					private void parseXmlresponseForProductCatagory(String MyResponse) throws JSONException 
					{


						if (responseSearch != null) 
						{
							
							JSONObject jsonObj = new JSONObject(MyResponse);

							// Getting JSON Array node
							JSONArray ContentList = jsonObj.getJSONArray("Category");


							// looping through All Contacts
							for (int i = 0; i < ContentList.length(); i++)
							{

								JSONObject cate = ContentList.getJSONObject(i);

								String _id = cate.getString("id");
								String _keyword_name = cate.getString("keyword");
								
								SearchAutoComplete_ID.add(_id);
								SearchAutoComplete_Name.add(_keyword_name);
							
							}

						}
						else 
						{
							Log.e("ServiceHandler", "Couldn't get any data from the url");
						}

						ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SearchDetails.this,android.R.layout.simple_spinner_item, SearchAutoComplete_Name);
						dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						SearchAutoCompleteTextView.setAdapter(dataAdapter);
						
					}


				});
				
				
				SearchAutoCompleteTextView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						SearchAutoCompleteTextView.showDropDown();
					}
				});
				SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						
						TempData.SEARCH_ID=SearchAutoComplete_ID.get(arg2);
						Log.e("IDDDDD", String.valueOf(SearchAutoComplete_ID.get(arg2)));
					Intent in=new Intent(SearchDetails.this,SearchDetails.class);
					startActivity(in);
					
					
					}
				});
			}
		}
}
