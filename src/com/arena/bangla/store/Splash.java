package com.arena.bangla.store;

import com.arena.bangla.store.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Splash extends Activity{

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	String PINCODE_="";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);  
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_animation);

		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        PINCODE_ = sharedpreferences.getString("PINCODE", "");
        
		WebView webView = (WebView)findViewById(R.id.webView1);

		WebSettings webSetting = webView.getSettings();
		webSetting.setBuiltInZoomControls(true);
		webSetting.setJavaScriptEnabled(true);

		webView.setWebViewClient(new WebViewClient());
		webView.loadUrl("file:///android_asset/web/splash_screen.html");

		 if(sharedpreferences.getString("Language", "").equals(""))
			{
				SharedPreferences.Editor editor2 = sharedpreferences.edit();
				editor2.putString("Language", "?lang=bn");
//				editor2.putString("Language", "");
				editor2.commit();
			}
		 
		 
		Handler handler = new Handler(); 
		handler.postDelayed(new Runnable() { 
			@Override
			public void run() { 
				
				Intent intent = new Intent( Splash.this, TemplateHomeScreenActivity.class);
				startActivity(intent);
				finish(); 
			} 
		}, 5000);
		
		 
		/* Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE"+PINCODE_);
	        
	        if(PINCODE_.equals(""))
	        {
	        	Handler handler = new Handler(); 
	    		handler.postDelayed(new Runnable() { 
	    			@Override
	    			public void run() { 
	    				Intent intent = new Intent( Splash.this, NewNumberLoginScreenActivity.class);
	    				startActivity(intent);
	    				finish(); 
	    			} 
	    		}, 5000);
	        }
	        else
	        {
	        	String Screen = sharedpreferences.getString("HomeScreen", null);
	        	Log.e("RRRRRRRRRRRRRRRRRRR", "RRRRRRRRRRRRRRRRRRR"+Screen);
	        	
	        	if(Screen.equals("TemplateHomeScreenActivity"))
	        	{
	        		Handler handler = new Handler(); 
		    		handler.postDelayed(new Runnable() { 
		    			@Override
		    			public void run() { 
		    				
		    				Intent intent = new Intent( Splash.this, TemplateHomeScreenActivity.class);
		    				startActivity(intent);
		    				finish(); 
		    			} 
		    		}, 5000);
	        	}
	        	else
	        	{
	        		Handler handler = new Handler(); 
		    		handler.postDelayed(new Runnable() { 
		    			@Override
		    			public void run() { 
		    				Intent intent = new Intent( Splash.this, MainActivity_Static.class);
		    				startActivity(intent);
		    				finish(); 
		    			} 
		    		}, 5000);
	        	}
	        }*/
		
	} 
	

}
