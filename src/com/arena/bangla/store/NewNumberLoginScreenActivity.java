package com.arena.bangla.store;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.arena.json.XMLfunctions_back;
import com.arena.json.JsonParse;
import com.arena.json.URLs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NewNumberLoginScreenActivity extends Activity{

	String PinCode_ForLoign ="";
	String Login_Status = "",Login_Message="";
	String JSONResponse_Type_Login="";
	String MobileNumber ="";
	String PinCode ="";
	String Amount="";
	String Bill_Status = "",Bill_Message="",Regi_Status = "",Regi_Message="",Pin_Status = "",Pin_Message="";
	String Original_Price ="";
	String lat = "";
	String lng = "";
	String IMEI_No="";
	String JSONResponse_Type_Regi="",JSONResponse_Type_Pin="",JSONResponse_Type_Payment="";
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	EditText LoginEditTxt;
	TextView TView;
	Button LoginBtn;
	String xml;
	

	

	ProgressDialog pDialog,dlDialog;
	String response;
	String response12;
	public static SQLiteDatabase myDB;
	
	String NetworkOperatorName="";
	String Content_Id,Which_Screen, TitleName,SubLink;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login_screen);
		
		Bundle b = getIntent().getExtras();
		if (b!=null)
		{
			Content_Id = b.getString("Content_Id");
			Which_Screen = b.getString("which_screen");
			TitleName = b.getString("TitleName");
			SubLink = b.getString("SubLink");
		}
		 
		 myDB =  this.openOrCreateDatabase("apponit_db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
	        myDB.setVersion(1);
	        myDB.setLocale(Locale.getDefault());
	        
	        
	        myDB.execSQL("CREATE TABLE IF NOT EXISTS " +
	        		"apponit_table" +
	        		" (_id INTEGER PRIMARY KEY, mobile_number TEXT, pin TEXT, hash_tag TEXT);");

		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

		TelephonyManager TM = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	    
		NetworkOperatorName = TM.getNetworkOperatorName();
		IMEI_No = TM.getDeviceId();
		 Log.e("Data Connected : ", " ---------- IMEI_No--"+IMEI_No);
		
		LoginEditTxt = (EditText)findViewById(R.id.editTextMSISDNLogin);
		TView = (TextView)findViewById(R.id.Text);
		LoginBtn = (Button)findViewById(R.id.buttonLogin);
		
		TView.setText("Here enter the Moblie number and click the next button then you will get iToken by SMS within a few time");
	
//		LoginEditTxt.setText("01716526944");
		
		
		
		 ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			//mobile
			State mobile = conMan.getNetworkInfo(0).getState();
			//wifi
			State wifi = conMan.getNetworkInfo(1).getState();

			if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) 
			{
			    //mobile
				if(NetworkOperatorName.equals("Robi"))
				{
					new GetMobileNumberAsync().execute();
				}
				else
				{
					Toast.makeText(NewNumberLoginScreenActivity.this, "Please enable the Robi internet", Toast.LENGTH_LONG).show();
				}
//				 Log.e("Data Connected : ", " ---------- mobile");
			}
			else
			{
				Toast.makeText(NewNumberLoginScreenActivity.this, "Please enable the Robi internet", Toast.LENGTH_LONG).show();
			}
			/*else if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) 
			{
			    //wifi
				Log.e("Data Connected : ", " ---------- wifi");
			}*/
		
		
		LoginBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				MobileNumber = LoginEditTxt.getText().toString();
				/*SharedPreferences.Editor editor = sharedpreferences.edit();
				editor.putString("MobileNumberForPurchase", "88"+MobileNumber);
				editor.commit();   
				new User_RegistrationAsyn().execute();*/
				
				if(MobileNumber.equals(""))
				{
					Toast.makeText(NewNumberLoginScreenActivity.this, "Please Enter Number", Toast.LENGTH_LONG).show();
				}
				else
				{
					String _MobileNumber = MobileNumber.substring(0, 3);
					 if(_MobileNumber.equals("018"))
					{
						
						SharedPreferences.Editor editor = sharedpreferences.edit();
						editor.putString("MobileNumberForPurchase", "88"+MobileNumber);
						editor.commit();   
						
						onSendMSg("21213","bstor 88"+MobileNumber+" "+IMEI_No);
						
					}
					else
					{
						Toast.makeText(NewNumberLoginScreenActivity.this, "Please Enter Robi Number", Toast.LENGTH_LONG).show();
						
					  /* SharedPreferences.Editor editor = sharedpreferences.edit();
						editor.putString("MobileNumberForPurchase", "88"+MobileNumber);
						editor.commit();   
						new User_RegistrationAsyn().execute();*/
					}
					
					
				}
							
			}
		});
		
		
	}
	
	
	class User_RegistrationAsyn extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			GPSTracker gps = new GPSTracker(getApplicationContext());
			if (gps.canGetLocation()) {

				lat = String.valueOf(gps.getLatitude());
				lng = String.valueOf(gps.getLongitude());

			}

			pDialog = new ProgressDialog(NewNumberLoginScreenActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			String final_json = "";

			String url=URLs.User_Registration;
			JSONObject jsonObject12 = new JSONObject();

			try {

				jsonObject12.accumulate("mobile_no", "88"+MobileNumber);
				jsonObject12.accumulate("imei_no", IMEI_No);
				jsonObject12.accumulate("location", lat+","+lng);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String	my_final_json = jsonObject12.toString().substring(1, jsonObject12.toString().length()-1)+",";
			String my_final_json1 = my_final_json.substring(0, my_final_json.length()-1);
			final_json = "{"+my_final_json1+"}";
			Log.e("Subroto","json :"+final_json);

			JSONResponse_Type_Regi = JsonParse.makeServiceCall(url,final_json);

			JSONResponse_Type_Regi = "{"+'"'+"result"+'"'+":["+JSONResponse_Type_Regi+"]}";

			Log.e("Subroto","response Tour User: "+JSONResponse_Type_Regi);

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {

						parseXmlresponseForProductCatagory(JSONResponse_Type_Regi);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String response) throws JSONException 
				{
					if (response != null) 
					{
						JSONObject jsonObj = new JSONObject(response);

						// Getting JSON Array node
						JSONArray TypArray = jsonObj.getJSONArray("result");

						// looping through All Contacts
						for (int i = 0; i < TypArray.length(); i++) {
							JSONObject c = TypArray.getJSONObject(i);

							Regi_Status = c.getString("status");
							Regi_Message = c.getString("message");
						}
						
						if(Regi_Status.equals("success"))
//						if(Regi_Status.equals("failed"))
						{

							Intent idn = new Intent(NewNumberLoginScreenActivity.this, LoginScreenVerificationActivity.class);
							startActivity(idn);
							finish();
						}
						else
						{
							Toast.makeText(NewNumberLoginScreenActivity.this,"Mobile number is already registered.", Toast.LENGTH_LONG).show();
							/*if(Regi_Message.equalsIgnoreCase("Mobile number is already registered."))
							{
								
								SharedPreferences.Editor editor = sharedpreferences.edit();
								editor.putString("PINCODE", PinCode);
								editor.commit(); 
								
								String mobile_input = sharedpreferences.getString("MobileNumberForPurchase", null);
								String hashtag = "HashTag";
								
								
								myDB.execSQL("INSERT INTO " +
						                "apponit_table" +
						                " Values (null,'"+mobile_input+"','"+PinCode+"','"+hashtag+"');");
								
								Intent idn = new Intent(NewNumberLoginScreenActivity.this, TemplateHomeScreenActivity.class);
								startActivity(idn);
								finish();
							}
							else
							{
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewNumberLoginScreenActivity.this);

								//						alertDialogBuilder.setTitle("");
								alertDialogBuilder
								.setMessage(Regi_Message)
								.setCancelable(false)
								.setPositiveButton("OK",new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {

										dialog.dismiss();
									}
								});

								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder.create();
								alertDialog.show();	
							}*/
							
						}

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

				}

			});

			pDialog.dismiss();

			

		}
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			
//			finish();
			Log.e("TTTTTTTTTTTTTTTTttt", "HHHHHHHHHHHHHHH--------------"+sharedpreferences.getString("MobileNumberForPurchase", ""));
			if(!sharedpreferences.getString("MobileNumberForPurchase", "").equals(""))
			{
				/*Intent idn = new Intent(NewNumberLoginScreenActivity.this, MainActivity_Static.class);
				startActivity(idn);
				finish();*/
				
				String Screen2 = sharedpreferences.getString("HomeScreen", null);

				Log.e("TTTTTTTTTTTTTTTTttt", "HHHHHHHHHHHHHHHHHHHH"+Screen2);
				if(Screen2.equals("MainActivity_Static"))
				{
					Intent idd = new Intent(NewNumberLoginScreenActivity.this, MainActivity_Static.class);
					startActivity(idd);
					finish();
				}
				else
				{
					Intent idd = new Intent(NewNumberLoginScreenActivity.this, TemplateHomeScreenActivity.class);
					startActivity(idd);
					finish(); 
				}
			}
			else
				finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
	
	
	//------------------------------SMS Send----------------------
	public void onSendMSg(String phoneNo, String msg)
	{
		/*  String phoneNo = etPhoneNo.getText().toString();
	    String msg = etMsg.getText().toString();*/
		try {

			String SENT = "sent";
			String DELIVERED = "delivered";

			Intent sentIntent = new Intent(SENT);
			/*Create Pending Intents*/
			PendingIntent sentPI = PendingIntent.getBroadcast(
					getApplicationContext(), 0, sentIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);

			Intent deliveryIntent = new Intent(DELIVERED);

			PendingIntent deliverPI = PendingIntent.getBroadcast(
					getApplicationContext(), 0, deliveryIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			/* Register for SMS send action */
			registerReceiver(new BroadcastReceiver() {

				@Override
				public void onReceive(Context context, Intent intent) {
					String result = "";

					switch (getResultCode()) {

					case Activity.RESULT_OK:
						result = "SMS Transmission successful";
						
						
						SharedPreferences.Editor editor = sharedpreferences.edit();
						editor.putString("PINCODE", PinCode);
						editor.commit(); 
						
						String mobile_input = sharedpreferences.getString("MobileNumberForPurchase", null);
						String hashtag = "HashTag";
						
						
						myDB.execSQL("INSERT INTO " +
				                "apponit_table" +
				                " Values (null,'"+mobile_input+"','"+PinCode+"','"+hashtag+"');");
						
						
						Intent idn = new Intent(NewNumberLoginScreenActivity.this, LoginScreenVerificationActivity.class);
						idn.putExtra("Content_Id", Content_Id);
						idn.putExtra("Which_Screen", Which_Screen);
						idn.putExtra("TitleName", TitleName);
						idn.putExtra("SubLink", SubLink);
						startActivity(idn);
						finish();
						
						
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						result = "SMS Transmission failed";
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						result = "Radio off";
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						result = "No PDU defined";
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						result = "No service";
						break;
					}

					Toast.makeText(getApplicationContext(), result,
							Toast.LENGTH_LONG).show();
				}

			}, new IntentFilter(SENT));
			/* Register for Delivery event */
			registerReceiver(new BroadcastReceiver() {

				@Override
				public void onReceive(Context context, Intent intent) {
					Toast.makeText(getApplicationContext(), "SMS Deliverd",
							Toast.LENGTH_LONG).show();
				}

			}, new IntentFilter(DELIVERED));

			/*Send SMS*/
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phoneNo, null, msg, sentPI,
					deliverPI);
		} catch (Exception ex) {
			Toast.makeText(getApplicationContext(),
					ex.getMessage().toString(), Toast.LENGTH_LONG)
					.show();
			ex.printStackTrace();
		}
	}
	
	
	
	private class GetMobileNumberAsync extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(NewNumberLoginScreenActivity.this);

			pDialog.setMessage("Retriving your mobile number, Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show(); 
		}

		@Override
		protected Void doInBackground(String... params) {
			xml = XMLfunctions_back.getXML(URLs.URL_FORWARD);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			runOnUiThread(new Runnable() {
				@Override
				@SuppressWarnings("deprecation")
				public void run() {

//					lyotDismisKeyboard.setVisibility(View.VISIBLE);

				/*	LinearLayout l = (LinearLayout) findViewById(R.id.firsttimelogin);
					l.setVisibility(View.VISIBLE);*/

					Log.e("mobile---------------", xml);
					if (xml.equals("serverError")) {

					} else {
						String MobileNo = xml.substring(13, 24);
						
//						Log.e("x---------------", ""+MobileNo.subSequence(0, 3));
						
						if(MobileNo.subSequence(0, 3).equals("018"))
						{
							LoginEditTxt.setText(MobileNo);
							LoginEditTxt.setEnabled(false);
						}
//						Document doc = XMLfunctions.XMLfromString(xml);
						
						/*NodeList nodes = doc
								.getElementsByTagName("IsValidUser");

						for (int j = 0; j < nodes.getLength(); j++) {
							Element y = (Element) nodes.item(j);

							urlMobileNo = XMLfunctions.getValue(y, "Mobile_No").trim();
							urlPassword = XMLfunctions.getValue(y, "Password").trim();

						}

						if (!urlMobileNo.equals("")) {
							MSISDNEd.setText("0" + urlMobileNo);
							PassEd.requestFocus();
						}*/
						pDialog.dismiss();
					}

				}
			});
		}
	}


}
