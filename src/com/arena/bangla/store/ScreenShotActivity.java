package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.image.loader.ImageLoader;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class ScreenShotActivity extends Activity {
	ProgressDialog pDialog;
	String response;
	String response12;
	AdapterForMainScreen adapter;

	ArrayList<HashMap<String, String>> InfoList;
	ArrayList<HashMap<String, String>> ContentArrayList;
	ListView mainsListview;

	Button AppBtn, SearchingCategoryBtn, SearchingSearchingBtn;
	RelativeLayout CategoryLayout, SearchingLayout;
	
	public ImageLoader imageLoader; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.screen_shot);
		
		Bundle b = getIntent().getExtras();
		
		String ImageUrl = b.getString("Screenshot");
		
		imageLoader=new ImageLoader(ScreenShotActivity.this);
		
		
		ImageView ScreenShot = (ImageView) findViewById(R.id.ScreenShot);

		imageLoader.DisplayImage(ImageUrl,ScreenShot);

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			
			finish();
			/*if(Which_Screen.equals("MainScreen"))
			{
				Intent idd = new Intent(AppsDetailsViewActivity.this, MainActivity_Static.class);
				startActivity(idd);
				finish();
			}
			
			 if(Which_Screen.equals("SearchDetails"))
			{
				finish();
			}
			 
			 if(Which_Screen.equals("InnerScreen"))
			{
				Intent idd = new Intent(AppsDetailsViewActivity.this, CategorywiseAppsDetailsActivity.class);
				startActivity(idd);
				finish();
			}
			 if(Which_Screen.equals("sub_inner"))
			 {
				 Intent idd = new Intent(AppsDetailsViewActivity.this, CategorywiseAppsDetailsActivity.class);
				 startActivity(idd);
				 finish();
			 }*/

			return true;
		}
		return super.onKeyDown(keyCode, event);

	}

}
