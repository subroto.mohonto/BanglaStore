package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.AdapterForMainScreenLatest;
import com.arena.adapter.AdapterLatestForMainScreen;
import com.arena.bangla.store.MainActivity.MainScreenShow;
import com.arena.image.loader.ImageLoader;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import com.arena.menu.MenuDialog;
import com.devsmart.android.ui.HorizontalListView;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity_Static extends ParentActivity implements OnMenuItemClickListener {

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	String lat = "";
	String lng = "";
	String Language ="";

	ProgressDialog pDialog;
	String response;
	String response12;
	AdapterForMainScreen adapter, adapter2, adapter3, adapter4,adapter5,adapter6;
	AdapterLatestForMainScreen adapterLatest;

	ArrayList<HashMap<String, String>> InfoList;
	ArrayList<HashMap<String, String>> ContentArrayListVideos;
	ArrayList<HashMap<String, String>> ContentArrayListMp3;
	ArrayList<HashMap<String, String>> ContentArrayListWallpaper;
	ArrayList<HashMap<String, String>> ContentArrayListApp;
	ArrayList<HashMap<String, String>> ContentArrayListGame;
	ArrayList<HashMap<String, String>> ContentArrayListBook;
	ArrayList<HashMap<String, String>> ContentArrayListLatest;

	ListView mainsListview;

	Button SearchingCategoryBtn, SearchingSearchingBtn, CategoryBtn;
	//	Button AppBtn, SearchingCategoryBtn, SearchingSearchingBtn, CategoryBtn,VideoBtn, AudioBtn,WallpaperBtn, GameBtn, BooksBtn;
//	RelativeLayout App_RelativeLayout,Video_RelativeLayout,Audio_RelativeLayout, Wallpaper_RelativeLayout,Game_RelativeLayout,Books_RelativeLayout,LatestNews_RelativeLayout;
	ImageView SearchingBackBtn;
	RelativeLayout CategoryLayout, SearchingLayout;


	HorizontalListView AppsProjectsList, VideoProjectsList, Mp3ProjectsList, WallpaperProjectsList, GameProjectsList, BookProjectsList,LatestProjectsList;

	LinearLayout AppsLayout,VideoLayout, Mp3Layout, WallpaperLayout, GameLayout, BookLayout, LatestLayout;
	TextView appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv,LatestTv;

	TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv,LatestSubTv;

	Button appMoreBtn, videoMoreBtn, mp3MoreBtn, gameMoreBtn, bookMoreBtn, wallapperMoreBtn;
	AutoCompleteTextView SearchAutoCompleteTextView;

	ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
	ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();


	public ImageLoader imageLoader; 

	ImageView BannerImage;
	String Top_banner_image,Top_banner_content_id="",Top_banner_content_type_id="";
	ScrollView bgSc;
	String IMEI_No="",JSONResponse_Type="";
	ImageView ExitTv;
	Button MenuBtn;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_main_listwise);

		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

		TelephonyManager TM = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		IMEI_No = TM.getDeviceId();

		Log.e("$$$$$$$$$$$$$$$$$$$$$","$$$$$$$$$$$$$$$$$$$$$$$ IMEI_No: "+IMEI_No);

		ViewMenu(R.id.App_RelativeLayout,R.id.Video_RelativeLayout,R.id.MP3_RelativeLayout,R.id.Wallpaper_RelativeLayout,R.id.Game_RelativeLayout,R.id.Book_RelativeLayout,R.id.LatestNews_RelativeLayout,"1111");

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels;
		int height=dm.heightPixels/4;

		Log.e("****************", "(------width-------)"+width+"(------height-------)"+height);


		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.putString("HomeScreen", "MainActivity_Static");
		editor.commit();
		
		Language = sharedpreferences.getString("Language", "");
		
		if(Language.equals("English"))
		{
			Language="";		
		}

		imageLoader=new ImageLoader(MainActivity_Static.this);

		BannerImage = (ImageView)findViewById(R.id.glry_Latest_Image);

		BannerImage.getLayoutParams().height = height;
		
		MenuBtn = (Button)findViewById(R.id.MenuBtn);

		AppsProjectsList  = (HorizontalListView) findViewById(R.id.glry_Apps_HorizontalListView);
		VideoProjectsList  = (HorizontalListView) findViewById(R.id.glry_Video_HorizontalListView);
		Mp3ProjectsList  = (HorizontalListView) findViewById(R.id.glry_mp3_HorizontalListView);
		WallpaperProjectsList  = (HorizontalListView) findViewById(R.id.glry_Wallpaper_HorizontalListView);
		GameProjectsList  = (HorizontalListView) findViewById(R.id.glry_Game_HorizontalListView);
		BookProjectsList  = (HorizontalListView) findViewById(R.id.glry_Book_HorizontalListView);
		LatestProjectsList  = (HorizontalListView) findViewById(R.id.glry_Latest_HorizontalListView);
		Scrolling();

		AppsLayout  = (LinearLayout) findViewById(R.id.LinearLayoutApp);
		VideoLayout  = (LinearLayout) findViewById(R.id.LinearLayoutVideo);
		Mp3Layout  = (LinearLayout) findViewById(R.id.LinearLayoutMp3);
		WallpaperLayout  = (LinearLayout) findViewById(R.id.LinearLayoutWallpaper);
		GameLayout  = (LinearLayout) findViewById(R.id.LinearLayoutGame);
		BookLayout  = (LinearLayout) findViewById(R.id.LinearLayoutBooks);
		LatestLayout  = (LinearLayout) findViewById(R.id.LinearLayoutLatest);

		//		TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv;
		AppSubTv = (TextView)findViewById(R.id.txtAppSub);
		VideoSubTv = (TextView)findViewById(R.id.txtVideoSub);
		Mp3SubTv = (TextView)findViewById(R.id.txtmp3Sub);
		GameSubTv = (TextView)findViewById(R.id.txtGameSub);
		BookSubTv = (TextView)findViewById(R.id.txtBookSub);
		WallapperSubTv = (TextView)findViewById(R.id.txtWallpaperSub);
		LatestSubTv = (TextView)findViewById(R.id.txtLatestSub);
		LatestSubTv.setVisibility(View.GONE);

		appTv = (TextView)findViewById(R.id.txtApp);
		videoTv = (TextView)findViewById(R.id.txtVideo);
		mp3Tv = (TextView)findViewById(R.id.txtmp3);
		gameTv = (TextView)findViewById(R.id.txtGame);
		bookTv = (TextView)findViewById(R.id.txtBook);
		wallapperTv = (TextView)findViewById(R.id.txtWallpaper);
		LatestTv = (TextView)findViewById(R.id.txtLatest);


		appMoreBtn = (Button)findViewById(R.id.txtMoreApp);
		videoMoreBtn = (Button)findViewById(R.id.txtMoreVideo);
		mp3MoreBtn = (Button)findViewById(R.id.txtMoreMp3);
		gameMoreBtn = (Button)findViewById(R.id.txtMoreGame);
		bookMoreBtn = (Button)findViewById(R.id.txtMoreBook);
		wallapperMoreBtn = (Button)findViewById(R.id.txtMoreWallpaper);
		
		String Language2 = sharedpreferences.getString("Language", "");
	        if(Language2.equals("English"))
	        {
	        	appMoreBtn.setText("More");
	        	videoMoreBtn.setText("More");
	        	mp3MoreBtn.setText("More");
	        	gameMoreBtn.setText("More");
	        	bookMoreBtn.setText("More");
	        	wallapperMoreBtn.setText("More");
	        }
	        else
	        {
	        	appMoreBtn.setText("আরও");
	        	videoMoreBtn.setText("আরও");
	        	mp3MoreBtn.setText("আরও");
	        	gameMoreBtn.setText("আরও");
	        	bookMoreBtn.setText("আরও");
	        	wallapperMoreBtn.setText("আরও");
	        }
	
		SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
		SearchAutoCompleteTextView.setThreshold(1);

		ContentArrayListVideos = new ArrayList<HashMap<String, String>>();
		ContentArrayListMp3 = new ArrayList<HashMap<String, String>>();
		ContentArrayListWallpaper = new ArrayList<HashMap<String, String>>();
		ContentArrayListApp = new ArrayList<HashMap<String, String>>();
		ContentArrayListGame = new ArrayList<HashMap<String, String>>();
		ContentArrayListBook = new ArrayList<HashMap<String, String>>();
		ContentArrayListLatest = new ArrayList<HashMap<String, String>>();


		InfoList = new ArrayList<HashMap<String, String>>();

		mainsListview = (ListView)findViewById(R.id.mainsListview);

		SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
		//		SearchingSearchingBtn = (Button)findViewById(R.id.SearchingSearchingBtn);
		//		CategoryBtn = (Button)findViewById(R.id.category);

		SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);

		ExitTv = (ImageView)findViewById(R.id.Title1);


		CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
		SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);


		ContentArrayListVideos = new ArrayList<HashMap<String, String>>();
		InfoList = new ArrayList<HashMap<String, String>>();

		mainsListview = (ListView)findViewById(R.id.mainsListview);

		/*App_RelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		Video_RelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		Audio_RelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		Wallpaper_RelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		Game_RelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		Books_RelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		LatestNews_RelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);*/

		bgSc = (ScrollView)findViewById(R.id.ScrollViewApp);

		/*String hide="banner";
		String hide1="game";

		if(hide.equals("banner"))
			BannerImage.setVisibility(View.GONE);


		if(hide1.equals("game"))
			GameLayout.setVisibility(View.GONE);
		 */



		if(isInternetOn())
		{
			try {
				new MainScreenShow().execute();
				new TrackingAsyn().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			Toast.makeText(MainActivity_Static.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();

		}


		MenuBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				MenuDialog menu = new MenuDialog(MainActivity_Static.this); 
				menu.show(); 
			}
		});

		ExitTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						MainActivity_Static.this);

				// set title
				//						alertDialogBuilder.setTitle("Exit");

				// set dialog message
				alertDialogBuilder
				.setMessage("Do you want to Exit it?")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//									MainActivity.this.finish();

						finish();

					}
				})
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});


		BannerImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(Top_banner_content_type_id.equals("1"))
				{
					Intent idn = new Intent(MainActivity_Static.this, AppsDetailsViewActivity.class);
					idn.putExtra("Content_Id", Top_banner_content_id);
					idn.putExtra("SubLink", Top_banner_content_type_id);
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}

				if(Top_banner_content_type_id.equals("2"))
				{
					Intent idn = new Intent(MainActivity_Static.this, VideoDetailsActivity.class);
					idn.putExtra("Content_Id", Top_banner_content_id);
					idn.putExtra("SubLink", Top_banner_content_type_id);
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}

				if(Top_banner_content_type_id.equals("3"))
				{
					Intent idn = new Intent(MainActivity_Static.this, AudioDetailsActivity.class);
					idn.putExtra("Content_Id", Top_banner_content_id);
					idn.putExtra("SubLink", Top_banner_content_type_id);
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}

				if(Top_banner_content_type_id.equals("4"))
				{
					Intent idn = new Intent(MainActivity_Static.this, WallpaperDetailsActivity.class);
					idn.putExtra("Content_Id", Top_banner_content_id);
					idn.putExtra("SubLink", Top_banner_content_type_id);
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}


				if(Top_banner_content_type_id.equals("5"))
				{
					Intent idn = new Intent(MainActivity_Static.this, GameDetailsActivity.class);
					idn.putExtra("Content_Id", Top_banner_content_id);
					idn.putExtra("SubLink", Top_banner_content_type_id);
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}


				if(Top_banner_content_type_id.equals("6"))
				{
					Intent idn = new Intent(MainActivity_Static.this, BooksDetailsActivity.class);
					idn.putExtra("Content_Id", Top_banner_content_id);
					idn.putExtra("SubLink", Top_banner_content_type_id);
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}

			}
		});

		SearchingCategoryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~`");

				CategoryLayout.setVisibility(View.GONE);
				SearchingLayout.setVisibility(View.VISIBLE);
				new SearchAutoCompleteTextViewList().execute();

			}
		});

		SearchingBackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~`");

				CategoryLayout.setVisibility(View.VISIBLE);
				SearchingLayout.setVisibility(View.GONE);


			}
		});




		WallpaperProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub



				HashMap<String, String> data = ContentArrayListWallpaper.get(position);

				Intent idn = new Intent(MainActivity_Static.this, WallpaperDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("SubLink", data.get("type_id"));
				idn.putExtra("TitleName", data.get("content_title"));
				idn.putExtra("which_screen", "MainScreen");
				startActivity(idn);
				//				finish();

			}
		});
		Mp3ProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListMp3.get(position);

				Intent idn = new Intent(MainActivity_Static.this, AudioDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("SubLink", data.get("type_id"));
				idn.putExtra("TitleName", data.get("content_title"));
				idn.putExtra("which_screen", "MainScreen");
				startActivity(idn);
				//				finish();
			}
		});
		VideoProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListVideos.get(position);

				Intent idn = new Intent(MainActivity_Static.this, VideoDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));		
				idn.putExtra("SubLink", data.get("type_id"));		
				idn.putExtra("TitleName", data.get("content_title"));
				idn.putExtra("which_screen", "MainScreen");
				startActivity(idn);
				//				finish();

			}
		});

		AppsProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListApp.get(position);

				Intent idn = new Intent(MainActivity_Static.this, AppsDetailsViewActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("SubLink", data.get("type_id"));
				idn.putExtra("TitleName", data.get("content_title"));
				idn.putExtra("which_screen", "MainScreen");
				startActivity(idn);
				//				finish();

			}
		});

		GameProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListGame.get(position);

				Intent idn = new Intent(MainActivity_Static.this, GameDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("SubLink", data.get("type_id"));
				idn.putExtra("TitleName", data.get("content_title"));
				idn.putExtra("which_screen", "MainScreen");
				startActivity(idn);
				//				finish();


			}
		});

		BookProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListBook.get(position);
				Intent idn = new Intent(MainActivity_Static.this, BooksDetailsActivity.class);
				idn.putExtra("Content_Id", data.get("content_id"));
				idn.putExtra("SubLink", data.get("type_id"));
				idn.putExtra("TitleName", data.get("content_title"));
				idn.putExtra("which_screen", "MainScreen");
				startActivity(idn);
				//				finish();

			}
		});


		LatestProjectsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				HashMap<String, String> data = ContentArrayListLatest.get(position);
				//	if(data.get(""))
				/*LatestContent.put("latest_content_id", latest_content_id);
				LatestContent.put("type_id", type_id);*/
				//				Toast.makeText(MainActivity_Static.this, "type_id "+data.get("type_id")+" \nlatest_content_id "+data.get("latest_content_id"),Toast.LENGTH_LONG).show();

				if(data.get("type_id").equals("1"))
				{
					Intent idn = new Intent(MainActivity_Static.this, AppsDetailsViewActivity.class);
					idn.putExtra("Content_Id", data.get("latest_content_id"));
					idn.putExtra("SubLink", data.get("type_id"));
					idn.putExtra("TitleName", data.get("content_title"));
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}

				if(data.get("type_id").equals("2"))
				{
					Intent idn = new Intent(MainActivity_Static.this, VideoDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("latest_content_id"));
					idn.putExtra("SubLink", data.get("type_id"));
					idn.putExtra("TitleName", data.get("content_title"));
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}

				if(data.get("type_id").equals("3"))
				{
					Intent idn = new Intent(MainActivity_Static.this, AudioDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("latest_content_id"));
					idn.putExtra("SubLink", data.get("type_id"));
					idn.putExtra("TitleName", data.get("content_title"));
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}

				if(data.get("type_id").equals("4"))
				{
					Intent idn = new Intent(MainActivity_Static.this, WallpaperDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("latest_content_id"));
					idn.putExtra("SubLink", data.get("type_id"));
					idn.putExtra("TitleName", data.get("content_title"));
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}


				if(data.get("type_id").equals("5"))
				{
					Intent idn = new Intent(MainActivity_Static.this, GameDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("latest_content_id"));
					idn.putExtra("SubLink", data.get("type_id"));
					idn.putExtra("TitleName", data.get("content_title"));
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}


				if(data.get("type_id").equals("6"))
				{
					Intent idn = new Intent(MainActivity_Static.this, BooksDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("latest_content_id"));
					idn.putExtra("SubLink", data.get("type_id"));
					idn.putExtra("TitleName", data.get("content_title"));
					idn.putExtra("which_screen", "MainScreen");
					startActivity(idn);
					//					finish();
				}


			}
		});




		/*LatestNews_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, LatestNewsActivity.class);
				idn.putExtra("TitleName", "সর�?বশেষ সংবাদ");
				idn.putExtra("SubLink", "1000");
				startActivity(idn);
				//				finish();


			}
		});*/


		/*App_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("TitleName", "অ�?যাপস");
				idn.putExtra("SubLink", "1");
				startActivity(idn);
				//				finish();


			}
		});*/

		appMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("SubLink", "1");
				idn.putExtra("TitleName", "অ�?যাপস");
				startActivity(idn);
				//				finish();


			}
		});


		/*Video_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("TitleName", "ভিডিও");
				idn.putExtra("SubLink", "2");
				startActivity(idn);
				//				finish();


			}
		});*/

		videoMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("TitleName", "ভিডিও");
				idn.putExtra("SubLink", "2");
				startActivity(idn);
				//				finish();


			}
		});

		/*Audio_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("TitleName", "�?মপিথ�?রি");
				idn.putExtra("SubLink", "3");
				startActivity(idn);
				//				finish();


			}
		});*/

		mp3MoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("SubLink", "3");
				idn.putExtra("TitleName", "�?মপিথ�?রি");
				startActivity(idn);
				//				finish();


			}
		});
		/*Game_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("TitleName", "গেইম");
				idn.putExtra("SubLink", "5");
				startActivity(idn);
				//				finish();


			}
		});*/

		gameMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("SubLink", "5");
				idn.putExtra("TitleName", "গেইম");
				startActivity(idn);
				//				finish();


			}
		});
		/*Wallpaper_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("TitleName", "ওয়ালপেপার");
				idn.putExtra("SubLink", "4");
				startActivity(idn);
				//				finish();


			}
		});*/

		wallapperMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("SubLink", "4");
				idn.putExtra("TitleName", "ওয়ালপেপার");
				startActivity(idn);
				//				finish();


			}
		});
	/*	Books_RelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("TitleName", "বই");
				idn.putExtra("SubLink", "6");
				startActivity(idn);
				//				finish();


			}
		});
*/
		bookMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(MainActivity_Static.this, Category_Wise_Activity.class);
				idn.putExtra("SubLink", "6");
				idn.putExtra("TitleName", "বই");
				startActivity(idn);
				//				finish();


			}
		});

		SearchAutoCompleteTextView.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+ SearchAutoComplete_Name.get(position));
				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+SearchAutoComplete_ID.get(position));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		/*SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+SearchAutoComplete_Name.get(arg2));
				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+SearchAutoComplete_ID.get(arg2));
			}
		});*/

		/*SearchAutoCompleteTextView.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

				String SearchAutoComplete_ID12 = SearchAutoComplete_ID.get(arg2);

				Log.e("****************", "&&&&&&&&&&&&&&&&&&&&&&"+SearchAutoComplete_Name.get(arg2));
				if(!SpASM.getSelectedItem().toString().equalsIgnoreCase("Select Asm"))
				{
					new TourDsmList().execute();
					//Log.d("adnan","HH:"+selectedAsm);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});*/

	}


	private void Scrolling() {
		// TODO Auto-generated method stub

		ScrollView ScrollViewApp=(ScrollView)findViewById(R.id.ScrollViewApp);


		ScrollViewApp.setOnTouchListener(new ListView.OnTouchListener() {

			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {


				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						return true;
					else
						return false;

				}


				v.onTouchEvent(event);

				return true;
			}
		});





		AppsProjectsList.setOnTouchListener(new ListView.OnTouchListener() {


			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						v.getParent().requestDisallowInterceptTouchEvent(true);
					else
						v.getParent().requestDisallowInterceptTouchEvent(false);

				}
				return false;
			}
		});


		VideoProjectsList.setOnTouchListener(new ListView.OnTouchListener() {


			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						v.getParent().requestDisallowInterceptTouchEvent(true);
					else
						v.getParent().requestDisallowInterceptTouchEvent(false);

				}
				return false;
			}
		});


		Mp3ProjectsList.setOnTouchListener(new ListView.OnTouchListener() {


			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						v.getParent().requestDisallowInterceptTouchEvent(true);
					else
						v.getParent().requestDisallowInterceptTouchEvent(false);

				}
				return false;
			}
		});


		WallpaperProjectsList.setOnTouchListener(new ListView.OnTouchListener() {


			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						v.getParent().requestDisallowInterceptTouchEvent(true);
					else
						v.getParent().requestDisallowInterceptTouchEvent(false);

				}
				return false;
			}
		});


		GameProjectsList.setOnTouchListener(new ListView.OnTouchListener() {
			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						v.getParent().requestDisallowInterceptTouchEvent(true);
					else
						v.getParent().requestDisallowInterceptTouchEvent(false);

				}
				return false;
			}
		});

		BookProjectsList.setOnTouchListener(new ListView.OnTouchListener() {


			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						v.getParent().requestDisallowInterceptTouchEvent(true);
					else
						v.getParent().requestDisallowInterceptTouchEvent(false);

				}
				return false;
			}
		});


		LatestProjectsList.setOnTouchListener(new ListView.OnTouchListener() {


			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						v.getParent().requestDisallowInterceptTouchEvent(true);
					else
						v.getParent().requestDisallowInterceptTouchEvent(false);

				}
				return false;
			}
		});




	}


	class MainScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();



			/*	pDialog = new ProgressDialog(MainActivity_Static.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();*/
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.MainScreen+Language;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
				//				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Category"+'"'+":["+response12+"]}";
				Log.e("---------response-------------", response12);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Category"+'"'+":}";

					Log.e("---------respon-------------", respon);

					if (!response.equals(respon)) 
					{

						bgSc.setVisibility(View.VISIBLE);


						Log.e("---------response-------------", response);


						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);



							String latest_content12 = cate.getString("latest_content");

							String home_content12 = cate.getString("home_content");

							String TopBanner_content12 = cate.getString("top_banner");

							Log.e("---------latest_content12-------------", latest_content12);
							Log.e("---------home_content-------------", home_content12);
							Log.e("---------Top_content-------------", TopBanner_content12);


							String Latest_ContentResponse = "{"+'"'+"latest_content"+'"'+":["+latest_content12+"]}";
							String Home_ContentResponse = "{"+'"'+"home_content"+'"'+":"+home_content12+"}";
							String TopBanner_ContentResponse = "{"+'"'+"top_banner"+'"'+":"+TopBanner_content12+"}";

							Log.e("---------latest_contentResponse-------------", Latest_ContentResponse);
							Log.e("---------Home_ContentResponse-------------", Home_ContentResponse);
							Log.e("---------TopBanner_ContentResponse-------------", TopBanner_ContentResponse);


							JSONObject TopBanner_ContentJsonObj = new JSONObject(TopBanner_ContentResponse);

							JSONObject TOP_BC = TopBanner_ContentJsonObj.getJSONObject("top_banner");

							Top_banner_content_id = TOP_BC.getString("content_id");
							Top_banner_content_type_id = TOP_BC.getString("content_type_id");
							Top_banner_image = TOP_BC.getString("banner");


							Log.e("---------Top_banner_content_id-------------", Top_banner_content_id);
							Log.e("---------Top_banner_image-****&&&&&&&-------------", Top_banner_image);

							/*for (int j2 = 0; j2 < TopBanner_Content_List.length(); j2++)
							{
								JSONObject TOP_BC = TopBanner_Content_List.getJSONObject(j2);
								Top_banner_content_id = TOP_BC.getString("content_id");
								Top_banner_image = TOP_BC.getString("banner");
							}*/



							JSONObject Latest_ContentJsonObj = new JSONObject(Latest_ContentResponse);
							JSONArray Latest_Content_List = Latest_ContentJsonObj.getJSONArray("latest_content");

							// content node is JSON Object

							for (int j = 0; j < Latest_Content_List.length(); j++)
							{
								JSONObject LCL = Latest_Content_List.getJSONObject(j);
								String category_title = LCL.getString("category_title");
								LatestTv.setText(category_title);
								Log.e("---------category_title-------------", category_title);

								String content_for_latest = LCL.getString("content");
								Log.e("---------content_for_latest-------------", content_for_latest);

								String Latest_Content_Content_Response = "{"+'"'+"latest_content_content"+'"'+":"+content_for_latest+"}";
								Log.e("---------Latest_Content_Content_Response-------------", Latest_Content_Content_Response);

								JSONObject Latest_ContentContent_JsonObj = new JSONObject(Latest_Content_Content_Response);

								JSONArray Latest_ContentContent_List = Latest_ContentContent_JsonObj.getJSONArray("latest_content_content");

								if(Latest_ContentContent_List.length()!=0)
								{

									for (int k = 0; k < Latest_ContentContent_List.length(); k++)
									{
										JSONObject LCCL = Latest_ContentContent_List.getJSONObject(k);

										String latest_content_id = LCCL.getString("id");
										String type_id = LCCL.getString("type_id");
										String content_title = LCCL.getString("content_title");
										String preview = LCCL.getString("preview");
										String total_download = LCCL.getString("total_download");
										String payment_type = LCCL.getString("payment_type");
										String price = LCCL.getString("price");
										String rating = LCCL.getString("rating");


										HashMap<String, String> LatestContent = new HashMap<String, String>();

										LatestContent.put("latest_content_id", latest_content_id);
										LatestContent.put("type_id", type_id);
										LatestContent.put("content_title", content_title);
										LatestContent.put("preview", preview);
										LatestContent.put("total_download", total_download);
										LatestContent.put("payment_type", payment_type);
										LatestContent.put("price", price);
										LatestContent.put("rating", rating);

										ContentArrayListLatest.add(LatestContent);
									}
								}
								else
								{
									LatestLayout.setVisibility(View.GONE);
								}




							}


							JSONObject Home_ContentJsonObj = new JSONObject(Home_ContentResponse);
							JSONArray Home_Content_List = Home_ContentJsonObj.getJSONArray("home_content");

							for (int i1 = 0; i1 < Home_Content_List.length(); i1++)
							{

								JSONObject cateHome = Home_Content_List.getJSONObject(i1);

								String _id = cateHome.getString("type_id");
								String _type_name = cateHome.getString("type_name");
								String _short_description = cateHome.getString("short_description");


								//								appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv;

								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_id);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_type_name);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_short_description);


								String ContentResponse = cateHome.getString("content");

								if(_id.equals("1")&&ContentResponse.equals("[]"))
									AppsLayout.setVisibility(View.GONE);


								if(_id.equals("2")&&ContentResponse.equals("[]"))

									VideoLayout.setVisibility(View.GONE);


								if(_id.equals("3")&&ContentResponse.equals("[]"))
									Mp3Layout.setVisibility(View.GONE);



								if(_id.equals("4")&&ContentResponse.equals("[]"))
									WallpaperLayout.setVisibility(View.GONE);


								if(_id.equals("5")&&ContentResponse.equals("[]"))
									GameLayout.setVisibility(View.GONE);



								if(_id.equals("6")&&ContentResponse.equals("[]"))
									BookLayout.setVisibility(View.GONE);


								//								TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv;

								if(_id.equals("1"))
								{
									appTv.setText(_type_name);
									AppSubTv.setText(_short_description);
								}

								if(_id.equals("2"))
								{
									videoTv.setText(_type_name);
									VideoSubTv.setText(_short_description);
								}

								if(_id.equals("3"))
								{
									mp3Tv.setText(_type_name);
									Mp3SubTv.setText(_short_description);
								}

								if(_id.equals("4"))
								{
									wallapperTv.setText(_type_name);
									WallapperSubTv.setText(_short_description);
								}

								if(_id.equals("5"))
								{
									gameTv.setText(_type_name);
									GameSubTv.setText(_short_description);
								}

								if(_id.equals("6"))
								{
									bookTv.setText(_type_name);
									BookSubTv.setText(_short_description);
								}




								if(!ContentResponse.equals("[]"))
								{

									HashMap<String, String> Info = new HashMap<String, String>();
									Info.put("id", _id);
									Info.put("type_name", _type_name);
									Info.put("short_description", _short_description);



									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_id);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_type_name);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_short_description);


									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";

									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);

									JSONArray Content_List = ContentJsonObj.getJSONArray("content");

									// content node is JSON Object

									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject c = Content_List.getJSONObject(j);

										String _content_id = c.getString("id");
										String _content_title = c.getString("content_title");
										String _preview = c.getString("preview");
										String _rating = c.getString("rating");


										HashMap<String, String> Content12 = new HashMap<String, String>();


										String type_name = Info.get("type_name");
										String type_id = Info.get("id");
										Log.e("AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC", "AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC"+type_name);

										if(type_id.equals("1"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);



											ContentArrayListApp.add(Content12);

										}


										if(type_id.equals("2"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);



											ContentArrayListVideos.add(Content12);

										}

										if(type_id.equals("3"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);



											ContentArrayListMp3.add(Content12);

										}



										if(type_id.equals("4"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);



											ContentArrayListWallpaper.add(Content12);

										}

										if(type_id.equals("5"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);



											ContentArrayListGame.add(Content12);
										}

										if(type_id.equals("6"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);



											ContentArrayListBook.add(Content12);

										}


									}
									//		                      InfoList.add(Info);
								}
								else
								{
									Log.e("ServiceHandler", "Couldn't get any data from the content");

								}



							}


						}

						Log.e("---------top_banner-------------", Top_banner_image);
						imageLoader.DisplayImage(Top_banner_image,BannerImage);

						adapterLatest=new AdapterLatestForMainScreen(MainActivity_Static.this, ContentArrayListLatest);
						adapter=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListVideos);
						adapter2=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListMp3);
						adapter3=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListWallpaper);
						adapter4=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListApp);
						adapter5=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListGame);
						adapter6=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListBook);


						WallpaperProjectsList.setAdapter(adapter3);
						VideoProjectsList.setAdapter(adapter);
						Mp3ProjectsList.setAdapter(adapter2);
						AppsProjectsList.setAdapter(adapter4);
						GameProjectsList.setAdapter(adapter5);
						BookProjectsList.setAdapter(adapter6);
						LatestProjectsList.setAdapter(adapterLatest);

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(MainActivity_Static.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			//			pDialog.dismiss();

			LinearLayout HomeScreen = (LinearLayout)findViewById(R.id.home_withScreen);
			//			LinearLayout HomeData = (LinearLayout)findViewById(R.id.home_withData);

			HomeScreen.setVisibility(View.GONE);
			//			HomeData.setVisibility(View.VISIBLE);
			//			new MainScreenShow12().execute();



		}

	}



	class MainScreenShow12 extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();



			/*pDialog = new ProgressDialog(MainActivity_Static.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();*/
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.MainScreen;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
				//				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Category"+'"'+":["+response12+"]}";
				Log.e("---------response-------------", response12);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Category"+'"'+":}";

					Log.e("---------respon-------------", respon);

					if (!response.equals(respon)) 
					{

						bgSc.setVisibility(View.VISIBLE);


						Log.e("---------response-------------", response);


						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);



							String latest_content12 = cate.getString("latest_content");

							String home_content12 = cate.getString("home_content");

							String TopBanner_content12 = cate.getString("top_banner");

							Log.e("---------latest_content12-------------", latest_content12);
							Log.e("---------home_content-------------", home_content12);
							Log.e("---------Top_content-------------", TopBanner_content12);


							String Latest_ContentResponse = "{"+'"'+"latest_content"+'"'+":["+latest_content12+"]}";
							String Home_ContentResponse = "{"+'"'+"home_content"+'"'+":"+home_content12+"}";
							String TopBanner_ContentResponse = "{"+'"'+"top_banner"+'"'+":"+TopBanner_content12+"}";

							Log.e("---------latest_contentResponse-------------", Latest_ContentResponse);
							Log.e("---------Home_ContentResponse-------------", Home_ContentResponse);
							Log.e("---------TopBanner_ContentResponse-------------", TopBanner_ContentResponse);


							JSONObject TopBanner_ContentJsonObj = new JSONObject(TopBanner_ContentResponse);

							JSONObject TOP_BC = TopBanner_ContentJsonObj.getJSONObject("top_banner");

							Top_banner_content_id = TOP_BC.getString("content_id");
							Top_banner_content_type_id = TOP_BC.getString("content_type_id");
							Top_banner_image = TOP_BC.getString("banner");


							Log.e("---------Top_banner_content_id-------------", Top_banner_content_id);
							Log.e("---------Top_banner_image-****&&&&&&&-------------", Top_banner_image);

							/*for (int j2 = 0; j2 < TopBanner_Content_List.length(); j2++)
							{
								JSONObject TOP_BC = TopBanner_Content_List.getJSONObject(j2);
								Top_banner_content_id = TOP_BC.getString("content_id");
								Top_banner_image = TOP_BC.getString("banner");
							}*/



							JSONObject Latest_ContentJsonObj = new JSONObject(Latest_ContentResponse);
							JSONArray Latest_Content_List = Latest_ContentJsonObj.getJSONArray("latest_content");

							// content node is JSON Object

							for (int j = 0; j < Latest_Content_List.length(); j++)
							{
								JSONObject LCL = Latest_Content_List.getJSONObject(j);
								String category_title = LCL.getString("category_title");
								LatestTv.setText(category_title);
								Log.e("---------category_title-------------", category_title);

								String content_for_latest = LCL.getString("content");
								Log.e("---------content_for_latest-------------", content_for_latest);

								String Latest_Content_Content_Response = "{"+'"'+"latest_content_content"+'"'+":"+content_for_latest+"}";
								Log.e("---------Latest_Content_Content_Response-------------", Latest_Content_Content_Response);

								JSONObject Latest_ContentContent_JsonObj = new JSONObject(Latest_Content_Content_Response);

								JSONArray Latest_ContentContent_List = Latest_ContentContent_JsonObj.getJSONArray("latest_content_content");

								if(Latest_ContentContent_List.length()!=0)
								{

									for (int k = 0; k < Latest_ContentContent_List.length(); k++)
									{
										JSONObject LCCL = Latest_ContentContent_List.getJSONObject(k);

										String latest_content_id = LCCL.getString("id");
										String type_id = LCCL.getString("type_id");
										String content_title = LCCL.getString("content_title");
										String preview = LCCL.getString("preview");
										String total_download = LCCL.getString("total_download");
										String payment_type = LCCL.getString("payment_type");
										String price = LCCL.getString("price");
										String rating = LCCL.getString("rating");


										HashMap<String, String> LatestContent = new HashMap<String, String>();

										LatestContent.put("latest_content_id", latest_content_id);
										LatestContent.put("type_id", type_id);
										LatestContent.put("content_title", content_title);
										LatestContent.put("preview", preview);
										LatestContent.put("total_download", total_download);
										LatestContent.put("payment_type", payment_type);
										LatestContent.put("price", price);
										LatestContent.put("rating", rating);

										ContentArrayListLatest.add(LatestContent);
									}
								}
								else
								{
									LatestLayout.setVisibility(View.GONE);
								}




							}


							JSONObject Home_ContentJsonObj = new JSONObject(Home_ContentResponse);
							JSONArray Home_Content_List = Home_ContentJsonObj.getJSONArray("home_content");

							for (int i1 = 0; i1 < Home_Content_List.length(); i1++)
							{

								JSONObject cateHome = Home_Content_List.getJSONObject(i1);

								String _id = cateHome.getString("type_id");
								String _type_name = cateHome.getString("type_name");
								String _short_description = cateHome.getString("short_description");


								//								appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv;

								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_id);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_type_name);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_short_description);


								String ContentResponse = cateHome.getString("content");

								if(_id.equals("1")&&ContentResponse.equals("[]"))
									AppsLayout.setVisibility(View.GONE);


								if(_id.equals("2")&&ContentResponse.equals("[]"))

									VideoLayout.setVisibility(View.GONE);


								if(_id.equals("3")&&ContentResponse.equals("[]"))
									Mp3Layout.setVisibility(View.GONE);



								if(_id.equals("4")&&ContentResponse.equals("[]"))
									WallpaperLayout.setVisibility(View.GONE);


								if(_id.equals("5")&&ContentResponse.equals("[]"))
									GameLayout.setVisibility(View.GONE);



								if(_id.equals("6")&&ContentResponse.equals("[]"))
									BookLayout.setVisibility(View.GONE);


								//								TextView AppSubTv, VideoSubTv, Mp3SubTv, GameSubTv, BookSubTv, WallapperSubTv;

								/*	if(_id.equals("1"))
								{
									appTv.setText(_type_name);
								 	AppSubTv.setText(_short_description);
								}

								if(_id.equals("2"))
								{
									videoTv.setText(_type_name);
									VideoSubTv.setText(_short_description);
								}

								if(_id.equals("3"))
								{
									mp3Tv.setText(_type_name);
									Mp3SubTv.setText(_short_description);
								}

								if(_id.equals("4"))
								{
									wallapperTv.setText(_type_name);
									WallapperSubTv.setText(_short_description);
								}

								if(_id.equals("5"))
								{
									gameTv.setText(_type_name);
									GameSubTv.setText(_short_description);
								}

								if(_id.equals("6"))
								{
									bookTv.setText(_type_name);
									BookSubTv.setText(_short_description);
								}*/




								if(!ContentResponse.equals("[]"))
								{

									HashMap<String, String> Info = new HashMap<String, String>();
									Info.put("id", _id);
									Info.put("type_name", _type_name);
									Info.put("short_description", _short_description);



									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_id);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_type_name);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_short_description);


									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";

									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);

									JSONArray Content_List = ContentJsonObj.getJSONArray("content");

									// content node is JSON Object

									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject c = Content_List.getJSONObject(j);

										String _content_id = c.getString("id");
										String _content_title = c.getString("content_title");
										//										String _content_description = c.getString("content_description");
										String _preview = c.getString("preview");
										//										String _banner = c.getString("banner");
										String _total_download = c.getString("total_download");
										String _payment_type = c.getString("payment_type");
										String _price = c.getString("price");
										String _rating = c.getString("rating");
										/*	String _company_name = c.getString("company_name");
										String _developer_name = c.getString("developer_name");
										String _category_name = c.getString("category_name");*/

										HashMap<String, String> Content12 = new HashMap<String, String>();


										String type_name = Info.get("type_name");
										String type_id = Info.get("id");
										Log.e("AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC", "AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC"+type_name);

										if(type_id.equals("1"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/



											ContentArrayListApp.add(Content12);

										}


										if(type_id.equals("2"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/



											ContentArrayListVideos.add(Content12);

										}

										if(type_id.equals("3"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/



											ContentArrayListMp3.add(Content12);

										}



										if(type_id.equals("4"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/


											ContentArrayListWallpaper.add(Content12);

										}

										if(type_id.equals("5"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/


											ContentArrayListGame.add(Content12);
										}

										if(type_id.equals("6"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("type_name", type_name);
											Content12.put("content_title", _content_title);
											//											Content12.put("content_description", _content_description);
											Content12.put("preview", _preview);
											//											Content12.put("banner", _banner);
											Content12.put("total_download", _total_download);
											Content12.put("payment_type", _payment_type);
											Content12.put("price", _price);
											Content12.put("rating", _rating);
											/*Content12.put("company_name", _company_name);
											Content12.put("developer_name", _developer_name);
											Content12.put("category_name", _category_name);*/


											ContentArrayListBook.add(Content12);

										}


									}
									//		                      InfoList.add(Info);
								}
								else
								{
									Log.e("ServiceHandler", "Couldn't get any data from the content");



								}



							}


						}

						Log.e("---------top_banner-------------", Top_banner_image);
						imageLoader.DisplayImage(Top_banner_image,BannerImage);

						adapterLatest=new AdapterLatestForMainScreen(MainActivity_Static.this, ContentArrayListLatest);
						adapter=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListVideos);
						adapter2=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListMp3);
						adapter3=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListWallpaper);
						adapter4=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListApp);
						adapter5=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListGame);
						adapter6=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListBook);


						WallpaperProjectsList.setAdapter(adapter3);
						VideoProjectsList.setAdapter(adapter);
						Mp3ProjectsList.setAdapter(adapter2);
						AppsProjectsList.setAdapter(adapter4);
						GameProjectsList.setAdapter(adapter5);
						BookProjectsList.setAdapter(adapter6);
						LatestProjectsList.setAdapter(adapterLatest);

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(MainActivity_Static.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			//			pDialog.dismiss();



		}

	}



	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.item_Video:
			Toast.makeText(this, "Video Clicked", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.item_Audio:
			Toast.makeText(this, "Audio Clicked", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.item_Apps:
			Toast.makeText(this, "Apps Clicked", Toast.LENGTH_SHORT).show();
			return true;		

		case R.id.item_Game:
			Toast.makeText(this, "Game Clicked", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.item_Wallpaper:
			Toast.makeText(this, "Wallpaper Clicked", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.item_Books:
			Toast.makeText(this, "Books Clicked", Toast.LENGTH_SHORT).show();
			return true;		
		}
		return false;
	}


	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}

	/*	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent idd = new Intent(Retailer_Update_GPRS_Activity.this, Retailer_Menu_Activation.class);
			startActivity(idd);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
	 */

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					MainActivity_Static.this);

			// set title
			//				alertDialogBuilder.setTitle("Exit");

			// set dialog message
			alertDialogBuilder
			.setMessage("Do you want to Exit it?")
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity
					//							MainActivity.this.finish();

					finish();

				}
			})
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}



	class SearchAutoCompleteTextViewList extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			SearchAutoComplete_ID.clear();
			SearchAutoComplete_Name.clear();

			/*AsmID.clear();
			AsmName.clear();
			AsmDesg.clear();
			AsmID.add("00");
			AsmName.add("Select Asm");
			AsmDesg.add("00");*/
			/*pDialog = new ProgressDialog(DoctorVisitAddActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();*/
		}

		@Override
		protected String doInBackground(String... args) {


			try {
				String url=URLs.Search_Keyword;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
				//				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Category"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response12);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {


						parseXmlresponseForProductCatagory(response);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String MyResponse) throws JSONException 
				{


					if (response != null) 
					{

						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);

							String _id = cate.getString("id");
							String _keyword_name = cate.getString("keyword");

							SearchAutoComplete_ID.add(_id);
							SearchAutoComplete_Name.add(_keyword_name);

							Log.e("%%%%%%%%%%%%%%%%%%%%", "%%%%%%%%%%%%%%%%%%%%"+_id);
							Log.e("%%%%%%%%%%%%%%%%%%%%", "%%%%%%%%%%%%%%%%%%%%"+_keyword_name);

							/*JSONObject jsonObj = new JSONObject(response);

						// Getting JSON Array node
						TourUser = jsonObj.getJSONArray("user");

						// looping through All Contacts
						for (int i = 0; i < TourUser.length(); i++) {
							JSONObject c = TourUser.getJSONObject(i);

							String id = c.getString("id");
							String name = c.getString("name");
							String designation = c.getString("designation_id");

							// tmp hashmap for single contact
							HashMap<String, String> contact = new HashMap<String, String>();

							// adding each child node to HashMap key => value
							contact.put("pme_id", id);
							contact.put("name", name);
							contact.put("desig",designation);

							// adding contact to contact list
							AsmID.add(id);
							AsmName.add(name);
							AsmDesg.add(designation);
							//Log.e("%%%%%%%%%%%%%%%%%%%%", "%%%%%%%%%%%%%%%%%%%%"+RouteName);

						}*/
						}

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity_Static.this,android.R.layout.simple_spinner_item, SearchAutoComplete_Name);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					SearchAutoCompleteTextView.setAdapter(dataAdapter);
					//pDialog.dismiss();



				}


			});
			/*if(pDialog.isShowing())
			pDialog.dismiss();*/

			SearchAutoCompleteTextView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SearchAutoCompleteTextView.showDropDown();
				}
			});
			SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {

					TempData.SEARCH_ID=SearchAutoComplete_ID.get(arg2);
					Log.e("IDDDDD", String.valueOf(SearchAutoComplete_ID.get(arg2)));
					Intent in=new Intent(MainActivity_Static.this,SearchDetails.class);
					startActivity(in);


				}
			});
		}
	}




	class TrackingAsyn extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			GPSTracker gps = new GPSTracker(getApplicationContext());
			if (gps.canGetLocation()) {

				lat = String.valueOf(gps.getLatitude());
				lng = String.valueOf(gps.getLongitude());

				Log.e("LLLLLLL", lat);
			}


			/*pDialog = new ProgressDialog(MainActivity_Static.this);
			pDialog.setMessage("Please wait...");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();*/
		}

		@Override
		protected String doInBackground(String... args) {
			String final_json = "";

			String url=URLs.Tracking;
			JSONObject jsonObject12 = new JSONObject();

			String lat_long = lat+","+lng;

			try {

				jsonObject12.accumulate("imei_no", IMEI_No);
				jsonObject12.accumulate("location", lat_long);



			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String	my_final_json = jsonObject12.toString().substring(1, jsonObject12.toString().length()-1)+",";
			String my_final_json1 = my_final_json.substring(0, my_final_json.length()-1);
			final_json = "{"+my_final_json1+"}";
			Log.e("Subroto","json :"+final_json);


			JSONResponse_Type = JsonParse.makeServiceCall(url,final_json);
			Log.e("Subroto","response Tour User: "+JSONResponse_Type);

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {


						parseXmlresponseForProductCatagory(JSONResponse_Type);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String response) throws JSONException 
				{


					if (response != null) 
					{

						JSONObject jsonObj = new JSONObject(response);

						// Getting JSON Array node
						JSONArray TypArray = jsonObj.getJSONArray("result");

						// looping through All Contacts
						for (int i = 0; i < TypArray.length(); i++) {
							JSONObject c = TypArray.getJSONObject(i);

							String status = c.getString("status");
							String message = c.getString("message");



							Log.e("%%%%%%%%%%%%%%%%%%%%", "%%%%%%%%%%%%%%%%%%%%"+message);

						}


					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}



				}


			});

			//			pDialog.dismiss();
		}
	}

}
