package com.arena.bangla.store;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import com.arena.bangla.store.R;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoPreviewActivity extends Activity {
//	public static String url = "rtsp://v3.cache8.c.youtube.com/CiILENy73wIaGQmXovF6e-Rf-BMYDSANFEgGUgZ2aWRlb3MM/0/0/0/video.3gp";
//	public static String url = "http://www.arena.com.bd/song_xml/song/Alarm_The_Ring_320x240.3gp";
	private VideoView videoView = null;
	private ProgressBar prog = null;
	private Context ctx = null;
	private MediaController mediaController = null;
	String Content_Id12="",Which_screen12="";
	private Handler myHandler = new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setContentView(R.layout.video_preview);
		
		Bundle b = getIntent().getExtras();
		String ourVideo = b.getString("content_file");
		Content_Id12 = b.getString("Content_Id");
		Which_screen12 = b.getString("which_screen");
		
		Log.d("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV", "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD"+ourVideo);
		
		ctx = this;
		prog = (ProgressBar) findViewById(R.id.prog);
		videoView = (VideoView) findViewById(R.id.video);
		Uri video = Uri.parse(ourVideo);
		mediaController = new MediaController(this);
		mediaController.setAnchorView(videoView);
		videoView.setMediaController(mediaController);
		videoView.setVideoURI(video);

		videoView.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				Toast.makeText(ctx, "Error occured", 500).show();
				return false;
			}
		});

		videoView.setOnPreparedListener(new OnPreparedListener() {

			public void onPrepared(MediaPlayer arg0) {
				prog.setVisibility(View.GONE);
				videoView.start();
				
//				  myHandler.postDelayed(StopSongTime,50 * 1000);
				  
			}
		});
	}

	@Override
	protected void onDestroy() {
		try {
			videoView.stopPlayback();
//			videoView.
		} catch (Exception e) {
			//
		}
		super.onDestroy();
	}
	
	   private Runnable StopSongTime = new Runnable() {
		   public void run() {
			  /* finalTime = mediaPlayer.getCurrentPosition();
			   seekbar.setProgress((int)finalTime);*/
//			   mediaPlayer.pause();
			   videoView.stopPlayback();
//			   mediaPlayer.release();
			   Toast.makeText(VideoPreviewActivity.this, "Please download the full song", Toast.LENGTH_LONG).show();
		   }
	   };
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			
			/*Intent idd = new Intent(VideoPreviewActivity.this, VideoDetailsActivity.class);
			idd.putExtra("Content_Id", Content_Id12);
			idd.putExtra("which_screen", Which_screen12);
			startActivity(idd);*/
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
}
