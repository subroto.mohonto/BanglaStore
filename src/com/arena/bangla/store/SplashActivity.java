package com.arena.bangla.store;

import com.arena.bangla.store.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
 


public class SplashActivity extends Activity {
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	String PINCODE_="";
	
	
     public void onAttachedToWindow() {
            super.onAttachedToWindow();
            Window window = getWindow();
            window.setFormat(PixelFormat.RGBA_8888);
        }
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
        
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        PINCODE_ = sharedpreferences.getString("PINCODE", "");
        

        
        if(sharedpreferences.getString("Language", "").equals(""))
		{
			SharedPreferences.Editor editor2 = sharedpreferences.edit();
			editor2.putString("Language", "?lang=bn");
//			editor2.putString("Language", "");
			editor2.commit();
		}
       
        
        StartAnimations();
                
    }
    
    Thread splashTread;
    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);
 
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.logo);
        iv.clearAnimation();
        iv.startAnimation(anim);
        
     
        
        Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE"+PINCODE_);
        
        if(PINCODE_.equals(""))
        {
        	 splashTread = new Thread() {
                 @Override
                 public void run() {
                     try {
                         int waited = 0;
                         // Splash screen pause time
                         while (waited < 3500) {
                             sleep(100);
                             waited += 100;
                         }
                         Intent intent = new Intent(SplashActivity.this,
                        		 NewNumberLoginScreenActivity.class);
                         intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                         startActivity(intent);
                         SplashActivity.this.finish();
                     } catch (InterruptedException e) {
                         // do nothing
                     } finally {
                     	SplashActivity.this.finish();
                     }

                 }
             };
             splashTread.start();
        }
        
        else
        {
        	String Screen = sharedpreferences.getString("HomeScreen", null);
        	
        	if(Screen.equals("MainActivity_Static"))
        	{
        		 splashTread = new Thread() {
                     @Override
                     public void run() {
                         try {
                             int waited = 0;
                             // Splash screen pause time
                             while (waited < 3500) {
                                 sleep(100);
                                 waited += 100;
                             }
                             Intent intent = new Intent(SplashActivity.this,
                                     MainActivity_Static.class);
                             intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                             startActivity(intent);
                             SplashActivity.this.finish();
                         } catch (InterruptedException e) {
                             // do nothing
                         } finally {
                         	SplashActivity.this.finish();
                         }

                     }
                 };
                 splashTread.start();
        	}
        	else
        	{
        		splashTread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;
                            // Splash screen pause time
                            while (waited < 3500) {
                                sleep(100);
                                waited += 100;
                            }
                            Intent intent = new Intent(SplashActivity.this,
                                    TemplateHomeScreenActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            SplashActivity.this.finish();
                        } catch (InterruptedException e) {
                            // do nothing
                        } finally {
                        	SplashActivity.this.finish();
                        }

                    }
                };
                splashTread.start();
        	}
        	
        	
        }
        
       

 
    }
 
}