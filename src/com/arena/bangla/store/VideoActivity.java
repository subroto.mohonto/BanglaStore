package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.LazyAdapter;
import com.arena.bangla.store.R;
import com.arena.json.JsonParse;
import com.arena.json.URLs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class VideoActivity extends Activity{
	
	ProgressDialog pDialog;
	String response;
	String response12;
//	LazyAdapter adapter;
	LazyAdapter adapter;
	
	ArrayList<HashMap<String, String>> ContentArrayList;
	GridView gv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.apps_screen);
		ViewInitializationAndAction();
		gv = (GridView)findViewById(R.id.gridView14);
		ContentArrayList = new ArrayList<HashMap<String, String>>();
		
		new VideoScreenShow().execute();
		
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(VideoActivity.this, VideoDetailsActivity.class);
				startActivity(idn);
				
				
			}
		});
		
	}
	
	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(VideoActivity.this, AppsActivity.class);
				startActivity(idn);
				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(VideoActivity.this, Video12Activity.class);
				startActivity(idn);
				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(VideoActivity.this, AudioActivity.class);
				startActivity(idn);
				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(VideoActivity.this, WallpaperActivity.class);
				startActivity(idn);
				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(VideoActivity.this, GameActivity.class);
				startActivity(idn);
				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(VideoActivity.this, BooksActivity.class);
				startActivity(idn);
				finish();
			}
		});


	}


	class VideoScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(VideoActivity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.Videos;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Contents"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Contents"+'"'+":}";
					Log.e("---------respon-------------", respon);
					
					if (!response.equals(respon)) 
					{
						
						
						
						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Contents");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject con = ContentList.getJSONObject(i);

							
							/*id": "10",
					        "content_title": "Depika's New Video",
					        "preview": "http:\/\/192.168.1.20\/apponit\/uploaded_content\/videos\/preview\/20160525144140.png",
					        "total_download": "0",
					        "payment_type": "Paid",
					        "price": "10.00",
					        "rating": 4*/
					        
							String _content_id = con.getString("id");
							String _content_title = con.getString("content_title");
							String _preview = con.getString("preview");
							String _total_download = con.getString("total_download");
							String _payment_type = con.getString("payment_type");
							String _price = con.getString("price");
							String _rating = con.getString("rating");
							

							HashMap<String, String> Content12 = new HashMap<String, String>();
							
							
							Content12.put("content_id", _content_id);
							Content12.put("content_title", _content_title);
							Content12.put("preview", _preview);
						    Content12.put("total_download", _total_download);
							Content12.put("payment_type", _payment_type);
							Content12.put("price", _price);
							Content12.put("rating", _rating);
							
							ContentArrayList.add(Content12);
							
						}
						adapter = new LazyAdapter(VideoActivity.this, ContentArrayList);	
						gv.setAdapter(adapter);
							

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(VideoActivity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			pDialog.dismiss();



		}

	}
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}

}
