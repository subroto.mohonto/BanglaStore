package com.arena.bangla.store;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.AdapterLatestForMainScreen;
import com.arena.image.loader.ImageLoader;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class TemplateHomeScreen_2_Activity extends Activity{
	
	ProgressDialog pDialog;
	String response;
	String response12;
	
	ImageView BannerImage;
	String Top_banner_image,Top_banner_content_id="",Top_banner_content_type_id="";
	LinearLayout im1,im2,im3,im4,im5,im6,im8;
	ImageView imm1,imm2,imm3,imm4,imm5,imm6,imm8;
	
	public ImageLoader imageLoader; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_main2);
		
		imageLoader=new ImageLoader(TemplateHomeScreen_2_Activity.this);
		
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels/3;
		int height=dm.heightPixels/4;
		
		int width2=dm.widthPixels/3;
		int height2=dm.heightPixels/4;
		
		int width3 = width+width2;
		
		int height3 = dm.heightPixels/2;
		
		Log.e("****************", "(------width-------)"+width+"(------height-------)"+height);
		
		
		int width22=dm.widthPixels/2;
		
		 im1 = (LinearLayout)findViewById(R.id.image1);
		 im2 = (LinearLayout)findViewById(R.id.image2);
		 im3 = (LinearLayout)findViewById(R.id.image3);
		 im4 = (LinearLayout)findViewById(R.id.image4);
		 im5 = (LinearLayout)findViewById(R.id.image5);
		 im6 = (LinearLayout)findViewById(R.id.image6);
		 im8 = (LinearLayout)findViewById(R.id.image8);
		 
		 
		 imm1 = (ImageView)findViewById(R.id.im1);
		 imm2 = (ImageView)findViewById(R.id.im2);
		 imm3 = (ImageView)findViewById(R.id.im3);
		 imm4 = (ImageView)findViewById(R.id.im4);
		 imm5 = (ImageView)findViewById(R.id.im5);
		 imm6 = (ImageView)findViewById(R.id.im6);
		 imm8 = (ImageView)findViewById(R.id.im8);
		
		 
		 
		/*LinearLayout im9 = (LinearLayout)findViewById(R.id.image9);
		LinearLayout im10 = (LinearLayout)findViewById(R.id.image10);*/
		
		im1.getLayoutParams().height = width;
		im1.getLayoutParams().width = width;
		
		im2.getLayoutParams().height = width;
		im2.getLayoutParams().width = width;
		
		im3.getLayoutParams().height = width;
		im3.getLayoutParams().width = width3;
		
		im4.getLayoutParams().height = width3;
		im4.getLayoutParams().width = width;
		
		im5.getLayoutParams().height = width;
		im5.getLayoutParams().width = width22;
		
		im6.getLayoutParams().height = width;
		im6.getLayoutParams().width = width22;
		
		
		/*im7.getLayoutParams().height = width;
		im7.getLayoutParams().width = width22;*/
		
		im8.getLayoutParams().height = width;
		im8.getLayoutParams().width = dm.widthPixels;
		
		
		new MainScreenShow().execute();
		
		
		
	}
	
	
	
	class MainScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();



			pDialog = new ProgressDialog(TemplateHomeScreen_2_Activity.this);
			pDialog.setMessage(" Please wait.....");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.MainScreen;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
				//				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Category"+'"'+":["+response12+"]}";
//				Log.e("---------response-------------", response12);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Category"+'"'+":}";

//					Log.e("---------respon-------------", respon);

					if (!response.equals(respon)) 
					{

//						bgSc.setVisibility(View.VISIBLE);


//						Log.e("---------response-------------", response);


						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);



							String latest_content12 = cate.getString("latest_content");

							String home_content12 = cate.getString("home_content");

							String TopBanner_content12 = cate.getString("top_banner");

							/*Log.e("---------latest_content12-------------", latest_content12);
							Log.e("---------home_content-------------", home_content12);
							Log.e("---------Top_content-------------", TopBanner_content12);*/


							String Latest_ContentResponse = "{"+'"'+"latest_content"+'"'+":["+latest_content12+"]}";
							String Home_ContentResponse = "{"+'"'+"home_content"+'"'+":"+home_content12+"}";
							String TopBanner_ContentResponse = "{"+'"'+"top_banner"+'"'+":"+TopBanner_content12+"}";

							/*Log.e("---------latest_contentResponse-------------", Latest_ContentResponse);
							Log.e("---------Home_ContentResponse-------------", Home_ContentResponse);
							Log.e("---------TopBanner_ContentResponse-------------", TopBanner_ContentResponse);*/


							JSONObject TopBanner_ContentJsonObj = new JSONObject(TopBanner_ContentResponse);

							JSONObject TOP_BC = TopBanner_ContentJsonObj.getJSONObject("top_banner");

							Top_banner_content_id = TOP_BC.getString("content_id");
							Top_banner_content_type_id = TOP_BC.getString("content_type_id");
							Top_banner_image = TOP_BC.getString("banner");


							Log.e("---------Top_banner_content_id-------------", Top_banner_content_id);
							Log.e("---------Top_banner_image-****&&&&&&&-------------", Top_banner_image);
							
							imageLoader.DisplayImage(Top_banner_image,imm8);

							/*for (int j2 = 0; j2 < TopBanner_Content_List.length(); j2++)
							{
								JSONObject TOP_BC = TopBanner_Content_List.getJSONObject(j2);
								Top_banner_content_id = TOP_BC.getString("content_id");
								Top_banner_image = TOP_BC.getString("banner");
							}*/



							JSONObject Latest_ContentJsonObj = new JSONObject(Latest_ContentResponse);
							JSONArray Latest_Content_List = Latest_ContentJsonObj.getJSONArray("latest_content");

							// content node is JSON Object

							for (int j = 0; j < Latest_Content_List.length(); j++)
							{
								JSONObject LCL = Latest_Content_List.getJSONObject(j);
								String category_title = LCL.getString("category_title");
								
								Log.e("---------category_title-------------", category_title);

								String content_for_latest = LCL.getString("content");
								Log.e("---------content_for_latest-------------", content_for_latest);

								String Latest_Content_Content_Response = "{"+'"'+"latest_content_content"+'"'+":"+content_for_latest+"}";
								Log.e("---------Latest_Content_Content_Response-------------", Latest_Content_Content_Response);

								JSONObject Latest_ContentContent_JsonObj = new JSONObject(Latest_Content_Content_Response);

								JSONArray Latest_ContentContent_List = Latest_ContentContent_JsonObj.getJSONArray("latest_content_content");

								if(Latest_ContentContent_List.length()!=0)
								{

									for (int k = 0; k < Latest_ContentContent_List.length(); k++)
									{
										JSONObject LCCL = Latest_ContentContent_List.getJSONObject(k);

										String latest_content_id = LCCL.getString("id");
										String type_id = LCCL.getString("type_id");
										String content_title = LCCL.getString("content_title");
										String preview = LCCL.getString("preview");
										String total_download = LCCL.getString("total_download");
										String payment_type = LCCL.getString("payment_type");
										String price = LCCL.getString("price");
										String rating = LCCL.getString("rating");


										HashMap<String, String> LatestContent = new HashMap<String, String>();

										LatestContent.put("latest_content_id", latest_content_id);
										LatestContent.put("type_id", type_id);
										LatestContent.put("content_title", content_title);
										LatestContent.put("preview", preview);
										LatestContent.put("total_download", total_download);
										LatestContent.put("payment_type", payment_type);
										LatestContent.put("price", price);
										LatestContent.put("rating", rating);

//										ContentArrayListLatest.add(LatestContent);
									}
								}
								/*else
								{
									LatestLayout.setVisibility(View.GONE);
								}*/




							}


							JSONObject Home_ContentJsonObj = new JSONObject(Home_ContentResponse);
							JSONArray Home_Content_List = Home_ContentJsonObj.getJSONArray("home_content");

							for (int i1 = 0; i1 < Home_Content_List.length(); i1++)
							{

								JSONObject cateHome = Home_Content_List.getJSONObject(i1);

								String _id = cateHome.getString("type_id");
								String _type_name = cateHome.getString("type_name");
								String _short_description = cateHome.getString("short_description");


								//								appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv;

								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_id);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_type_name);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_short_description);


								String ContentResponse = cateHome.getString("content");

							
								if(!ContentResponse.equals("[]"))
								{

									HashMap<String, String> Info = new HashMap<String, String>();
									Info.put("id", _id);
									Info.put("type_name", _type_name);
									Info.put("short_description", _short_description);



									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_id);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_type_name);
									Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_short_description);


									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";

									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);

									JSONArray Content_List = ContentJsonObj.getJSONArray("content");

									// content node is JSON Object

									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject c = Content_List.getJSONObject(j);

										String _content_id = c.getString("id");
										String _content_title = c.getString("content_title");
										String _preview = c.getString("preview");
										String _rating = c.getString("rating");


										HashMap<String, String> Content12 = new HashMap<String, String>();


										String type_name = Info.get("type_name");
										String type_id = Info.get("id");
										Log.e("AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC", "AAAAAAAAAAAAABBBBBBBBBBBCCCCCCCCC"+type_name);

										if(type_id.equals("1"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);

											imageLoader.DisplayImage(_preview,imm1);


//											ContentArrayListApp.add(Content12);

										}


										if(type_id.equals("2"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);

											imageLoader.DisplayImage(_preview,imm2);

//											ContentArrayListVideos.add(Content12);

										}

										if(type_id.equals("3"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);
											imageLoader.DisplayImage(_preview,imm3);


//											ContentArrayListMp3.add(Content12);

										}



										if(type_id.equals("4"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);

											imageLoader.DisplayImage(_preview,imm4);

//											ContentArrayListWallpaper.add(Content12);

										}

										if(type_id.equals("5"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);

											imageLoader.DisplayImage(_preview,imm5);

//											ContentArrayListGame.add(Content12);
										}

										if(type_id.equals("6"))
										{
											Content12.put("content_id", _content_id);
											Content12.put("content_title", _content_title);
											Content12.put("preview", _preview);
											Content12.put("rating", _rating);
											imageLoader.DisplayImage(_preview,imm6);


//											ContentArrayListBook.add(Content12);

										}


									}
									//		                      InfoList.add(Info);
								}
								else
								{
									Log.e("ServiceHandler", "Couldn't get any data from the content");

								}



							}


						}

					/*	Log.e("---------top_banner-------------", Top_banner_image);
						imageLoader.DisplayImage(Top_banner_image,BannerImage);

						adapterLatest=new AdapterLatestForMainScreen(MainActivity_Static.this, ContentArrayListLatest);
						adapter=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListVideos);
						adapter2=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListMp3);
						adapter3=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListWallpaper);
						adapter4=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListApp);
						adapter5=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListGame);
						adapter6=new AdapterForMainScreen(MainActivity_Static.this, ContentArrayListBook);


						WallpaperProjectsList.setAdapter(adapter3);
						VideoProjectsList.setAdapter(adapter);
						Mp3ProjectsList.setAdapter(adapter2);
						AppsProjectsList.setAdapter(adapter4);
						GameProjectsList.setAdapter(adapter5);
						BookProjectsList.setAdapter(adapter6);
						LatestProjectsList.setAdapter(adapterLatest);
*/
					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(TemplateHomeScreen_2_Activity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
						pDialog.dismiss();

			


		}

	}


}
