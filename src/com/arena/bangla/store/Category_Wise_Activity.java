package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.CustomAdapter;
import com.arena.adapter.CustomAdapterTemplateHomeScreen;
import com.arena.adapter.CustomAdapterTemplateHomeScreen_WithInnerScreen;
import com.arena.bangla.store.MainActivity_Static.SearchAutoCompleteTextViewList;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import com.devsmart.android.ui.HorizontalListView;



import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Category_Wise_Activity extends ParentActivity {

		String Language ="";
		public static final String MyPREFERENCES = "MyPrefs" ;
		SharedPreferences sharedpreferences;
	    ListView lv;
	    Context context;

	    ProgressDialog pDialog;
	    String ParentType="abc";
		String response;
		  ArrayList<ArrayList< HashMap<String, String>>> collection=new ArrayList<ArrayList< HashMap<String, String>>>();
		  String SubLink_, TitleName;
		  TextView AppBtn1, VideoBtn1, AudioBtn1, GameBtn1, WallpaperBtn1,BookBtn1,LatestNewsBtn1;
		  
		  ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
	      ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();
	      String response12;
	      AutoCompleteTextView SearchAutoCompleteTextView;
	        
		@Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.category_wise);
	        
	        Bundle b = getIntent().getExtras();
	        SubLink_ = b.getString("SubLink");
	        TitleName = b.getString("TitleName");
	        
	        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

	        Language = sharedpreferences.getString("Language", "");
			if(Language.equals("English"))
			{
				Language="";		
			}
	        
	        SearchMenuOption(TitleName);
	      /*  ViewInitializationAndActionofMenu(SubLink_);
	        ViewInitializationAndAction();*/
	        
	        ViewMenu(R.id.App_RelativeLayout,R.id.Video_RelativeLayout,R.id.MP3_RelativeLayout,R.id.Wallpaper_RelativeLayout,R.id.Game_RelativeLayout,R.id.Book_RelativeLayout,R.id.LatestNews_RelativeLayout,SubLink_);
	        
	        context=this;
	        lv=(ListView) findViewById(R.id.mainsListview);
	        
	        SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
			SearchAutoCompleteTextView.setThreshold(1);
			
			try {
				 new MP3Json().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
	      

	    }

		class MP3Json extends AsyncTask<String, String, String> {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				pDialog = new ProgressDialog(Category_Wise_Activity.this);
				pDialog.setMessage("");
				pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
			}

			@Override
			protected String doInBackground(String... args) {

				try {
					String url=URLs.DOMAIN2+SubLink_+Language;
					Log.e("---------url-------------", url);
					response = JsonParse.makeServiceCall(url);
					response = "{"+'"'+"Category"+'"'+":"+response+"}";
					Log.e("---------response-------------", response);

				} catch (Exception e) {
				}

				return null;
			}

			@Override
			protected void onPostExecute(String file_url) {

				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						try {
							parseXmlresponseForItemCatagory(response);

						} catch (Exception e) { }



					}

					private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
					{
						String respon = "{"+'"'+"Category"+'"'+":}";
						if (!response.equals(respon)) 
						{
							JSONObject jsonObj = new JSONObject(MyResponse);
							JSONArray ContentList = jsonObj.getJSONArray("Category");
							
							for (int i = 0; i < ContentList.length(); i++)
							{
								
								JSONObject cateHome = ContentList.getJSONObject(i);
								String _id = cateHome.getString("id");
								String _type_name = cateHome.getString("category_title");
								String _short_description = cateHome.getString("short_description");
								
								
								String ContentResponse = cateHome.getString("content");
								if(!ContentResponse.equals("[]"))
								{
									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";
									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);
									JSONArray Content_List = ContentJsonObj.getJSONArray("content");
								    ArrayList< HashMap<String, String>> catagoryList= new ArrayList< HashMap<String, String>>();
									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject con= Content_List.getJSONObject(j);
										String _content_id = con.getString("id");
										String _content_title = con.getString("content_title");
										String _preview = con.getString("preview");
										String _total_download = con.getString("total_download");
										String _payment_type = con.getString("payment_type");
										String _price = con.getString("price");
										String _rating = con.getString("rating");
										

										HashMap<String, String> map = new HashMap<String, String>();
										map.put("ParentID",_id);
										map.put("ParentType", _type_name);
										map.put("ParentShortDescription", _short_description);
										
										map.put("content_id", _content_id);
										map.put("content_title", _content_title);
										map.put("preview", _preview);
									    map.put("total_download", _total_download);
										map.put("payment_type", _payment_type);
										map.put("price", _price);
										map.put("rating", _rating);
										map.put("SubLink", SubLink_);
										catagoryList.add(map);
										
										
										if(!_type_name.equalsIgnoreCase(ParentType))
										{
										collection.add(catagoryList);
										ParentType=_type_name;
										}

									}
									

								}
								
								
							}
						}
						else 
						{
						}

					}

				});
				
				lv.setAdapter(new CustomAdapter(Category_Wise_Activity.this, collection));
				pDialog.dismiss();
				



			}

		}
		
		
		private void SearchMenuOption(String Name) {
			
			Button SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
			ImageView SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);		
			TextView NameBack = (TextView)findViewById(R.id.TitleText);	
			
			final RelativeLayout CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
			final RelativeLayout SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);
			
			NameBack.setText(Name);
			
			SearchingCategoryBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
									
					CategoryLayout.setVisibility(View.GONE);
					SearchingLayout.setVisibility(View.VISIBLE);
					new SearchAutoCompleteTextViewList().execute();
					
				}
			});
			
			SearchingBackBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
										
					CategoryLayout.setVisibility(View.VISIBLE);
					SearchingLayout.setVisibility(View.GONE);
					
					
				}
			});
			
			NameBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					finish();
				}
			});
			
		}
		/*private void ViewInitializationAndActionofMenu(String ID) {
			
			TextView AppBtn1 = (TextView)findViewById(R.id.AppBtn1);
			TextView VideoBtn1 = (TextView)findViewById(R.id.VideoBtn1);
			TextView AudioBtn1 = (TextView)findViewById(R.id.AudioBtn2);
			TextView WallpaperBtn1 = (TextView)findViewById(R.id.WallpaperBtn2);
			TextView GameBtn1 = (TextView)findViewById(R.id.GameBtn2);
			TextView BookBtn1 = (TextView)findViewById(R.id.BookBtn2);
			TextView LatestNewsBtn1 = (TextView)findViewById(R.id.LatestNewsBtn2);
			
			RelativeLayout App_RelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
			RelativeLayout Video_RelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
			RelativeLayout MP3_RelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
			RelativeLayout Game_RelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
			RelativeLayout Wallpaper_RelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
			RelativeLayout Book_RelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
			RelativeLayout LatestNews_RelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
			
			if(ID.equals("1"))
			{
				AppBtn1.setTextColor(color.ActionBar_Orange);
				App_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
			}			
			else if(ID.equals("2"))
			{
				VideoBtn1.setTextColor(color.ActionBar_Orange);
				Video_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
			}
			else if(ID.equals("3"))
			{
				AudioBtn1.setTextColor(color.ActionBar_Orange);
				MP3_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
			}
			else if(ID.equals("4"))
			{
				WallpaperBtn1.setTextColor(color.ActionBar_Orange);
				Wallpaper_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
			}
			else if(ID.equals("5"))
			{
				GameBtn1.setTextColor(color.ActionBar_Orange);
				Game_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
			}
			else if(ID.equals("6"))
			{
				BookBtn1.setTextColor(color.ActionBar_Orange);
				Book_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
			}
			else if(ID.equals("1000"))
			{
				LatestNewsBtn1.setTextColor(color.ActionBar_Orange);
				LatestNews_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
			}
			
			
			
		}*/
		
		private void ViewInitializationAndAction() {
			// TODO Auto-generated method stub
			RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
			RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
			RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
			RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
			RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
			RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
			RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
			
			
			AppRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "1");
					idn.putExtra("TitleName", "অ�?যাপস");
					startActivity(idn);
//					finish();
				}
			});
			
			
			VideoRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "2");
					idn.putExtra("TitleName", "ভিডিও");
					startActivity(idn);
//					finish();
				}
			});
			AudioRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "3");
					idn.putExtra("TitleName", "�?মপিথ�?রি");
					startActivity(idn);
//					finish();
				}
			});
			WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "4");
					idn.putExtra("TitleName", "ওয়ালপেপার");
					startActivity(idn);
//					finish();
				}
			});
			GameRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "5");
					idn.putExtra("TitleName", "গেইম");
					startActivity(idn);
//					finish();
				}
			});
			BooksRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "6");
					idn.putExtra("TitleName", "বই");
					startActivity(idn);
//					finish();
				}
			});
			
			LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
					idn.putExtra("SubLink", "1000");
					idn.putExtra("TitleName", "সর�?বশেষ সংবাদ");
					startActivity(idn);
//					finish();
				}
			});



		}

		
		
		class SearchAutoCompleteTextViewList extends AsyncTask<String, String, String> {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				SearchAutoComplete_ID.clear();
				SearchAutoComplete_Name.clear();
				
				
			}

			@Override
			protected String doInBackground(String... args) {
				

				try {
					String url=URLs.Search_Keyword;
					//				String response12 = JsonParse.makeServiceCall(url);
					response12 = JsonParse.makeServiceCall(url);

					response = "{"+'"'+"Category"+'"'+":"+response12+"}";
					Log.e("---------response-------------", response12);

				} catch (Exception e) {
				}

				return null;
			}

			@Override
			protected void onPostExecute(String file_url) {
				runOnUiThread(new Runnable() {
					@SuppressLint("SetJavaScriptEnabled")
					@Override
					public void run() {

						try {


							parseXmlresponseForProductCatagory(response);

						} catch (Exception e) { }

					}

					private void parseXmlresponseForProductCatagory(String MyResponse) throws JSONException 
					{


						if (response != null) 
						{
							
							JSONObject jsonObj = new JSONObject(MyResponse);

							// Getting JSON Array node
							JSONArray ContentList = jsonObj.getJSONArray("Category");


							// looping through All Contacts
							for (int i = 0; i < ContentList.length(); i++)
							{

								JSONObject cate = ContentList.getJSONObject(i);

								String _id = cate.getString("id");
								String _keyword_name = cate.getString("keyword");
								
								SearchAutoComplete_ID.add(_id);
								SearchAutoComplete_Name.add(_keyword_name);
							
							}

						}
						else 
						{
							Log.e("ServiceHandler", "Couldn't get any data from the url");
						}

						ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Category_Wise_Activity.this,android.R.layout.simple_spinner_item, SearchAutoComplete_Name);
						dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						SearchAutoCompleteTextView.setAdapter(dataAdapter);
						
					}


				});
				
				
				SearchAutoCompleteTextView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						SearchAutoCompleteTextView.showDropDown();
					}
				});
				SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						
						TempData.SEARCH_ID=SearchAutoComplete_ID.get(arg2);
						Log.e("IDDDDD", String.valueOf(SearchAutoComplete_ID.get(arg2)));
					Intent in=new Intent(Category_Wise_Activity.this,SearchDetails.class);
					startActivity(in);
					
					
					}
				});
			}
		}
		
		
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
			if(keyCode==KeyEvent.KEYCODE_BACK)
			{
				/*Intent idd = new Intent(Category_Wise_Activity.this, MainActivity_Static.class);
				startActivity(idd);*/
				finish();
				return true;
			}
			return super.onKeyDown(keyCode, event);

		}
}
