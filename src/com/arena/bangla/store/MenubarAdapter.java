package com.arena.bangla.store;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenubarAdapter extends ArrayAdapter<RowItem> {

	Context context;

	public MenubarAdapter(Context context, int resourceId, List<RowItem> items) 
	{
		super(context, resourceId, items);
		this.context = context;
	}
 
	private class ViewHolder 
	{
		
		TextView txtTitle;
		TextView txtDesc;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		ViewHolder holder = null;
		RowItem rowItem = getItem(position);

		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) 
		{
			convertView = mInflater.inflate(R.layout.list_item_top_menu, null);
			holder = new ViewHolder(); 
			
			//setting title and icon
			holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
		
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();
 
			holder.txtTitle.setText(rowItem.getTitle());
			

			return convertView;
	}
}