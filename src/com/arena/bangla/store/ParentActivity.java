package com.arena.bangla.store;

import com.arena.bangla.store.R;
import com.arena.bangla.store.R.color;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

public class ParentActivity extends Activity{
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{   
		super.onCreate(savedInstanceState); 
		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

	}  
	
	
	
	public void ViewMenu(int App,int Video,int Audio,int Wallpaper,int Game,int Books,int LatestNews,String ID) {
		
//		ViewInitializationAndAction(R.id.App_RelativeLayout,R.id.Video_RelativeLayout,R.id.MP3_RelativeLayout,R.id.Wallpaper_RelativeLayout,R.id.Game_RelativeLayout,R.id.Book_RelativeLayout,R.id.LatestNews_RelativeLayout,SubLink_);
		
		
	/*	RelativeLayout App_RelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout Video_RelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout MP3_RelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout Game_RelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout Wallpaper_RelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout Book_RelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNews_RelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);*/
		
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(App);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(Video);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(Audio);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(Wallpaper);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(Game);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(Books);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(LatestNews);
		
		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		String Screen2 = sharedpreferences.getString("HomeScreen", null);
		
		if(Screen2.equals("MainActivity_Static"))
		{
			AppRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "1");
					idn.putExtra("TitleName", "অ্যাপস");
					startActivity(idn);
//					finish();
				}
			});
			
			
			VideoRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "2");
					idn.putExtra("TitleName", "ভিডিও");
					startActivity(idn);
//					finish();
				}
			});
			AudioRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "3");
					idn.putExtra("TitleName", "গান");
					startActivity(idn);
//					finish();
				}
			});
			WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "4");
					idn.putExtra("TitleName", "ওয়ালপেপার");
					startActivity(idn);
//					finish();
				}
			});
			GameRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "5");
					idn.putExtra("TitleName", "গেমস");
					startActivity(idn);
//					finish();
				}
			});
			BooksRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), Category_Wise_Activity.class);
					idn.putExtra("SubLink", "6");
					idn.putExtra("TitleName", "বই");
					startActivity(idn);
//					finish();
				}
			});
			
			LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
					idn.putExtra("SubLink", "1000");
					idn.putExtra("TitleName", "সর্বশেষ সংবাদ");
					startActivity(idn);
//					finish();
				}
			});
		}
		else
		{
			AppRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
					idn.putExtra("SubLink", "1");
					idn.putExtra("TitleName", "অ্যাপস");
					startActivity(idn);
//					finish();
				}
			});
			
			
			VideoRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
					idn.putExtra("SubLink", "2");
					idn.putExtra("TitleName", "ভিডিও");
					startActivity(idn);
//					finish();
				}
			});
			AudioRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
					idn.putExtra("SubLink", "3");
					idn.putExtra("TitleName", "গান");
					startActivity(idn);
//					finish();
				}
			});
			WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
					idn.putExtra("SubLink", "4");
					idn.putExtra("TitleName", "ওয়ালপেপার");
					startActivity(idn);
//					finish();
				}
			});
			GameRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
					idn.putExtra("SubLink", "5");
					idn.putExtra("TitleName", "গেমস");
					startActivity(idn);
//					finish();
				}
			});
			BooksRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
					idn.putExtra("SubLink", "6");
					idn.putExtra("TitleName", "বই");
					startActivity(idn);
//					finish();
				}
			});
			
			LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
					idn.putExtra("SubLink", "1000");
					idn.putExtra("TitleName", "সর্বশেষ সংবাদ");
					startActivity(idn);
//					finish();
				}
			});
		}
		


		Log.e("UUUUUUUUUUUUUUUUUUUU", "UUUUUUUUUUUUUUUUUUUUUU"+ID);
		
		if(ID.equals("1"))
		{
//			AppBtn1.setTextColor(color.ActionBar_Orange);
			AppRelativeLayout.setBackground(getResources().getDrawable(R.color.appinit_btn_hover_menu));
		}			
		else if(ID.equals("2"))
		{
//			VideoBtn1.setTextColor(color.ActionBar_Orange);
			VideoRelativeLayout.setBackground(getResources().getDrawable(R.color.appinit_btn_hover_menu));
		}
		else if(ID.equals("3"))
		{
//			AudioBtn1.setTextColor(color.ActionBar_Orange);
			AudioRelativeLayout.setBackground(getResources().getDrawable(R.color.appinit_btn_hover_menu));
		}
		else if(ID.equals("4"))
		{
//			WallpaperBtn1.setTextColor(color.ActionBar_Orange);
			Log.e("UUUUUUUUUUUUUUUUUUUU", "UUUUUUUUUUU  WallpaperRelativeLayout  UUUUUUUUUUU"+ID);
			WallpaperRelativeLayout.setBackground(getResources().getDrawable(R.color.appinit_btn_hover_menu));
		}
		else if(ID.equals("5"))
		{
			Log.e("UUUUUUUUUUUUUUUUUUUU", "UUUUUUUUUUU  GameRelativeLayout  UUUUUUUUUUU"+ID);
//			GameBtn1.setTextColor(color.ActionBar_Orange);
			GameRelativeLayout.setBackground(getResources().getDrawable(R.color.appinit_btn_hover_menu));
		}
		else if(ID.equals("6"))
		{
//			BookBtn1.setTextColor(color.ActionBar_Orange);
			BooksRelativeLayout.setBackground(getResources().getDrawable(R.color.appinit_btn_hover_menu));
		}
		else if(ID.equals("1000"))
		{
//			LatestNewsBtn1.setTextColor(color.ActionBar_Orange);
			LatestNewsRelativeLayout.setBackground(getResources().getDrawable(R.color.appinit_btn_hover_menu));
		}
		
		


	}

}
