package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.AdapterLatestForMainScreen;
import com.arena.adapter.CustomAdapter;
import com.arena.adapter.CustomAdapterTemplateHomeScreen;
import com.arena.bangla.store.R;
import com.arena.bangla.store.R.color;
import com.arena.bangla.store.TemplateHome_InnerScreenActivity.MP3Json;
import com.arena.bangla.store.MainActivity_Static.SearchAutoCompleteTextViewList;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;
import com.arena.menu.MenuDialog;
import com.devsmart.android.ui.HorizontalListView;



import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class TemplateHomeScreenActivity extends ParentActivity {

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;

	ListView lv;
	Context context;

	ProgressDialog pDialog;
	String ParentType="abc";
	String response;
	ArrayList<ArrayList< HashMap<String, String>>> collection=new ArrayList<ArrayList< HashMap<String, String>>>();
	String SubLink_, TitleName;
	TextView AppBtn1, VideoBtn1, AudioBtn1, GameBtn1, WallpaperBtn1,BookBtn1,LatestNewsBtn1;

	ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
	ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();
	String response12;
	AutoCompleteTextView SearchAutoCompleteTextView;

	ArrayList<HashMap<String, String>> ContentArrayListLatest;


	ImageView SearchingBackBtn;
	Button SearchingCategoryBtn, SearchingSearchingBtn, CategoryBtn;
	RelativeLayout CategoryLayout, SearchingLayout;
	Button MenuBtn;
	
	String Language ="";
	
	public static Boolean isScrolling = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		 StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

		setContentView(R.layout.template_home_screen);

		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.putString("HomeScreen", "TemplateHomeScreenActivity");
		editor.putString("PINCODE", "no");
		editor.putString("MobileNumberForPurchase", "no");
		editor.putString("download", "no");
		editor.commit();
		
		/*  Bundle b = getIntent().getExtras();
	        SubLink_ = b.getString("SubLink");
	        TitleName = b.getString("TitleName");*/


		SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
		//		SearchingSearchingBtn = (Button)findViewById(R.id.SearchingSearchingBtn);
		//		CategoryBtn = (Button)findViewById(R.id.category);

		SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);

		CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
		SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);
		MenuBtn = (Button)findViewById(R.id.MenuBtn);

		/* SearchMenuOption(TitleName);
	        ViewInitializationAndActionofMenu(SubLink_);
	        ViewInitializationAndAction();*/
		ViewMenu(R.id.App_RelativeLayout,R.id.Video_RelativeLayout,R.id.MP3_RelativeLayout,R.id.Wallpaper_RelativeLayout,R.id.Game_RelativeLayout,R.id.Book_RelativeLayout,R.id.LatestNews_RelativeLayout,"1111");
		
//		 Scrolling();
		
		
		
		
		
		Language = sharedpreferences.getString("Language", "");
		if(Language.equals("English"))
		{
			Language="";
			
		}
		
		Log.e("XXXXXXXXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+Language);
		
	

		context=this;
		lv=(ListView) findViewById(R.id.mainsListview);

		SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
		SearchAutoCompleteTextView.setThreshold(1);

		ContentArrayListLatest = new ArrayList<HashMap<String, String>>();

		
		
		if(isInternetOn())
		{
			try {
				new MP3Json().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			Toast.makeText(TemplateHomeScreenActivity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
			
		}


		SearchingCategoryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~`");

				CategoryLayout.setVisibility(View.GONE);
				SearchingLayout.setVisibility(View.VISIBLE);
				new SearchAutoCompleteTextViewList().execute();

			}
		});

		SearchingBackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~`");

				CategoryLayout.setVisibility(View.VISIBLE);
				SearchingLayout.setVisibility(View.GONE);


			}
		});

		MenuBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				MenuDialog menu = new MenuDialog(TemplateHomeScreenActivity.this); 
				menu.show(); 
			}
		});
		
		
		
		/*lv.setOnScrollListener(new OnScrollListener() {
			public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCoun) {
			}
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			        if (scrollState != 0)
			        	lv.getAdapter()).isScrolling = true;
			        else {
			            adapter.isScrolling = false;
			            adapter.notifyDataSetChanged();
			            lv.getAdapter().isScrolling = false;
			        }
			} });*/
		

	}

	class MP3Json extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(TemplateHomeScreenActivity.this);
			pDialog.setMessage("");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				/*String url=URLs.DOMAIN2+SubLink_+"?lang=bn";
					Log.e("---------url-------------", url);
					response = JsonParse.makeServiceCall(url);
					response = "{"+'"'+"Category"+'"'+":"+response+"}";
					Log.e("---------response-------------", response);*/

				String url=URLs.MainScreen+Language;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);
				//					response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Category"+'"'+":["+response12+"]}";
				//Log.e("---------response-------------", response12);


			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {
						//							parseXmlresponseForItemCatagory(response);
						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				/*private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
					{
						String respon = "{"+'"'+"Category"+'"'+":}";
						if (!response.equals(respon)) 
						{
							JSONObject jsonObj = new JSONObject(MyResponse);
							JSONArray ContentList = jsonObj.getJSONArray("Category");

							for (int i = 0; i < ContentList.length(); i++)
							{

								JSONObject cateHome = ContentList.getJSONObject(i);
								String _id = cateHome.getString("id");
								String _type_name = cateHome.getString("category_title");
								String _short_description = cateHome.getString("short_description");


								String ContentResponse = cateHome.getString("content");
								if(!ContentResponse.equals("[]"))
								{
									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";
									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);
									JSONArray Content_List = ContentJsonObj.getJSONArray("content");
								    ArrayList< HashMap<String, String>> catagoryList= new ArrayList< HashMap<String, String>>();
									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject con= Content_List.getJSONObject(j);
										String _content_id = con.getString("id");
										String _content_title = con.getString("content_title");
										String _preview = con.getString("preview");
										String _total_download = con.getString("total_download");
										String _payment_type = con.getString("payment_type");
										String _price = con.getString("price");
										String _rating = con.getString("rating");


										HashMap<String, String> map = new HashMap<String, String>();
										map.put("ParentID",_id);
										map.put("ParentType", _type_name);
										map.put("ParentShortDescription", _short_description);

										map.put("content_id", _content_id);
										map.put("content_title", _content_title);
										map.put("preview", _preview);
									    map.put("total_download", _total_download);
										map.put("payment_type", _payment_type);
										map.put("price", _price);
										map.put("rating", _rating);
										map.put("SubLink", SubLink_);
										catagoryList.add(map);


										if(!_type_name.equalsIgnoreCase(ParentType))
										{
										collection.add(catagoryList);
										ParentType=_type_name;
										}

									}


								}


							}
						}
						else 
						{
						}

					}*/


				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Category"+'"'+":}";

					//Log.e("---------respon-------------", respon);

					if (!response.equals(respon)) 
					{




						//Log.e("---------response-------------", response);


						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");
						Log.e("SIZE", ""+ContentList.length());
						
						

						for (int i = 0; i <ContentList.length(); i++)
						{
						
	
							
							
						/*	if(ContentList.getJSONArray(i)!=null)
							{*/

							JSONObject cate = ContentList.getJSONObject(i);



							//String latest_content12 = cate.getString("latest_content");

							String home_content12 = cate.getString("home_content");

							String TopBanner_content12 = cate.getString("top_banner");

							//Log.e("---------latest_content12 **************", latest_content12);
							Log.e("---------home_content-**************", home_content12);
							Log.e("---------Top_content----**********-", TopBanner_content12);


						//	String Latest_ContentResponse = "{"+'"'+"latest_content"+'"'+":["+latest_content12+"]}";
							String Home_ContentResponse = "{"+'"'+"home_content"+'"'+":"+home_content12+"}";
							String TopBanner_ContentResponse = "{"+'"'+"top_banner"+'"'+":"+TopBanner_content12+"}";

						//	Log.e("---------latest_contentResponse-------------", Latest_ContentResponse);
							Log.e("---------Home_ContentResponse-------------", Home_ContentResponse);
							Log.e("---------TopBanner_ContentResponse-------------", TopBanner_ContentResponse);


							JSONObject TopBanner_ContentJsonObj = new JSONObject(TopBanner_ContentResponse);

							JSONObject TOP_BC = TopBanner_ContentJsonObj.getJSONObject("top_banner");

							/*Top_banner_content_id = TOP_BC.getString("content_id");
								Top_banner_content_type_id = TOP_BC.getString("content_type_id");
								Top_banner_image = TOP_BC.getString("banner");


								Log.e("---------Top_banner_content_id-------------", Top_banner_content_id);
								Log.e("---------Top_banner_image-****&&&&&&&-------------", Top_banner_image);*/





							//JSONObject Latest_ContentJsonObj = new JSONObject(Latest_ContentResponse);
							//JSONArray Latest_Content_List = Latest_ContentJsonObj.getJSONArray("latest_content");

							// content node is JSON Object

							/*for (int j = 0; j < Latest_Content_List.length(); j++)
							{
								JSONObject LCL = Latest_Content_List.getJSONObject(j);
								String category_title = LCL.getString("category_title");

								Log.e("---------category_title-------------", category_title);

								String content_for_latest = LCL.getString("content");
								Log.e("---------content_for_latest-------------", content_for_latest);

								String Latest_Content_Content_Response = "{"+'"'+"latest_content_content"+'"'+":"+content_for_latest+"}";
								Log.e("---------Latest_Content_Content_Response-------------", Latest_Content_Content_Response);

								JSONObject Latest_ContentContent_JsonObj = new JSONObject(Latest_Content_Content_Response);

								JSONArray Latest_ContentContent_List = Latest_ContentContent_JsonObj.getJSONArray("latest_content_content");

								if(Latest_ContentContent_List.length()!=0)
								{

									for (int k = 0; k < Latest_ContentContent_List.length(); k++)
									{
										JSONObject LCCL = Latest_ContentContent_List.getJSONObject(k);

										String latest_content_id = LCCL.getString("id");
										String type_id = LCCL.getString("type_id");
										String content_title = LCCL.getString("content_title");
										String preview = LCCL.getString("preview");
										String total_download = LCCL.getString("total_download");
										String payment_type = LCCL.getString("payment_type");
										String price = LCCL.getString("price");
										String rating = LCCL.getString("rating");
										
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEE latest_content_id  "+latest_content_id);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE type_id  "+type_id);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE content_title"+content_title);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE preview"+preview);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE total_download"+total_download);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE payment_type"+payment_type);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE price"+price);
										Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEE rating"+rating);


										HashMap<String, String> LatestContent = new HashMap<String, String>();

										LatestContent.put("latest_content_id", latest_content_id);
										LatestContent.put("type_id", type_id);
										LatestContent.put("content_title", content_title);
										LatestContent.put("preview", preview);
										LatestContent.put("total_download", total_download);
										LatestContent.put("payment_type", payment_type);
										LatestContent.put("price", price);
										LatestContent.put("rating", rating);

										ContentArrayListLatest.add(LatestContent);
									}
								}									

							}*/


							Log.e("GGGGGGGGGGGGGGGG", "GGGGGGGGGGGGG  Home_ContentResponse   "+Home_ContentResponse);
							JSONObject Home_ContentJsonObj = new JSONObject(Home_ContentResponse);
							JSONArray Home_Content_List = Home_ContentJsonObj.getJSONArray("home_content");

							
							
							
							for (int i1 = 0; i1 < Home_Content_List.length(); i1++)
							{

								JSONObject cateHome = Home_Content_List.getJSONObject(i1);

								String _type_id = cateHome.getString("type_id");
								String _type_name = cateHome.getString("type_name");
								String _short_description = cateHome.getString("short_description");


								//									appTv, videoTv, mp3Tv, gameTv, bookTv, wallapperTv;

								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_type_id);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_type_name);
								Log.e("EEEEEEEEEEEEEEEEEE", "EEEEEEEEEEEEEEEEEEEEEEEEe"+_short_description);


								String ContentResponse = cateHome.getString("content");	



								if(!ContentResponse.equals("[]"))
								{

									/*HashMap<String, String> Info = new HashMap<String, String>();
										Info.put("id", _id);
										Info.put("type_name", _type_name);
										Info.put("short_description", _short_description);



										Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_id);
										Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_type_name);
										Log.e("~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~"+_short_description);*/


									String _ContentResponse = "{"+'"'+"content"+'"'+":"+ContentResponse+"}";

									JSONObject ContentJsonObj = new JSONObject(_ContentResponse);

									JSONArray Content_List = ContentJsonObj.getJSONArray("content");

									// content node is JSON Object
									ArrayList< HashMap<String, String>> catagoryList= new ArrayList< HashMap<String, String>>();
									for (int j = 0; j < Content_List.length(); j++)
									{
										JSONObject c = Content_List.getJSONObject(j);

										String _content_id = c.getString("id");
										String _content_title = c.getString("content_title");
										String _preview = c.getString("preview");
										String _rating = c.getString("rating");


										Log.e("EEEEEEEEEEEEEEEEEE", "HHHHHHHHHHHHH  _content_id "+_content_id);
										Log.e("EEEEEEEEEEEEEEEEEE", "HHHHHHHHHHHHH  _content_title  "+_content_title);
										Log.e("EEEEEEEEEEEEEEEEEE", "HHHHHHHHHHHHH   _preview"+ _preview);
										

										HashMap<String, String> map = new HashMap<String, String>();
										map.put("ParentID",_type_id);
										map.put("ParentType", _type_name);
										map.put("ParentShortDescription", _short_description);

										map.put("content_id", _content_id);
										map.put("content_title", _content_title);
										map.put("preview", _preview);
										map.put("rating", _rating);
										map.put("SubLink", _type_id);
										catagoryList.add(map);
										if(!_type_name.equalsIgnoreCase(ParentType))
										{
											collection.add(catagoryList);
											ParentType=_type_name;
										}



									}
									//		                      InfoList.add(Info);
								}
								else
								{
									Log.e("ServiceHandler", "Couldn't get any data from the content");

								}



							}

							
						}



					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(TemplateHomeScreenActivity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}


			});

			lv.setAdapter(new CustomAdapterTemplateHomeScreen(TemplateHomeScreenActivity.this, collection));
			pDialog.dismiss();

		}

	}


	private void SearchMenuOption(String Name) {

		Button SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
		ImageView SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);		
		TextView NameBack = (TextView)findViewById(R.id.TitleText);	

		final RelativeLayout CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
		final RelativeLayout SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);

		NameBack.setText(Name);

		SearchingCategoryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				CategoryLayout.setVisibility(View.GONE);
				SearchingLayout.setVisibility(View.VISIBLE);
				new SearchAutoCompleteTextViewList().execute();

			}
		});

		SearchingBackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				CategoryLayout.setVisibility(View.VISIBLE);
				SearchingLayout.setVisibility(View.GONE);


			}
		});

		NameBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				finish();
			}
		});

	}
	/*private void ViewInitializationAndActionofMenu(String ID) {

		TextView AppBtn1 = (TextView)findViewById(R.id.AppBtn1);
		TextView VideoBtn1 = (TextView)findViewById(R.id.VideoBtn1);
		TextView AudioBtn1 = (TextView)findViewById(R.id.AudioBtn2);
		TextView WallpaperBtn1 = (TextView)findViewById(R.id.WallpaperBtn2);
		TextView GameBtn1 = (TextView)findViewById(R.id.GameBtn2);
		TextView BookBtn1 = (TextView)findViewById(R.id.BookBtn2);
		TextView LatestNewsBtn1 = (TextView)findViewById(R.id.LatestNewsBtn2);

		if(ID.equals("1"))
			AppBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("2"))
			VideoBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("3"))
			AudioBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("4"))
			WallpaperBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("5"))
			GameBtn1.setTextColor(color.ActionBar_Orange);

		else if(ID.equals("6"))
			BookBtn1.setTextColor(color.ActionBar_Orange);
		else if(ID.equals("1000"))
			LatestNewsBtn1.setTextColor(color.ActionBar_Orange);



	}

	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);


		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "1");
				idn.putExtra("TitleName", "অ�?যাপস");
				startActivity(idn);
				//					finish();
			}
		});


		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "2");
				idn.putExtra("TitleName", "ভিডিও");
				startActivity(idn);
				//					finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "3");
				idn.putExtra("TitleName", "�?মপিথ�?রি");
				startActivity(idn);
				//					finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "4");
				idn.putExtra("TitleName", "ওয়ালপেপার");
				startActivity(idn);
				//					finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "5");
				idn.putExtra("TitleName", "গেইম");
				startActivity(idn);
				//					finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "6");
				idn.putExtra("TitleName", "বই");
				startActivity(idn);
				//					finish();
			}
		});

		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
				idn.putExtra("SubLink", "1000");
				idn.putExtra("TitleName", "সর�?বশেষ সংবাদ");
				startActivity(idn);
				//					finish();
			}
		});



	}*/



	class SearchAutoCompleteTextViewList extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			SearchAutoComplete_ID.clear();
			SearchAutoComplete_Name.clear();


		}

		@Override
		protected String doInBackground(String... args) {


			try {
				String url=URLs.Search_Keyword;
				//				String response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url);

				response = "{"+'"'+"Category"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response12);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {
			runOnUiThread(new Runnable() {
				@SuppressLint("SetJavaScriptEnabled")
				@Override
				public void run() {

					try {


						parseXmlresponseForProductCatagory(response);

					} catch (Exception e) { }

				}

				private void parseXmlresponseForProductCatagory(String MyResponse) throws JSONException 
				{


					if (response != null) 
					{

						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Category");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject cate = ContentList.getJSONObject(i);

							String _id = cate.getString("id");
							String _keyword_name = cate.getString("keyword");

							SearchAutoComplete_ID.add(_id);
							SearchAutoComplete_Name.add(_keyword_name);

						}

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
					}

					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(TemplateHomeScreenActivity.this,android.R.layout.simple_spinner_item, SearchAutoComplete_Name);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					SearchAutoCompleteTextView.setAdapter(dataAdapter);

				}


			});


			SearchAutoCompleteTextView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SearchAutoCompleteTextView.showDropDown();
				}
			});
			SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {

					TempData.SEARCH_ID=SearchAutoComplete_ID.get(arg2);
					Log.e("IDDDDD", String.valueOf(SearchAutoComplete_ID.get(arg2)));
					Intent in=new Intent(TemplateHomeScreenActivity.this,SearchDetails.class);
					startActivity(in);


				}
			});
		}
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			/*Intent idd = new Intent(Category_Wise_Activity.this, MainActivity_Static.class);
				startActivity(idd);*/
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
	
	
	private void Scrolling() {
		// TODO Auto-generated method stub

		ListView ScrollViewApp=(ListView)findViewById(R.id.mainsListview);


		ScrollViewApp.setOnTouchListener(new ListView.OnTouchListener() {

			float x1=0;
			float x2=0;
			float y1=0,y2=0;
			float deltaX=0,deltaY=0;
			@Override
			public boolean onTouch(View v, MotionEvent event) {


				int action = event.getAction();

				if (event.getAction()==MotionEvent.ACTION_DOWN) {

					x1=event.getX();
					y1=event.getY();
					Log.e("X1", String.valueOf(x1));
					Log.e("Y1", String.valueOf(y1));

				}

				if (event.getAction()==MotionEvent.ACTION_MOVE){

					x2=event.getX();
					y2=event.getY();
					Log.e("X2", String.valueOf(x2));
					Log.e("Y2", String.valueOf(y2));
					deltaX=Math.abs(x2-x1);
					deltaY=Math.abs(y2-y1);
					Log.e("DELTA_X", String.valueOf(deltaX));
					Log.e("DELTA_Y", String.valueOf(deltaY));

					if(deltaX>deltaY)
						return true;
					else
						return false;

				}


				v.onTouchEvent(event);

				return true;
			}
		});
	}
	
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
}
