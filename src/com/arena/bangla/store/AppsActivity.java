package com.arena.bangla.store;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.arena.adapter.AdapterForMainScreen;
import com.arena.adapter.LazyAdapter;
import com.arena.bangla.store.MainActivity_Static.MainScreenShow;
import com.arena.image.loader.TempData;
import com.arena.json.JsonParse;
import com.arena.json.URLs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AppsActivity extends ParentActivity{
	
	String Language ="";
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	
	ProgressDialog pDialog;
	String response;
	String response12;
//	LazyAdapter adapter;
	LazyAdapter adapter;
	
	ArrayList<HashMap<String, String>> ContentArrayList;
	GridView gv;
	
	String Content_Id,SubLink,TitleName;
	
	 ArrayList<String> SearchAutoComplete_ID=new ArrayList<String>();
     ArrayList<String> SearchAutoComplete_Name=new ArrayList<String>();
     String responseSearch;
     AutoCompleteTextView SearchAutoCompleteTextView;
     
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.apps_screen);
		
		Bundle b= getIntent().getExtras();
		Content_Id = b.getString("Content_Id");
		SubLink = b.getString("SubLink");
		TitleName = b.getString("TitleName");
		
		Log.e("KKKKKKKKKKKKKKKKKKKK", "KKKKKKKKKKKKKKKKKKKKKKK"+SubLink);
		
		SearchMenuOption(TitleName);
//		ViewInitializationAndAction();
		ViewMenu(R.id.App_RelativeLayout,R.id.Video_RelativeLayout,R.id.MP3_RelativeLayout,R.id.Wallpaper_RelativeLayout,R.id.Game_RelativeLayout,R.id.Book_RelativeLayout,R.id.LatestNews_RelativeLayout,SubLink);
//	    ViewInitializationAndActionofMenu(SubLink);
		
		gv = (GridView)findViewById(R.id.gridView14);
		ContentArrayList = new ArrayList<HashMap<String, String>>();
		  
        SearchAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewSearch);
		SearchAutoCompleteTextView.setThreshold(1);
		
		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		Language = sharedpreferences.getString("Language", "");
		if(Language.equals("English"))
		{
			Language="";		
		}
		
		if(isInternetOn())
		{
			try {
				new VideoScreenShow().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			Toast.makeText(AppsActivity.this, URLs.InternetMSG, Toast.LENGTH_LONG).show();
			
		}

		
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				
						
				
				HashMap<String, String> data = ContentArrayList.get(position);
				
				if(SubLink.equals("1"))
				{
					
					Intent idn = new Intent(AppsActivity.this, AppsDetailsViewActivity.class);
					idn.putExtra("Content_Id", data.get("content_id"));
					idn.putExtra("which_screen", "InnerScreen");
					idn.putExtra("SubLink", SubLink);
					startActivity(idn);
				}
				if(SubLink.equals("2"))
				{
					Intent idn = new Intent(AppsActivity.this, VideoDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("content_id"));
					idn.putExtra("which_screen", "InnerScreen");
					idn.putExtra("SubLink", SubLink);
					startActivity(idn);
				}
				if(SubLink.equals("3"))
				{
					Intent idn = new Intent(AppsActivity.this, AudioDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("content_id"));
					idn.putExtra("which_screen", "InnerScreen");
					idn.putExtra("SubLink", SubLink);
					startActivity(idn);
				}
				if(SubLink.equals("4"))
				{
					Intent idn = new Intent(AppsActivity.this, WallpaperDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("content_id"));
					idn.putExtra("which_screen", "InnerScreen");
					idn.putExtra("SubLink", SubLink);
					startActivity(idn);
				}
				
				if(SubLink.equals("5"))
				{
					Intent idn = new Intent(AppsActivity.this, GameDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("content_id"));
					idn.putExtra("which_screen", "InnerScreen");
					idn.putExtra("SubLink", SubLink);
					startActivity(idn);
				}
				
				if(SubLink.equals("6"))
				{
					Intent idn = new Intent(AppsActivity.this, BooksDetailsActivity.class);
					idn.putExtra("Content_Id", data.get("content_id"));
					idn.putExtra("which_screen", "InnerScreen");
					idn.putExtra("SubLink", SubLink);
					startActivity(idn);
				}
				
				
	

			}
		});
		
	}
	
	
	private void ViewInitializationAndAction() {
		// TODO Auto-generated method stub
		RelativeLayout AppRelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout VideoRelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout AudioRelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout WallpaperRelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout GameRelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout BooksRelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNewsRelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		
		AppRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "1");
				startActivity(idn);
				finish();
			}
		});
		
		
		VideoRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "2");
				startActivity(idn);
				finish();
			}
		});
		AudioRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "3");
				startActivity(idn);
				finish();
			}
		});
		WallpaperRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "4");
				startActivity(idn);
				finish();
			}
		});
		GameRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "5");
				startActivity(idn);
				finish();
			}
		});
		BooksRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", "6");
				startActivity(idn);
				finish();
			}
		});
		
		LatestNewsRelativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent idn = new Intent(getApplicationContext(), LatestNewsActivity.class);
				idn.putExtra("SubLink", "1000");
				startActivity(idn);
				finish();
			}
		});



	}


	class VideoScreenShow extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(AppsActivity.this);
			pDialog.setMessage("");
			pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {

			try {
				String url=URLs.CategoryWise+SubLink+"/"+Content_Id+Language;
				//				String response12 = JsonParse.makeServiceCall(url);
//				response12 = JsonParse.makeServiceCall(url);
				response12 = JsonParse.makeServiceCall(url).replaceAll("<pre>", "");
				response = "{"+'"'+"Contents"+'"'+":"+response12+"}";
				Log.e("---------response-------------", response);

			} catch (Exception e) {
			}

			return null;
		}

		@Override
		protected void onPostExecute(String file_url) {

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {


						parseXmlresponseForItemCatagory(response);

					} catch (Exception e) { }



				}

				private void parseXmlresponseForItemCatagory(String MyResponse) throws JSONException 
				{
					String respon = "{"+'"'+"Contents"+'"'+":}";
					Log.e("---------respon-------------", respon);
					
					if (!response.equals(respon)) 
					{
						
						
						
						JSONObject jsonObj = new JSONObject(MyResponse);

						// Getting JSON Array node
						JSONArray ContentList = jsonObj.getJSONArray("Contents");


						// looping through All Contacts
						for (int i = 0; i < ContentList.length(); i++)
						{

							JSONObject con = ContentList.getJSONObject(i);

							
							/*id": "10",
					        "content_title": "Depika's New Video",
					        "preview": "http:\/\/192.168.1.20\/apponit\/uploaded_content\/videos\/preview\/20160525144140.png",
					        "total_download": "0",
					        "payment_type": "Paid",
					        "price": "10.00",
					        "rating": 4*/
					        
							String _content_id = con.getString("id");
							String _content_title = con.getString("content_title");
							String _preview = con.getString("preview");
							String _total_download = con.getString("total_download");
							String _payment_type = con.getString("payment_type");
							String _price = con.getString("price");
							String _rating = con.getString("rating");
							

							HashMap<String, String> Content12 = new HashMap<String, String>();
							
							
							Content12.put("content_id", _content_id);
							Content12.put("content_title", _content_title);
							Content12.put("preview", _preview);
						    Content12.put("total_download", _total_download);
							Content12.put("payment_type", _payment_type);
							Content12.put("price", _price);
							Content12.put("rating", _rating);
							
							ContentArrayList.add(Content12);
							
						}
						adapter = new LazyAdapter(AppsActivity.this, ContentArrayList);	
						gv.setAdapter(adapter);
							

					}
					else 
					{
						Log.e("ServiceHandler", "Couldn't get any data from the url");
						Toast.makeText(AppsActivity.this, "Couldn't get any data from the url", Toast.LENGTH_LONG).show();
					}

				}

			});
			pDialog.dismiss();



		}

	}
	
	
	 private void SearchMenuOption(String Name) {
			
			Button SearchingCategoryBtn = (Button)findViewById(R.id.SearchCategoryBtn);
			ImageView SearchingBackBtn = (ImageView)findViewById(R.id.SearchingBackBtn);		
			TextView NameBack = (TextView)findViewById(R.id.TitleText);	
			
			final RelativeLayout CategoryLayout = (RelativeLayout)findViewById(R.id.home_logoCategory);
			final RelativeLayout SearchingLayout = (RelativeLayout)findViewById(R.id.home_logoSeraching);
			
			NameBack.setText(Name);
			
			SearchingCategoryBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
									
					CategoryLayout.setVisibility(View.GONE);
					SearchingLayout.setVisibility(View.VISIBLE);
					new SearchAutoCompleteTextViewList().execute();
					
				}
			});
			
			SearchingBackBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
										
					CategoryLayout.setVisibility(View.VISIBLE);
					SearchingLayout.setVisibility(View.GONE);
					
					
				}
			});
			
			NameBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					finish();
				}
			});
			
		}
	 
		class SearchAutoCompleteTextViewList extends AsyncTask<String, String, String> {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				SearchAutoComplete_ID.clear();
				SearchAutoComplete_Name.clear();
				
				
			}

			@Override
			protected String doInBackground(String... args) {
				

				try {
					String url=URLs.Search_Keyword;
					//				String response12 = JsonParse.makeServiceCall(url);
					responseSearch = JsonParse.makeServiceCall(url);

					response = "{"+'"'+"Category"+'"'+":"+responseSearch+"}";
					Log.e("---------response-------------", responseSearch);

				} catch (Exception e) {
				}

				return null;
			}

			@Override
			protected void onPostExecute(String file_url) {
				runOnUiThread(new Runnable() {
					@SuppressLint("SetJavaScriptEnabled")
					@Override
					public void run() {

						try {


							parseXmlresponseForProductCatagory(response);

						} catch (Exception e) { }

					}

					private void parseXmlresponseForProductCatagory(String MyResponse) throws JSONException 
					{


						if (response != null) 
						{
							
							JSONObject jsonObj = new JSONObject(MyResponse);

							// Getting JSON Array node
							JSONArray ContentList = jsonObj.getJSONArray("Category");


							// looping through All Contacts
							for (int i = 0; i < ContentList.length(); i++)
							{

								JSONObject cate = ContentList.getJSONObject(i);

								String _id = cate.getString("id");
								String _keyword_name = cate.getString("keyword");
								
								SearchAutoComplete_ID.add(_id);
								SearchAutoComplete_Name.add(_keyword_name);
							
							}

						}
						else 
						{
							Log.e("ServiceHandler", "Couldn't get any data from the url");
						}

						ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AppsActivity.this,android.R.layout.simple_spinner_item, SearchAutoComplete_Name);
						dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						SearchAutoCompleteTextView.setAdapter(dataAdapter);
						
					}


				});
				
				
				SearchAutoCompleteTextView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						SearchAutoCompleteTextView.showDropDown();
					}
				});
				SearchAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						
						TempData.SEARCH_ID=SearchAutoComplete_ID.get(arg2);
						Log.e("IDDDDD", String.valueOf(SearchAutoComplete_ID.get(arg2)));
					Intent in=new Intent(AppsActivity.this,SearchDetails.class);
					startActivity(in);
					
					
					}
				});
			}
		}
		
	
	public final boolean isInternetOn() {
		ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

			return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

			return false;
		}
		return false;
	}
	
	
	/*private void ViewInitializationAndActionofMenu(String ID) {
		
		TextView AppBtn1 = (TextView)findViewById(R.id.AppBtn1);
		TextView VideoBtn1 = (TextView)findViewById(R.id.VideoBtn1);
		TextView AudioBtn1 = (TextView)findViewById(R.id.AudioBtn2);
		TextView WallpaperBtn1 = (TextView)findViewById(R.id.WallpaperBtn2);
		TextView GameBtn1 = (TextView)findViewById(R.id.GameBtn2);
		TextView BookBtn1 = (TextView)findViewById(R.id.BookBtn2);
		TextView LatestNewsBtn1 = (TextView)findViewById(R.id.LatestNewsBtn2);
		
		RelativeLayout App_RelativeLayout = (RelativeLayout)findViewById(R.id.App_RelativeLayout);
		RelativeLayout Video_RelativeLayout = (RelativeLayout)findViewById(R.id.Video_RelativeLayout);
		RelativeLayout MP3_RelativeLayout = (RelativeLayout)findViewById(R.id.MP3_RelativeLayout);
		RelativeLayout Game_RelativeLayout = (RelativeLayout)findViewById(R.id.Game_RelativeLayout);
		RelativeLayout Wallpaper_RelativeLayout = (RelativeLayout)findViewById(R.id.Wallpaper_RelativeLayout);
		RelativeLayout Book_RelativeLayout = (RelativeLayout)findViewById(R.id.Book_RelativeLayout);
		RelativeLayout LatestNews_RelativeLayout = (RelativeLayout)findViewById(R.id.LatestNews_RelativeLayout);
		
		if(ID.equals("1"))
		{
			AppBtn1.setTextColor(color.ActionBar_Orange);
			App_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}			
		else if(ID.equals("2"))
		{
			VideoBtn1.setTextColor(color.ActionBar_Orange);
			Video_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("3"))
		{
			AudioBtn1.setTextColor(color.ActionBar_Orange);
			MP3_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("4"))
		{
			WallpaperBtn1.setTextColor(color.ActionBar_Orange);
			Wallpaper_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("5"))
		{
			GameBtn1.setTextColor(color.ActionBar_Orange);
			Game_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("6"))
		{
			BookBtn1.setTextColor(color.ActionBar_Orange);
			Book_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		else if(ID.equals("1000"))
		{
			LatestNewsBtn1.setTextColor(color.ActionBar_Orange);
			LatestNews_RelativeLayout.setBackground(getResources().getDrawable(R.color.ActionBar_Green));
		}
		
		
		
	}*/
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			/*Intent idd = new Intent(AppsActivity.this, MainActivity_Static.class);
			startActivity(idd);*/
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}

}
