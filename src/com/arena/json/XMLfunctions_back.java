package com.arena.json;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
 
public class XMLfunctions_back {

	public final static Document XMLfromString(String xml){
		
		Document doc = null;
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
        	
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			InputSource is = new InputSource();
	        is.setCharacterStream(new StringReader(xml));
	        doc = db.parse(is); 
	        
		} catch (ParserConfigurationException e) {
			System.out.println("XML parse error: " + e.getMessage());
			return null;
		} catch (SAXException e) {
			System.out.println("Wrong XML file structure: " + e.getMessage());
            return null;
		} catch (IOException e) {
			System.out.println("I/O exeption: " + e.getMessage());
			return null;
		}
		       
        return doc;
        
	}
 
	 public final static String getElementValue( Node elem ) {
	     Node kid;
	     if( elem != null){
	         if (elem.hasChildNodes()){
	             for( kid = elem.getFirstChild(); kid != null; kid = kid.getNextSibling() ){
	                 if( kid.getNodeType() == Node.TEXT_NODE  ){
	                     return kid.getNodeValue();
	                 }
	             }
	         }
	     }
	     return "";
	 }
	  
	 public static  String getXMLOther(){	 
			String line = null;

			try {
				
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost("http://newsrss.bbc.co.uk/rss/sportonline_uk_edition/other_sports/rss.xml");

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				line = EntityUtils.toString(httpEntity);
				
			} catch (UnsupportedEncodingException e) {
				line = "<results status=\"error\"><msg>Can't connect to server</msg></results>";
			} catch (MalformedURLException e) {
				line = "<results status=\"error\"><msg>Can't connect to server</msg></results>";
			} catch (IOException e) {
				line = "<results status=\"error\"><msg>Can't connect to server</msg></results>";
			}

			return line;

	}  
	 
	 public static boolean isAvailable(String link){
 
		    boolean available = false; 

		    URL url = null;
			try {
				url = new URL(link);
			} catch (MalformedURLException e) {
				 
				e.printStackTrace();
			}
		    HttpURLConnection connection = null;
			try {
				connection = (HttpURLConnection) url.openConnection();
			} catch (IOException e1) {
				  
			}
		    connection.setRequestProperty("Connection", "close");
		    connection.setConnectTimeout(10000); // Timeout 100 seconds
		 
		    try {
				connection.connect();
			} catch (IOException e) {
				  
			}
 
		    try {
				if (connection.getResponseCode() == 200) {
				 //   return true;
				    available = true;
				}
				else 
					available = false;
					//return false;
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		    
		    return available;
	}
	  
	 public static boolean exists(String URLName){
		    try {
		      HttpURLConnection.setFollowRedirects(false);
		      // note : you may also need
		      //        HttpURLConnection.setInstanceFollowRedirects(false)
		      HttpURLConnection con =
		         (HttpURLConnection) new URL(URLName).openConnection();
		      con.setRequestMethod("HEAD");
		      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		    }
		    catch (Exception e) {
		       e.printStackTrace();
		       return false;
		    }
	 }
	 
	
	 public static  String getXML(String url){	 
			String line = null;

		 	
			if( isAvailable(url) ){
				
				int times = 2; 
				for( int i=0; i<times; i++)
				{ 
	 				try { 
	 			/*		
	 					HttpParams my_httpParams = new BasicHttpParams();
	 					HttpConnectionParams.setConnectionTimeout(my_httpParams, 100);
	 					HttpConnectionParams.setSoTimeout(my_httpParams, 1);  
						DefaultHttpClient httpClient = new DefaultHttpClient(my_httpParams); 
				*/		
					
	 			/*		
						DefaultHttpClient httpClient = new DefaultHttpClient(); 
						HttpPost httpPost = new HttpPost(url);
		
						HttpResponse httpResponse = httpClient.execute(httpPost);
						HttpEntity httpEntity = httpResponse.getEntity();
						line = EntityUtils.toString(httpEntity);
				*/		
						 
	 					HttpGet httpGet = new HttpGet(url);
	 					HttpParams httpParameters = new BasicHttpParams();
	 					// Set the timeout in milliseconds until a connection is established.
	 					// The default value is zero, that means the timeout is not used. 
	 					int timeoutConnection = 10000;
	 					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
	 					// Set the default socket timeout (SO_TIMEOUT) 
	 					// in milliseconds which is the timeout for waiting for data.
	 					int timeoutSocket = 10000;
	 					HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

	 					DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
	 					HttpResponse httpResponse = httpClient.execute(httpGet);
	 					HttpEntity httpEntity = httpResponse.getEntity();
						line = EntityUtils.toString(httpEntity);
	 					  
	 					
						break;
						    
					} 
					 
					catch (RuntimeException e) {
						line = "serverError";
					}
					catch (UnsupportedEncodingException e) {
						line = "serverError";
					}
					catch (MalformedURLException e) {
						line = "serverError";
					}
					catch (IOException e) {
						line = "serverError";
					}
					catch (Exception e) {
						line = "serverError";
					}
 				 
				}
				 
 			}
			else
				line = "serverError";
			 
 			return line; 
	 }
  
 
	public static int numResults(Document doc){		
		Node categorys = doc.getDocumentElement();
		int res = -1;
		
		try{
			res = Integer.valueOf(categorys.getAttributes().getNamedItem("2").getNodeValue());
		}catch(Exception e ){
			res = -1;
		}
		
		return res;
	}

	public static String getValue(Element item, String str) {		
		NodeList n = item.getElementsByTagName(str);		
		return XMLfunctions_back.getElementValue(n.item(0));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	 public static  String getXML(String url, String data[][]){	 
		 String line = null;
		 
		 ArrayList<NameValuePair>nameValuePairs = new ArrayList<NameValuePair>();
		 
		 for(int i=0; i<data.length; i++)
			 nameValuePairs.add(new BasicNameValuePair(data[i][0], data[i][1]));
		 
		 
		 if( isAvailable(url) ){
			 
			 int times = 5; 
			 for( int i=0; i<times; i++)
			 { 
				 try {
					 
					 DefaultHttpClient httpClient = new DefaultHttpClient();
					 HttpPost httpPost = new HttpPost(url);
					 httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					 
					 HttpResponse httpResponse = httpClient.execute(httpPost);
					 HttpEntity httpEntity = httpResponse.getEntity();
					 line = EntityUtils.toString(httpEntity);
					 
					 break;
					 
				 } 
				 
				 catch (RuntimeException e) {
					 line = "serverError";
				 }
				 catch (UnsupportedEncodingException e) {
					 line = "serverError";
				 }
				 catch (MalformedURLException e) {
					 line = "serverError";
				 }
				 catch (IOException e) {
					 line = "serverError";
				 }
				 catch (Exception e) {
					 line = "serverError";
				 }  
			 }
			 
		 }
		 else
			 line = "serverError";
		 
		 return line; 
	 }


	 
}
 
