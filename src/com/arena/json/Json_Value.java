package com.arena.json;

import org.json.JSONException;
import org.json.JSONObject;

public class Json_Value {

	public static String ShopIdJson(String user)
	{

		String json = "";

		// 3. build jsonObject
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("user_id", user);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		json = jsonObject.toString();

		return json;
	}
	
	public static String CategoryIdJson(String user, String categoryId)
	{
		
		String json = "";
		
		// 3. build jsonObject
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("user_id", user);
			jsonObject.accumulate("category_id", categoryId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		json = jsonObject.toString();
		
		return json;
	}
	

}
