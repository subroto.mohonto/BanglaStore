package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.arena.bangla.store.R;
import com.arena.bangla.store.LatestNewsDetailsActivity;
import com.arena.image.loader.TempData;







import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AdapterForLatestNews extends BaseAdapter {

	// Declare Variables
Context context;
	
	ArrayList<HashMap<String, String>> JobItemList;
	String Head="abd";
	
	private static final String TAG_STATUS = "status";
	private static final String TAG_MSG = "message";
	int pos=0;

	HashMap<String, String> resultMap = new HashMap<String, String>();
	TextView txtTotal;

	public AdapterForLatestNews(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		JobItemList = arraylist;
		

	}

	@Override
	public int getCount() {
		return JobItemList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_latest_news, null);
		TextView txtTitle=(TextView)retval.findViewById(R.id.txtTitle);
		TextView txtAuthorName=(TextView)retval.findViewById(R.id.txtAuthorName);
		TextView txtDescripiton=(TextView)retval.findViewById(R.id.txtDescripiton);

		TextView txtHead=(TextView)retval.findViewById(R.id.txtHeadd);
		
		HashMap<String, String> resultMap = new HashMap<String, String>();
		resultMap=JobItemList.get(position);
		
		txtTitle.setText(resultMap.get("title"));
		txtAuthorName.setText(resultMap.get("authorname"));
		txtDescripiton.setText(resultMap.get("description"));
		

		LinearLayout ll=(LinearLayout)retval.findViewById(R.id.llHeading);
	
		Log.e("Head", Head);
		Log.e("resultMap.get(Head)", resultMap.get("Head"));
		
		
		
		if(position==0)
		{
			txtHead.setText(resultMap.get("Head"));
			ll.setVisibility(View.VISIBLE);
		}
		else
		{
			if(!resultMap.get("Head").equalsIgnoreCase(Head))
			{
				Head=resultMap.get("Head");	
				ll.setVisibility(View.VISIBLE);
				txtHead.setText(Head);
			}
		}
		
		
		
		LinearLayout llDescription=(LinearLayout)retval.findViewById(R.id.llDescription);

		llDescription.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TempData.DescriptionID=position;
				Intent in=new Intent(context,LatestNewsDetailsActivity.class);
				context.startActivity(in);
			}
		});
		return retval;
	}


	

}
