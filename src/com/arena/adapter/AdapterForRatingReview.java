package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.arena.bangla.store.R;
import com.arena.image.loader.ImageLoader;
import com.devsmart.android.ui.HorizontalListView;






import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AdapterForRatingReview extends BaseAdapter {

	// Declare Variables
	Context context;
	
	ArrayList<HashMap<String, String>> itemListContent = new ArrayList<HashMap<String, String>>();


	

	HashMap<String, String> mapContent = new HashMap<String, String>();
	HomeProjectListAdapter featuredProjectsListAdapter;
	

	

	public AdapterForRatingReview(Context context,ArrayList<HashMap<String, String>> arraylistContent) {
		this.context = context;
		
		itemListContent = arraylistContent;
		
		 
	}

	@Override
	public int getCount() {
		return itemListContent.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
//		return position;
	}

	
	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		
		// Declare Variables
//		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_row, null);
		View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_list_row, null);
		

		
	
		
		
	    TextView txtName=(TextView)view2.findViewById(R.id.name); // project name 
	    TextView txtComment=(TextView)view2.findViewById(R.id.Comment); // project name 
	    RatingBar ratingBar = (RatingBar)view2.findViewById(R.id.Rating);
		
		
		
		mapContent = itemListContent.get(position);
		
//		resultMap=JobItemList.get(position);
		/*	
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapInfo.get("id"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapInfo.get("type_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapInfo.get("short_description"));*/
		
	
		
		
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("content_id"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("content_title"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("content_description"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("preview"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("banner"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("total_download"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("payment_type"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("price"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("company_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("developer_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("category_name"));
		
		
		

	     
	
		
	    String p_name = mapContent.get("user_name");
        String comment = mapContent.get("review_text");
        String rating =  mapContent.get("rating");
//        String _Price =  mapContent.get("price");
        
        Float _rating = Float.parseFloat(rating);
        		
        Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&& p_name "+p_name);
        Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&  comment "+comment);
        Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+rating);
		
        ratingBar.setRating(_rating);
        txtName.setText(p_name);
        txtComment.setText(comment);
       
        
//        ProjectsList.addView(p_name);
		return view2;
	}


	
	

	

}
