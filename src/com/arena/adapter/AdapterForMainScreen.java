package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.arena.bangla.store.R;
import com.arena.image.loader.ImageLoader;
import com.devsmart.android.ui.HorizontalListView;






import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AdapterForMainScreen extends BaseAdapter {

	// Declare Variables
	Context context;
	
	ArrayList<HashMap<String, String>> itemListContent = new ArrayList<HashMap<String, String>>();


	

	HashMap<String, String> mapContent = new HashMap<String, String>();
	HomeProjectListAdapter featuredProjectsListAdapter;
	  public ImageLoader imageLoader; 

	

	public AdapterForMainScreen(Context context,ArrayList<HashMap<String, String>> arraylistContent) {
		this.context = context;
		
		itemListContent = arraylistContent;
		
		 imageLoader=new ImageLoader(context.getApplicationContext());
	}

	@Override
	public int getCount() {
		return itemListContent.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
//		return position;
	}

	
	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		
		// Declare Variables
//		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_row, null);
		View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.apps_list_row, null);
		

		
	
		
		ImageView thumb_image=(ImageView)view2.findViewById(R.id.list_image); // thumb image 
	    TextView txtName=(TextView)view2.findViewById(R.id.title); // project name 
//	    TextView txtPrice=(TextView)view2.findViewById(R.id.Price); // project name 
	    RatingBar ratingBar = (RatingBar)view2.findViewById(R.id.Rating);
	    ProgressBar progressBar1 = (ProgressBar)view2.findViewById(R.id.progressBar1);
		
		
		
		mapContent = itemListContent.get(position);
		
//		resultMap=JobItemList.get(position);
		/*	
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapInfo.get("id"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapInfo.get("type_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapInfo.get("short_description"));*/
		
	
		
		
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("content_id"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("content_title"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("content_description"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("preview"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("banner"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("total_download"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("payment_type"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("price"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("company_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("developer_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "+++++++++++++++++++++++"+mapContent.get("category_name"));
		
		
		/*TextView txtProductName=(TextView)view.findViewById(R.id.txtfeatureproject);
		
		txtProductName.setText(mapInfo.get("type_name"));*/
		
		
//		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout., null);
		
		/* TextView txtCode=(TextView)view.findViewById(R.id.txtCode);
	     TextView txtDate=(TextView)view.findViewById(R.id.txtDate);
	     TextView txtStatus=(TextView)view.findViewById(R.id.txtStatus);
	        
	     txtDate.setVisibility(View.GONE);
	     txtStatus.setVisibility(View.GONE);
	    
		
	     
	     map = itemListFromDB.get(position);
	     
	     
	     String NOTICE_INFO = map.get("NOTICE_INFO");
	     txtCode.setText(NOTICE_INFO);*/
	     
		Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+mapContent.get("content_title"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+mapContent.get("preview"));
		
	    String p_name = mapContent.get("content_title");
        String image_view = mapContent.get("preview");
        String rating =  mapContent.get("rating");
//        String _Price =  mapContent.get("price");
        String Name = null;
       
        if(p_name.length()>10)
      
        	Name = p_name.substring(0,10)+"...";
          else
        	  Name = p_name;
        
        Float _rating = Float.parseFloat(rating);
        		
        Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+rating);
		
        ratingBar.setRating(_rating);
        txtName.setText(Name);
//        txtPrice.setText(_Price);
        imageLoader.DisplayImage(image_view, thumb_image);
        
//        ProjectsList.addView(p_name);
		return view2;
	}


	
	

	

}
