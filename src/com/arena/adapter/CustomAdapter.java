package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.arena.bangla.store.R;
import com.arena.bangla.store.AppsActivity;
import com.arena.bangla.store.AppsDetailsViewActivity;
import com.arena.bangla.store.AudioDetailsActivity;
import com.arena.bangla.store.BooksDetailsActivity;
import com.arena.bangla.store.Category_Wise_Activity;
import com.arena.bangla.store.CategorywiseAppsDetailsActivity;
import com.arena.bangla.store.GameDetailsActivity;
import com.arena.bangla.store.VideoDetailsActivity;
import com.arena.bangla.store.WallpaperDetailsActivity;
import com.arena.image.loader.ImageLoader;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends BaseAdapter{   
	ArrayList<ArrayList< HashMap<String, String>>> result=new ArrayList<ArrayList< HashMap<String, String>>>();
	ArrayList< HashMap<String, String>> CatagoryList=new ArrayList< HashMap<String, String>>();

	Activity context;
	int [] imageId;
	private static LayoutInflater inflater=null;
	public ImageLoader imageLoader; 
	String Language ="";
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;


	public CustomAdapter(Category_Wise_Activity category_Wise_Activity,
			ArrayList<ArrayList<HashMap<String, String>>> collection) {
		result=collection;
		context=category_Wise_Activity;

		inflater = ( LayoutInflater )context.
				getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader=new ImageLoader(context.getApplicationContext());
		 sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return result.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View view=inflater.inflate(R.layout.item, null);
		LinearLayout llTop=(LinearLayout)view.findViewById(R.id.llTop);
		TextView txtHeading=(TextView)view.findViewById(R.id.txtHeading);
		Button txtMore=(Button)view.findViewById(R.id.txtMoreVideo);

		View view1=inflater.inflate(R.layout.item_data, null);
		LinearLayout llDescrition=(LinearLayout)view1.findViewById(R.id.llItemDescription);

		 Language = sharedpreferences.getString("Language", "");
	        if(Language.equals("English"))
	        	txtMore.setText("More");
	        else
	        	txtMore.setText("আরও");

		CatagoryList=result.get(position);

		Log.e("Position", String.valueOf(position));
		HashMap<String, String> mapHead=CatagoryList.get(0);
		txtHeading.setText(mapHead.get("ParentType"));


		for(int i=0;i<CatagoryList.size();i++)
		{

			HashMap<String, String> map=CatagoryList.get(i);
			View view3=inflater.inflate(R.layout.apps_list_row, null);
			final RelativeLayout llDetails=(RelativeLayout)view3.findViewById(R.id.thumbnail);
			llDetails.setId(i);


			TextView text=(TextView)view3.findViewById(R.id.title);
			ImageView image=(ImageView)view3.findViewById(R.id.list_image);
			RatingBar ratingBar = (RatingBar)view3.findViewById(R.id.Rating);

			Log.e("map.get(preview)", map.get("preview"));


			String p_name = map.get("content_title");
			String image_view = map.get("preview");
			String rating =  map.get("rating");
//			String SubLink =  map.get("SubLink");
			
			//		        String _Price =  mapContent.get("price");
			String Name = null;

			if(p_name.length()>10)

				Name = p_name.substring(0,10)+"...";
			else
				Name = p_name;

			Float _rating = Float.parseFloat(rating);

			Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+rating);

//			ratingBar.setRating(_rating);
//			txtName.setText(Name);
			//		        txtPrice.setText(_Price);
//			imageLoader.DisplayImage(image_view, thumb_image);




			ratingBar.setRating(_rating);

			text.setText(Name);

			imageLoader.DisplayImage(image_view, image);

			map.get("type_id");




			view3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,          
					LayoutParams.WRAP_CONTENT));
			llDescrition.addView(view3);




			llDetails.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					CatagoryList=result.get(position);
					HashMap<String, String> map=CatagoryList.get(llDetails.getId());
//					Toast.makeText(context, "ID:"+String.valueOf(map.get("content_id")), 1000).show();
					
					if( map.get("SubLink").equals("1"))
					{
						Intent idn = new Intent(context, AppsDetailsViewActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						idn.putExtra("SubLink", map.get("SubLink"));
						context.startActivity(idn);
//						context.finish();
					}
					if( map.get("SubLink").equals("2"))
					{
						Intent idn = new Intent(context, VideoDetailsActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						idn.putExtra("SubLink", map.get("SubLink"));
						context.startActivity(idn);
//						context.finish();
					}
					if( map.get("SubLink").equals("3"))
					{
						Intent idn = new Intent(context, AudioDetailsActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						idn.putExtra("SubLink", map.get("SubLink"));
						context.startActivity(idn);
//						context.finish();
					}
					if( map.get("SubLink").equals("4"))
					{
						Intent idn = new Intent(context, WallpaperDetailsActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						idn.putExtra("SubLink", map.get("SubLink"));
						context.startActivity(idn);
//						context.finish();
					}
					if( map.get("SubLink").equals("5"))
					{
						Intent idn = new Intent(context, GameDetailsActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						idn.putExtra("SubLink", map.get("SubLink"));
						context.startActivity(idn);
//						context.finish();
					}
					if( map.get("SubLink").equals("6"))
					{
						Intent idn = new Intent(context, BooksDetailsActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						idn.putExtra("SubLink", map.get("SubLink"));
						context.startActivity(idn);
//						context.finish();
					}
					
					
				}
			});

		}

		txtMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CatagoryList=result.get(position);
				HashMap<String, String> map=CatagoryList.get(0);
//				Toast.makeText(context,"Catagory:"+ String.valueOf(map.get("ParentID")), 1000).show();
				
				Log.e("~~~~~~~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ParentID  : "+map.get("ParentID"));
				Log.e("~~~~~~~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ParentType  : "+map.get("ParentType"));
				Log.e("~~~~~~~~~~~~~~~~~~~~~~~~~~", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SubLink  : "+map.get("SubLink"));
				
				Intent idn = new Intent(context, AppsActivity.class);
				idn.putExtra("Content_Id", map.get("ParentID"));
				idn.putExtra("TitleName", map.get("ParentType"));
				idn.putExtra("SubLink", map.get("SubLink"));
				context.startActivity(idn);
//				context.finish();

			}
		});






		llTop.addView(view1);


		return view;

	}

} 
