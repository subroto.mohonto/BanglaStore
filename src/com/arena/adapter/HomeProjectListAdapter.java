package com.arena.adapter;
 

import java.util.ArrayList;
import java.util.HashMap;

import com.arena.bangla.store.R;
import com.arena.image.loader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeProjectListAdapter extends BaseAdapter {
    
    private Context activity;
    private ArrayList<HashMap<String, String>> ContentData;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
	int imageWidth;
	int imageHeight; 
    
  /*  public HomeProjectListAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        imageLoader=new ImageLoader(activity.getApplicationContext());
        
        setScreenResolution();
    }
*/ 
    public HomeProjectListAdapter(Context c ,ArrayList<HashMap<String, String>> itemListContent) {
		// TODO Auto-generated constructor stub
    	
    	 activity = c;
    	
    	ContentData=itemListContent;
         inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         imageLoader=new ImageLoader(activity.getApplicationContext());
         
//         setScreenResolution();
	}

	@Override
	public int getCount() {
        return ContentData.size();
    }

    @Override
	public Object getItem(int position) {
        return position;
    }

    @Override
	public long getItemId(int position) {
        return position;
    }
    
  @Override
public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.home_project_listitem1, null);
          
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.hm_pro_list_adap_image); // thumb image 
        TextView txtName=(TextView)vi.findViewById(R.id.hm_pro_list_adap_text); // project name 
        
        
        
        /*   LayoutParams params=thumb_image.getLayoutParams();
        params.width=imageWidth; 
        params.height=imageHeight;         //another implementation and use home_project_listItem1.xml
 //     params.height=imageHeight-118;   //our phone and use home_project_listItem.xml 
 //       params.height=imageHeight-168;   //clients phone and use home_project_listItem.xml
        
        thumb_image.setLayoutParams(params); 
        
        LayoutParams tparams=txtName.getLayoutParams();
        tparams.width=imageWidth;   
        txtName.setLayoutParams(tparams);
        txtName.setGravity(Gravity.CENTER_HORIZONTAL);
        txtName.setPadding(0, 8, 0, 3);*/
        
        HashMap<String, String> mapContent = new HashMap<String, String>();
        mapContent = ContentData.get(position);
        
        Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("content_id"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("content_title"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("content_description"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("preview"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("banner"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("total_download"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("payment_type"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("price"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("company_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("developer_name"));
		Log.e("~~~~~~~~~~~~~~~~~~~~", "vvvvvvvvvvvvvvvvvvvvvvvv"+mapContent.get("category_name"));
		
        
        String p_name = mapContent.get("content_title");
        String image_view = mapContent.get("preview");
//        
//        String trimNavana = p_name.replaceAll("Navana ", "");  
//        String a_p_n[] = trimNavana.split(" ");
//        
//        String p_name_trim="";
//        
//        if(a_p_n.length>2 )
//        	p_name_trim = a_p_n[0] + " " + a_p_n[1]; 
//        else
//        	p_name_trim=trimNavana;	 
// 
//         
        
        String Name = null;
        
        if(p_name.length()>12)
      
        	Name = p_name.substring(0,12)+"...";
          else
        	  Name = p_name;
        
        txtName.setText(Name);
        imageLoader.DisplayImage(image_view, thumb_image);
		
        return vi;
    }
   
 /* @SuppressWarnings("deprecation")
	private void setScreenResolution()
	{
		int homePageLayoutHeight=480;
		int homeProjectImageHeightByWeight=192;
		WindowManager w = activity.getWindowManager();
		Display d = w.getDefaultDisplay();  
		imageWidth =(d.getWidth()-60)/2 ; 
		imageHeight = (d.getHeight()*homeProjectImageHeightByWeight)/homePageLayoutHeight-20;  //including padding

	}	*/
}

