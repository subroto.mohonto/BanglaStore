package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.arena.bangla.store.R;
import com.arena.bangla.store.AppsDetailsViewActivity;
import com.arena.bangla.store.AudioDetailsActivity;
import com.arena.bangla.store.BooksDetailsActivity;
import com.arena.bangla.store.GameDetailsActivity;
import com.arena.bangla.store.VideoDetailsActivity;
import com.arena.bangla.store.WallpaperDetailsActivity;
import com.arena.image.loader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore.Audio.AudioColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
 
public class LazyAdapter_Searching extends BaseAdapter {
     
    private Activity activity;
    ArrayList<HashMap<String, String>> itemListImage = new ArrayList<HashMap<String, String>>();
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    HashMap<String, String> mapImage = new HashMap<String, String>();
     
    public LazyAdapter_Searching(Activity a, ArrayList<HashMap<String, String>> arraylistImage) {
        activity = a;
        itemListImage=arraylistImage;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }
 
    public int getCount() {
        return itemListImage.size();
    }
 
    public Object getItem(int position) {
        return position;
    }
 
    public long getItemId(int position) {
        return position;
    }
     
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.apps_list_row12, null);
 
        TextView text=(TextView)vi.findViewById(R.id.title);
        ImageView image=(ImageView)vi.findViewById(R.id.list_image);
//        TextView txtPrice=(TextView)vi.findViewById(R.id.Price); // project name 
	    RatingBar ratingBar = (RatingBar)vi.findViewById(R.id.Rating);
        
        mapImage = itemListImage.get(position);
        
//        String image_view = "http://navana-realestate.com/mobile/apps/upload_images/project/"+mapImage.get("image");
        String image_view = mapImage.get("preview");
       
        String rating =  mapImage.get("rating");
//        String _Price =  mapImage.get("price");
        
        Float _rating = Float.parseFloat(rating);
        
        
        Log.e("RRRRRRRRRRRRRRRRRRRRR", "RRRRRRRRRRRRRRRRRRRRRRRRR"+image_view);
        
        ratingBar.setRating(_rating);
//         txtPrice.setText(_Price);
        
          text.setText(mapImage.get("content_title"));
        imageLoader.DisplayImage(image_view, image);
        
        mapImage.get("type_id");
        
        image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				 mapImage = itemListImage.get(position);
				if( mapImage.get("type_id").equalsIgnoreCase("1"))
				{
					Log.e("content_id", mapImage.get("content_id"));
                  Intent in=new Intent(activity,AppsDetailsViewActivity.class);
                  in.putExtra("Content_Id", mapImage.get("content_id"));
                  in.putExtra("SubLink", mapImage.get("content_id"));
                  in.putExtra("which_screen", "SearchDetails");
                  activity.startActivity(in);
				
				}
				if( mapImage.get("type_id").equalsIgnoreCase("2"))
				{
					Log.e("content_id", mapImage.get("content_id"));
                  Intent in=new Intent(activity,VideoDetailsActivity.class);
                  in.putExtra("SubLink", mapImage.get("content_id"));
                  in.putExtra("Content_Id", mapImage.get("content_id"));
                  in.putExtra("which_screen", "SearchDetails");
                  activity.startActivity(in);
				
				}
				if( mapImage.get("type_id").equalsIgnoreCase("3"))
				{
					Log.e("content_id", mapImage.get("content_id"));
                  Intent in=new Intent(activity,AudioDetailsActivity.class);
                  in.putExtra("SubLink", mapImage.get("content_id"));
                  in.putExtra("Content_Id", mapImage.get("content_id"));
                  in.putExtra("which_screen", "SearchDetails");
                  activity.startActivity(in);
				
				}
				if( mapImage.get("type_id").equalsIgnoreCase("4"))
				{
					Log.e("content_id", mapImage.get("content_id"));
                  Intent in=new Intent(activity,WallpaperDetailsActivity.class);
                  in.putExtra("SubLink", mapImage.get("content_id"));
                  in.putExtra("Content_Id", mapImage.get("content_id"));
                  in.putExtra("which_screen", "SearchDetails");
                  activity.startActivity(in);
				
				}
				if( mapImage.get("type_id").equalsIgnoreCase("5"))
				{
					Log.e("content_id", mapImage.get("content_id"));
                  Intent in=new Intent(activity,GameDetailsActivity.class);
                  in.putExtra("SubLink", mapImage.get("content_id"));
                  in.putExtra("Content_Id", mapImage.get("content_id"));
                  in.putExtra("which_screen", "SearchDetails");
                  activity.startActivity(in);
				
				}
				if( mapImage.get("type_id").equalsIgnoreCase("6"))
				{
					Log.e("content_id", mapImage.get("content_id"));
                  Intent in=new Intent(activity,BooksDetailsActivity.class);
                  in.putExtra("SubLink", mapImage.get("content_id"));
                  in.putExtra("Content_Id", mapImage.get("content_id"));
                  in.putExtra("which_screen", "SearchDetails");
                  activity.startActivity(in);
				
				}
				
					
			}
		});
        
        
        return vi;
    }
}