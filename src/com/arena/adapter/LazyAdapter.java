package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.arena.bangla.store.R;
import com.arena.bangla.store.AppsDetailsViewActivity;
import com.arena.bangla.store.AudioDetailsActivity;
import com.arena.bangla.store.BooksDetailsActivity;
import com.arena.bangla.store.GameDetailsActivity;
import com.arena.bangla.store.VideoDetailsActivity;
import com.arena.bangla.store.WallpaperDetailsActivity;
import com.arena.image.loader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore.Audio.AudioColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
 
public class LazyAdapter extends BaseAdapter {
     
    private Activity activity;
    ArrayList<HashMap<String, String>> itemListImage = new ArrayList<HashMap<String, String>>();
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    HashMap<String, String> mapImage = new HashMap<String, String>();
     
    public LazyAdapter(Activity a, ArrayList<HashMap<String, String>> arraylistImage) {
        activity = a;
        itemListImage=arraylistImage;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }
 
    public int getCount() {
        return itemListImage.size();
    }
 
    public Object getItem(int position) {
        return position;
    }
 
    public long getItemId(int position) {
        return position;
    }
     
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.apps_list_row12, null);
 
        TextView text=(TextView)vi.findViewById(R.id.title);
        ImageView image=(ImageView)vi.findViewById(R.id.list_image);
        
//        TextView txtPrice=(TextView)vi.findViewById(R.id.Price); // project name 
	    RatingBar ratingBar = (RatingBar)vi.findViewById(R.id.Rating);
        
        mapImage = itemListImage.get(position);
        
//        String image_view = "http://navana-realestate.com/mobile/apps/upload_images/project/"+mapImage.get("image");
        String image_view = mapImage.get("preview");
        String p_name = mapImage.get("content_title");
        
        String Name = null;
        
        if(p_name.length()>12)
      
        	Name = p_name.substring(0,12)+"...";
          else
        	  Name = p_name;
        
       
        String rating =  mapImage.get("rating");
//        String _Price =  mapImage.get("price");
        
        Float _rating = Float.parseFloat(rating);
        
        
        Log.e("RRRRRRRRRRRRRRRRRRRRR", "RRRRRRRRRRRRRRRRRRRRRRRRR"+image_view);
        
        ratingBar.setRating(_rating);
//         txtPrice.setText(_Price);
        
          text.setText(Name);
        imageLoader.DisplayImage(image_view, image);
        
        mapImage.get("type_id");
      
        
        
        return vi;
    }
}