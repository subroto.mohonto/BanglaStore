package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.arena.bangla.store.R;
import com.arena.bangla.store.AppsActivity;
import com.arena.bangla.store.AppsDetailsViewActivity;
import com.arena.bangla.store.AudioDetailsActivity;
import com.arena.bangla.store.BooksDetailsActivity;
import com.arena.bangla.store.Category_Wise_Activity;
import com.arena.bangla.store.CategorywiseAppsDetailsActivity;
import com.arena.bangla.store.GameDetailsActivity;
import com.arena.bangla.store.TemplateHomeScreenActivity;
import com.arena.bangla.store.TemplateHome_InnerScreenActivity;
import com.arena.bangla.store.VideoDetailsActivity;
import com.arena.bangla.store.WallpaperDetailsActivity;
import com.arena.image.loader.ImageLoader;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapterTemplateHomeScreen extends BaseAdapter{   
	ArrayList<ArrayList< HashMap<String, String>>> result=new ArrayList<ArrayList< HashMap<String, String>>>();
	ArrayList< HashMap<String, String>> CatagoryList=new ArrayList< HashMap<String, String>>();

	Activity context;
	int [] imageId;
	private static LayoutInflater inflater=null;
	public ImageLoader imageLoader; 
	String Language ="";
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	
	int imageWidth;
	int imageHeight; 


	public CustomAdapterTemplateHomeScreen(TemplateHomeScreenActivity category_Wise_Activity,
			ArrayList<ArrayList<HashMap<String, String>>> collection) {
		result=collection;
		context=category_Wise_Activity;

		inflater = ( LayoutInflater )context.
				getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader=new ImageLoader(context.getApplicationContext());
		
		 sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		 
		 setScreenResolution();

	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return result.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		
		Display display = context.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		
		Log.e("--------------------", "--------------------"+width);
		Log.e("--------------------", "--------------------"+height);
		
		
		View view=inflater.inflate(R.layout.item_for_templet, null);
		LinearLayout llTop=(LinearLayout)view.findViewById(R.id.llTop);
		TextView txtHeading=(TextView)view.findViewById(R.id.txtHeading);
		Button txtMore=(Button)view.findViewById(R.id.txtMoreVideo);
		final Button txtMoreBtn=(Button)view.findViewById(R.id.txtMorebtn);

		View view1=inflater.inflate(R.layout.item_data, null);
		LinearLayout llDescrition=(LinearLayout)view1.findViewById(R.id.llItemDescription);

		
		  Language = sharedpreferences.getString("Language", "");
	        if(Language.equals("English"))
	        	txtMore.setText("More");
	        else
	        	txtMore.setText("আরও");
		

		CatagoryList=result.get(position);

		Log.e("Position", String.valueOf(position));
		HashMap<String, String> mapHead=CatagoryList.get(0);
		txtHeading.setText(mapHead.get("ParentType"));
		txtMoreBtn.setText(mapHead.get("ParentID"));


		if(width>500)
		{
			for(int i=0;i<CatagoryList.size();i++)
			{

				HashMap<String, String> map=CatagoryList.get(i);
				View view3=inflater.inflate(R.layout.apps_list_row_lates_for_temp1t_3piece, null);
				final RelativeLayout llDetails=(RelativeLayout)view3.findViewById(R.id.thumbnail);
				llDetails.setId(i);


				TextView text=(TextView)view3.findViewById(R.id.title);
				ImageView image=(ImageView)view3.findViewById(R.id.list_image);
				RatingBar ratingBar = (RatingBar)view3.findViewById(R.id.Rating);

				Log.e("map.get(preview)", map.get("preview"));
				
				
				 android.view.ViewGroup.LayoutParams params=image.getLayoutParams();
			     params.width=imageWidth; 
			     image.setLayoutParams(params); 


				String p_name = map.get("content_title");
				String image_view = map.get("preview");
				String rating =  map.get("rating");
//				String SubLink =  map.get("SubLink");
				
				//		        String _Price =  mapContent.get("price");
				String Name = null;

				if(p_name.length()>10)

					Name = p_name.substring(0,10)+"...";
				else
					Name = p_name;

				Float _rating = Float.parseFloat(rating);

				Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+rating);

//				ratingBar.setRating(_rating);
//				txtName.setText(Name);
				//		        txtPrice.setText(_Price);
//				imageLoader.DisplayImage(image_view, thumb_image);




				ratingBar.setRating(_rating);

				text.setText(p_name);

				imageLoader.DisplayImage(image_view, image);

				map.get("type_id");




				view3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,          
						LayoutParams.WRAP_CONTENT));
				llDescrition.addView(view3);




				llDetails.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						CatagoryList=result.get(position);
						
						HashMap<String, String> map=CatagoryList.get(llDetails.getId());
						
						HashMap<String, String> mapHead=CatagoryList.get(0);
						
//						Toast.makeText(context, "ID:"+String.valueOf(map.get("content_id")), 1000).show();
						Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG"+map.get("content_id"));
						
					/*	Intent idn = new Intent(context, AppsDetailsViewActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						context.startActivity(idn);*/
						
						Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
						if( mapHead.get("ParentID").equals("1"))
						{
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG"+map.get("content_id"));
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
	        				Intent idn = new Intent(context, AppsDetailsViewActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("2"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, VideoDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("3"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, AudioDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("4"))
						{
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							
							Intent idn = new Intent(context, WallpaperDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("5"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, GameDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("6"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, BooksDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						
						
					}
				});

			}

		}
		else
		{
			for(int i=0;i<CatagoryList.size();i++)
			{

				HashMap<String, String> map=CatagoryList.get(i);
				View view3=inflater.inflate(R.layout.apps_list_row_lates_for_temp1t, null);
				final RelativeLayout llDetails=(RelativeLayout)view3.findViewById(R.id.thumbnail);
				llDetails.setId(i);


				TextView text=(TextView)view3.findViewById(R.id.title);
				ImageView image=(ImageView)view3.findViewById(R.id.list_image);
				RatingBar ratingBar = (RatingBar)view3.findViewById(R.id.Rating);

				Log.e("map.get(preview)", map.get("preview"));
				
				
				 android.view.ViewGroup.LayoutParams params=image.getLayoutParams();
			     params.width=imageWidth; 
			     image.setLayoutParams(params); 


				String p_name = map.get("content_title");
				String image_view = map.get("preview");
				String rating =  map.get("rating");
//				String SubLink =  map.get("SubLink");
				
				//		        String _Price =  mapContent.get("price");
				String Name = null;

				if(p_name.length()>10)

					Name = p_name.substring(0,10)+"...";
				else
					Name = p_name;

				Float _rating = Float.parseFloat(rating);

				Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+rating);

//				ratingBar.setRating(_rating);
//				txtName.setText(Name);
				//		        txtPrice.setText(_Price);
//				imageLoader.DisplayImage(image_view, thumb_image);




				ratingBar.setRating(_rating);

				text.setText(p_name);

				imageLoader.DisplayImage(image_view, image);

				map.get("type_id");




				view3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,          
						LayoutParams.WRAP_CONTENT));
				llDescrition.addView(view3);




				llDetails.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						CatagoryList=result.get(position);
						
						HashMap<String, String> map=CatagoryList.get(llDetails.getId());
						
						HashMap<String, String> mapHead=CatagoryList.get(0);
						
//						Toast.makeText(context, "ID:"+String.valueOf(map.get("content_id")), 1000).show();
						Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG"+map.get("content_id"));
						
					/*	Intent idn = new Intent(context, AppsDetailsViewActivity.class);
						idn.putExtra("Content_Id", map.get("content_id"));
						idn.putExtra("which_screen", "sub_inner");
						context.startActivity(idn);*/
						
						Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
						if( mapHead.get("ParentID").equals("1"))
						{
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG"+map.get("content_id"));
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
	        				Intent idn = new Intent(context, AppsDetailsViewActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("2"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, VideoDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("3"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, AudioDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("4"))
						{
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							
							Intent idn = new Intent(context, WallpaperDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("5"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, GameDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						if( mapHead.get("ParentID").equals("6"))
						{
							
							Log.e("GGGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGG---"+mapHead.get("ParentID"));
							Intent idn = new Intent(context, BooksDetailsActivity.class);
							idn.putExtra("Content_Id", map.get("content_id"));
							idn.putExtra("which_screen", "sub_inner");
							idn.putExtra("SubLink", mapHead.get("ParentID"));
							context.startActivity(idn);
//							context.finish();
						}
						
						
					}
				});

			}

		}
		//-------------------------------------if condition done-------------------------------------
		txtMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				CatagoryList=result.get(position);

				Log.e("Position", String.valueOf(position));
				HashMap<String, String> mapHead=CatagoryList.get(0);
				
				Log.e("VVVVVVVVVVVVVVVVVv", "VVVVVVVVVVVVVVVVVVVVV"+mapHead.get("ParentType"));
				Log.e("VVVVVVVVVVVVVVVVVv", "VVVVVVVVVVVVVVVVVVVVV"+mapHead.get("ParentID"));
				/*txtHeading.setText(mapHead.get("ParentType"));
				txtMoreBtn.setText(mapHead.get("ParentID"));*/
				
				
				
				/*Log.e("VVVVVVVVVVVVVVVVVv", "VVVVVVVVVVVVVVVVVVVVV"+txtMoreBtn.getText());
				CatagoryList=result.get(position);
				HashMap<String, String> map=CatagoryList.get(0);*/
				
				
//				Toast.makeText(context,"Catagory:"+ String.valueOf(map.get("ParentID")), 1000).show();
				
//				Log.e("VVVVVVVVVVVVVVVVVv", "VVVVVVVVVVVVVVVVVVVVV"+map.get("ParentID"));
				
				Intent idn = new Intent(context, TemplateHome_InnerScreenActivity.class);
				idn.putExtra("SubLink", mapHead.get("ParentID"));
				idn.putExtra("TitleName", mapHead.get("ParentType"));
//				idn.putExtra("SubLink", map.get("SubLink"));
				context.startActivity(idn);
//				context.finish();

			}
		});






		llTop.addView(view1);


		return view;

	}

	
	 @SuppressWarnings("deprecation")
		private void setScreenResolution()
		{
			int homePageLayoutHeight=480;
			int homeProjectImageHeightByWeight=192;
			WindowManager w = context.getWindowManager();
			Display d = w.getDefaultDisplay();  
			imageWidth =(int) (d.getWidth()-20)/2 ; 
			imageHeight = (d.getHeight()*homeProjectImageHeightByWeight)/homePageLayoutHeight-20;  //including padding

		}	
} 
