package com.arena.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.arena.bangla.store.R;
import com.arena.image.loader.ImageLoader;
import com.devsmart.android.ui.HorizontalListView;






import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AdapterForApps extends BaseAdapter {

	// Declare Variables
	Context context;
	
	ArrayList<HashMap<String, String>> itemListContent = new ArrayList<HashMap<String, String>>();


	

	HashMap<String, String> mapContent = new HashMap<String, String>();
	HomeProjectListAdapter featuredProjectsListAdapter;
	  public ImageLoader imageLoader; 

	

	public AdapterForApps(Context context,ArrayList<HashMap<String, String>> arraylistContent) {
		this.context = context;
		
		itemListContent = arraylistContent;
		
		 imageLoader=new ImageLoader(context.getApplicationContext());
	}

	@Override
	public int getCount() {
		return itemListContent.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
//		return position;
	}

	
	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		
		// Declare Variables
//		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_row, null);
		View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.screen_shot_list_row, null);
		

		
	
		
		ImageView thumb_image=(ImageView)view2.findViewById(R.id.list_image); // thumb image 
	   
		
		
		mapContent = itemListContent.get(position);
		

		Log.e("~~~~~~~~~~~~~~~~~~~~", "%%%%^^^^^^^^&&&&&&&&"+mapContent.get("My_Screenshot"));
		
	    ;
        String image_view = mapContent.get("My_Screenshot");
       

        imageLoader.DisplayImage(image_view, thumb_image);
        
//        ProjectsList.addView(p_name);
		return view2;
	}


	
	

	

}
